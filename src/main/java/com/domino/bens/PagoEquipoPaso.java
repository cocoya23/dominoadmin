/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.bens;

import javax.persistence.*;

/**
 *
 * @author Sergio Cano
 */
@Entity
@Table(name = "pago_equipo_paso")
public class PagoEquipoPaso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idinscripcion")
    private Integer idIncripcion;
    @Column(name = "idformacionequipo")
    private Integer idformacionequipo;
    @Column(name = "idTorneo")
    private Integer idTorneo;
    @Column(name = "pais")
    private Integer pais;
    @Column(name = "metropoli")
    private Integer metropoli;
    @Column(name = "club")
    private String club;
    @Column(name = "activo")
    private Integer activo;
    @Column(name = "tipo_pago")
    private Integer tipoPago;
    @Column(name = "identificacion")
    private String identificacion;
    @Column(name = "noidentificacion")
    private String noIdentifiacion;
    @Column(name = "comentarios")
    private String comentarios;
    @Column(name = "idjugador")
    private Integer idjugador;
  

    public PagoEquipoPaso() {
    }

    public PagoEquipoPaso(Integer idIncripcion, Integer idFormacionEquipo,
            Integer idTorneo, Integer pais, Integer metropoli, String club,
            Integer activo, Integer tipoPago, String identificacion,
            String noIdentifiacion, String comentarios, Integer idjugador
            ) {
        this.idIncripcion = idIncripcion;
        this.idformacionequipo = idFormacionEquipo;
        this.idTorneo = idTorneo;
        this.pais = pais;
        this.metropoli = metropoli;
        this.club = club;
        this.activo = activo;
        this.tipoPago = tipoPago;
        this.identificacion = identificacion;
        this.noIdentifiacion = noIdentifiacion;
        this.comentarios = comentarios;
        this.idjugador = idjugador;

    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Integer getIdformacionequipo() {
        return idformacionequipo;
    }

    public void setIdformacionequipo(Integer idformacionequipo) {
        this.idformacionequipo = idformacionequipo;
    }

   
    public Integer getIdIncripcion() {
        return idIncripcion;
    }

    public void setIdIncripcion(Integer idIncripcion) {
        this.idIncripcion = idIncripcion;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public String getNoIdentifiacion() {
        return noIdentifiacion;
    }

    public void setNoIdentifiacion(String noIdentifiacion) {
        this.noIdentifiacion = noIdentifiacion;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public Integer getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(Integer tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    
    
    
 }
