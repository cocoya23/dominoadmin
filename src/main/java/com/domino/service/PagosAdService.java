package com.domino.service;

import com.domino.bens.*;
import com.domino.dao.PagosAdDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

/**
 *
 * @author Sergio Cano
 */
@ManagedBean(name = "pagosAdService", eager = true)
@ApplicationScoped
public class PagosAdService implements Serializable {

    PagosAdDAO pagosAdDao = new PagosAdDAO();

    public List<Torneo> getTorneos() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Torneo> torneos = this.pagosAdDao.getTorneos();
        return torneos;
    }

    public List<JugadorTorneo> getJugador(int id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<JugadorTorneo> torneos = this.pagosAdDao.getJugador(id);
        return torneos;
    }

    public JugadorTorneo getJugadorxTorneo(Integer idTorneo, Integer idJugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        JugadorTorneo jugadorTorneo = this.pagosAdDao.getJugadorxTorneo(idTorneo, idJugador);
        return jugadorTorneo;
    }

    public boolean getrowsPago(Integer idTorneo, Integer idJugador) throws Exception {
        boolean valida;
        Session session = HibernateUtil.getSession();
        session.isConnected();
        int rows = this.pagosAdDao.getrowsPago(idTorneo, idJugador);
        if (rows == 1) {
            valida = false;
        } else {
            valida = true;
        }
        return valida;
    }

    public String namePareja(Integer idTorn, Integer idJug) throws Exception {
        String nombre = "";
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionPareja formacionPareja = this.pagosAdDao.namePareja(idTorn, idJug);
        System.out.println("PAREJA 1: "+formacionPareja.getIdjugador1());
        System.out.println("PAREJA 2: "+formacionPareja.getIdjugador2());
        if (formacionPareja.getIdjugador1() == null || formacionPareja.getIdjugador2() == null) {
            nombre = "";
        } else {
            if (idJug.intValue() == formacionPareja.getIdjugador1().intValue()) {
                nombre = this.pagosAdDao.getNombreEquipo(formacionPareja.getIdjugador2());
            }
            if (idJug.intValue() == formacionPareja.getIdjugador2().intValue()) {
                nombre = this.pagosAdDao.getNombreEquipo(formacionPareja.getIdjugador1());
            }
        }
        return nombre;
    }

    public Torneo getTorneos(Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Torneo torneo = this.pagosAdDao.getTorneos(idtorneo);
        return torneo;
    }

    public Integer idPareja(Integer idTorn, Integer idJug) throws Exception {
        Integer idpareja = 0;
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionPareja formacionPareja = this.pagosAdDao.namePareja(idTorn, idJug);
        if (formacionPareja.getIdjugador1() == null || formacionPareja.getIdjugador2() == null) {
            idpareja = 0;
        } else {
            if (idJug.intValue() == formacionPareja.getIdjugador1().intValue()) {
                idpareja = formacionPareja.getIdjugador2();
            }
            if (idJug.intValue() == formacionPareja.getIdjugador2().intValue()) {
                idpareja = formacionPareja.getIdjugador1();
            }
        }
        return idpareja;
    }

    public FormacionEquipo formEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionEquipo formacionEquipo = this.pagosAdDao.formEquipo(idTorn, idJug);
        return formacionEquipo;
    }

    public Integer equipoRows(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.pagosAdDao.equipoRows(idTorn, idJug);
        return rows;
    }

    public int getTorneosEquiupo() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<FormacionEquipo> torneos = this.pagosAdDao.getFormEquipo();
        int rows = torneos.size();
        return rows;
    }

    public String getNombreEquipo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        String name = this.pagosAdDao.getNombreEquipo(idjugador);
        return name;

    }

    public Integer getMaxControl(Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer maxidcontrol = this.pagosAdDao.getMaxControl(idtorneo);
        if (maxidcontrol == null) {
            return maxidcontrol = 1;
        } else {
            return maxidcontrol = maxidcontrol + 1;
        }


    }

    public void insertPagoControl(PagoControl pagoControl) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.pagosAdDao.insertPagoControl(pagoControl);
    }

    public void insertTorneoControl(TorneoControl torneoControl) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.pagosAdDao.insertTorneoControl(torneoControl);

    }

    public Integer getPAgoControl() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer maxidcontrol = this.pagosAdDao.getPAgoControl();
        if (maxidcontrol == null) {
            return maxidcontrol = 1;
        } else {
            return maxidcontrol = maxidcontrol + 1;
        }
    }

    public void insertaPagoJugador(Integer idjugador, Integer idtorneo, int tipopago, String identificacion, String noidentificacion, String comentarios, Integer num) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Jugador jugador = getJugador2(idjugador);
        Integer idpais = Integer.parseInt(jugador.getPais());
        Integer idmetropoli = Integer.parseInt(jugador.getMetropoli());
        Date now = new Date(System.currentTimeMillis());
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String dia = date.format(now);
        JugadorTorneo jugadorTorneo = getJugadorxTorneo(idtorneo, idjugador);
        Integer individual = jugadorTorneo.getIndividual();
        Integer pareja = jugadorTorneo.getIndividual();
        Integer equipo = jugadorTorneo.getIndividual();
        if (individual == 1) {
            PagoIndividual pagoIndividual = new PagoIndividual(null, idjugador, idtorneo, idpais, idmetropoli, jugador.getClub(), 1, dia, tipopago, 1, identificacion, noidentificacion, comentarios);
            insertPagoIndividual(pagoIndividual);
        }
        if (pareja == 1) {
            FormacionPareja formacionPareja = getformacionPareja(idtorneo, idjugador);
            PagoParejaPaso pagoParejaPaso = new PagoParejaPaso(null, formacionPareja.getIdformacionpareja(), idtorneo, idpais, idmetropoli, jugador.getClub(), 1, tipopago, identificacion, noidentificacion, comentarios, idjugador);
            insertPagoParejaPaso(pagoParejaPaso);
            Integer rows = getListParejaPaso(idtorneo, formacionPareja.getIdformacionpareja());
            if (rows == 2) {
                PagoPareja pagoPareja = new PagoPareja(null, formacionPareja.getIdformacionpareja(), idtorneo, idpais, idmetropoli, jugador.getClub(), 1, tipopago, identificacion, noidentificacion, comentarios);
                insertPagoPareja(pagoPareja);
            }
        }
        if (equipo == 1) {
            FormacionEquipo formacionEquipo = formEquipo(idtorneo, idjugador);
            System.out.println("*d*sf*sdf" + formacionEquipo.getIdformacionequipo());
            PagoEquipoPaso equipoPaso = new PagoEquipoPaso(null, formacionEquipo.getIdformacionequipo(), idtorneo, idpais, idmetropoli, jugador.getClub(), 1, tipopago, identificacion, noidentificacion, comentarios, idjugador);
            insertPagoEquipoPaso(equipoPaso);
            Integer rows = getListEquipoPaso(idtorneo, formacionEquipo.getIdformacionequipo());
            if (rows == 4) {
                PagoEquipo pagoEquipo = new PagoEquipo(null, formacionEquipo.getIdformacionequipo(), idtorneo, idpais, idmetropoli, jugador.getClub(), 1, tipopago, identificacion, noidentificacion, comentarios);
                insertPagoEquipo(pagoEquipo);
            }
        }
    }

    public FormacionPareja getformacionPareja(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionPareja formacionPareja = this.pagosAdDao.getformacionPareja(idTorn, idJug);
        return formacionPareja;
    }

    public void insertPagoIndividual(PagoIndividual pagoIndividual) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.pagosAdDao.insertPagoIndividual(pagoIndividual);
    }

    public void insertPagoParejaPaso(PagoParejaPaso pagoParejaPaso) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.pagosAdDao.insertPagoParejaPaso(pagoParejaPaso);
    }

    public void insertPagoPareja(PagoPareja pagoPareja) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.pagosAdDao.insertPagoPareja(pagoPareja);
    }

    public void insertPagoEquipoPaso(PagoEquipoPaso pagoEquipoPaso) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.pagosAdDao.insertPagoEquipoPaso(pagoEquipoPaso);
    }

    public void insertPagoEquipo(PagoEquipo pagoEquipo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.pagosAdDao.insertPagoEquipo(pagoEquipo);
    }

    public Jugador getJugador2(Integer idJugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Jugador jugador = this.pagosAdDao.getJugador(idJugador);
        return jugador;
    }

    public Integer getListParejaPaso(Integer idTorneo, Integer Idformacionpareja) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<PagoParejaPaso> result = this.pagosAdDao.getListParejaPaso(idTorneo, Idformacionpareja);
        Integer rows = result.size();
        return rows;
    }

    public Integer getListEquipoPaso(Integer idTorneo, Integer Idformacionequipo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<PagoEquipoPaso> result = this.pagosAdDao.getListEquipoPaso(idTorneo, Idformacionequipo);
        Integer rows = result.size();
        return rows;
    }

    public Integer getrowsPagoControl(Integer idTorneo, Integer idJugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.pagosAdDao.getrowsPagoControl(idTorneo, idJugador);
        return rows;

    }

    public boolean bloqueaPago1(Integer num) {

        if (num == 1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean bloqueaPago2(Integer num) {
        if (num == 1) {
            return true;
        } else {
            return false;
        }
    }
}