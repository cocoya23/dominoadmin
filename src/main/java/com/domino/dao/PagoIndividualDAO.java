package com.domino.dao;

import com.domino.bens.PagoIndividual;
import com.domino.util.HibernateUtil;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Order;

/**
 * Esta clase accede a la infomración de pago individual.
 * Se puede obtener la siguiente información:
 * 
 * <ol>
 * <il>Los pagos individuales </il>
 * <il>idPais por pago individual, un <code>List</code></il>
 * </ol>
 * @author A. Eliud Uriostegui Gonzalez
 * @see PagoIndividual
 */
public class PagoIndividualDAO {
    
    Session session;
    
    /**
     * Obtiene el pago individual lista
     * 
     * @return la lista del  pago individual
     * @throws Exception 
     */
    public List<PagoIndividual> getPagoIndividualList()throws Exception{
        List<PagoIndividual> result = null;
        try{
            Session session =  HibernateUtil.getSession();
            session.beginTransaction();
//            result = session.createSQLQuery(
//                    "from pago_individual").list();
            result = session.createCriteria(PagoIndividual.class)
                    .addOrder(Order.desc("pais")).list();
            session.getTransaction().commit();
            session.close();
            
        }catch(Exception e){
            Logger.getLogger(RandomDAO.class.getName())
                    .log(Level.SEVERE, null, e);
        }
        return result;
    }
    
    /**
     * Obtiene el pago Individual
     * @param inscription
     * @return 
     */
    public PagoIndividual getPagoIndividual(int inscription){
        PagoIndividual pagoIndividual =null;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            pagoIndividual =(PagoIndividual) session.createCriteria(
                    PagoIndividual.class).add(Restrictions.eq("inscripcion", inscription))
                    .addOrder(Order.desc("pais"))
                    .setMaxResults(1).uniqueResult();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            Logger.getLogger(RandomDAO.class.getName())
                    .log(Level.SEVERE, null, e);
        }
        return pagoIndividual;
    }
    
    /**
     * Consulta la base con la restriccion idincripcion.
     * 
     * @param idinscripcion
     * @return
     * @throws Exception 
     */
    public PagoIndividual getPais(int idinscripcion)throws Exception{
        
        session = HibernateUtil.getSession();
        session.beginTransaction();
        PagoIndividual pagoIndividual = (PagoIndividual)session
                .createCriteria(PagoIndividual.class)
                .add(Restrictions.eq("idinscripcion", idinscripcion))
                .uniqueResult();
        session.getTransaction().commit();
        session.close();
        return pagoIndividual;
    }
    
    /**
     * 
     * @param idinscripcion
     * @return
     * @throws Exception 
     */
    public int getPaisFilas(int idinscripcion)throws Exception{
        
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            PagoIndividual pagoIndividual = (PagoIndividual) session
                    .createCriteria(PagoIndividual.class)
                    .add(Restrictions.eq("idinscripcion", idinscripcion))
                    .uniqueResult();
            int pais = pagoIndividual.getPais();
            session.getTransaction();
            session.close();
            return pais;
        } catch (Exception e) {
            return 0;
        }
    }
    
//    /**
//     * Regresa el idinscripcion del pago individual.
//     * 
//     * @param idinscripcion del pago individual.
//     * @return idinscripcion
//     * @throws Exception en caso de no ingresar un valor entero.
//     */
//    public int getInscripcion()throws Exception{
//        System.out.println("Entro a service2");
//        Integer rows;
//        session = HibernateUtil.getSession();
//        session.beginTransaction();
//        List result = session.createCriteria(PagoIndividual.class).list();
//        rows = result.size();
//        session.getTransaction().commit();
//        session.close();
//        return rows;
//    }
    
    /**
     * Obtiene la cantidad de filas que tiene la tabla pago individual.
     * 
     * @return rows de la tabla pago individual
     * @throws Exception 
     */
    public int getRowsPagoIndividual() throws Exception {
        int rows;
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery("from PagoIndividual").list();
        //List result = session.createQuery("from PagoIndividual where idtorneo = "+5).list();
        rows=result.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }
    
    /**
     * Trunca la tabla pago individual Control.
     */
    public void truncateTable(){
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            Query q = session.createSQLQuery(
                    "truncate table pago_individual_control");
            q.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            Logger.getLogger(PagoIndividualDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Obtiene la consulta de pago unico de manera aleatoria.
     * 
     * @param pais
     * @return pago individual 
     */
    public PagoIndividual getRandom(int pais){
                    PagoIndividual individual = null;
        try {   
            session = HibernateUtil.getSession();
            session.beginTransaction();
            individual = (PagoIndividual) session.createCriteria(
                    PagoIndividual.class).add(Restrictions.eq("pais", pais)).
                    add(Restrictions.sqlRestriction("1=1 order by rand()")).
                    setMaxResults(1).uniqueResult();
            session.getTransaction().commit();
            session.close();

        } catch (Exception ex) {
            Logger.getLogger(PagoIndividualDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return individual;
    }
    
}