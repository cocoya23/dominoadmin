package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "registrotorneomodequipo")
public class RegistroTorneoModEquipo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idRTME")
    private Integer idRTME;
    
    @Column(name = "idjugador_x_torneo")
    private Integer idjugador_x_torneo;
    
    @Column(name = "idTorneo")
    private Integer idTorneo;
    
    @Column(name = "idjugador")
    private Integer idjugador;
    
    @Column(name = "activo")
    private String activo;
    
    @Column(name = "fechaRTME")
    private String fechaRTME;

    public RegistroTorneoModEquipo() {
    }

    public RegistroTorneoModEquipo(Integer idRTME, Integer idjugador_x_torneo, Integer idTorneo, Integer idjugador, String activo, String fechaRTME) {
        this.idRTME = idRTME;
        this.idjugador_x_torneo = idjugador_x_torneo;
        this.idTorneo = idTorneo;
        this.idjugador = idjugador;
        this.activo = activo;
        this.fechaRTME = fechaRTME;
    }

    public Integer getIdRTME() {
        return idRTME;
    }

    public void setIdRTME(Integer idRTME) {
        this.idRTME = idRTME;
    }

    public Integer getIdjugador_x_torneo() {
        return idjugador_x_torneo;
    }

    public void setIdjugador_x_torneo(Integer idjugador_x_torneo) {
        this.idjugador_x_torneo = idjugador_x_torneo;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getFechaRTME() {
        return fechaRTME;
    }

    public void setFechaRTME(String fechaRTME) {
        this.fechaRTME = fechaRTME;
    }
}
