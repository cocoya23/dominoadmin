package com.domino.dao;

import com.domino.bens.*;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Sergio Cano
 */
public class PagosAdDAO {

    Session session;

    public List<Torneo> getTorneos() throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List<Torneo> result = session.createCriteria(Torneo.class).add(Restrictions.eq("activoTorneo", 1)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public List<JugadorTorneo> getJugador(int id) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List<JugadorTorneo> jugadorTorneo = session.createCriteria(JugadorTorneo.class).add(Restrictions.eq("idTorneo", id)).list();
        session.getTransaction().commit();
        session.close();
        return jugadorTorneo;
    }

    public JugadorTorneo getJugadorxTorneo(Integer idTorneo, Integer idJugador) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadorTorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorneo.intValue()),
                Restrictions.eq("idjugador", idJugador.intValue()))).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return jugadorTorneo;
    }

    public int getrowsPago(Integer idTorneo, Integer idJugador) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List<PagoIndividual> pagoIndividual = session.createCriteria(PagoIndividual.class).
                add(Restrictions.and(
                Restrictions.eq("idtorneo", idTorneo),
                Restrictions.eq("idjugador", idJugador))).list();
        int rows = pagoIndividual.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }

    public FormacionPareja namePareja(Integer idTorn, Integer idJug) throws Exception {

            session = HibernateUtil.getSession();
            session.beginTransaction();
            FormacionPareja formacionpareja = (FormacionPareja) session.createCriteria(FormacionPareja.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())))).uniqueResult();
            session.getTransaction().commit();
            session.close();
            return formacionpareja;
        
    }

    public String getNombreEquipo(Integer idjugador) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        Jugador jugador = (Jugador) session.createCriteria(Jugador.class).
                add(Restrictions.eq("idjugador", idjugador.intValue())).uniqueResult();
        String name = jugador.getNombre();
        System.out.println("Nombre: 1: "+name);
        
        session.getTransaction().commit();
        session.close();
        return name;
    }

    public Torneo getTorneos(Integer idtorneo) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        Torneo torneo = (Torneo) session.createCriteria(Torneo.class).add(Restrictions.eq("idTorneo", idtorneo)).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return torneo;
    }

    public FormacionEquipo formEquipo(Integer idTorn, Integer idJug) throws Exception {
        System.out.println("FormEquipo torneo" + idTorn);
        System.out.println("FormEquipo jugador" + idJug);
            session = HibernateUtil.getSession();
            session.beginTransaction();
            FormacionEquipo formacionEquipo = (FormacionEquipo) session.createCriteria(FormacionEquipo.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador3", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador4", idJug.intValue())))).uniqueResult();
            session.getTransaction().commit();
            session.close();

        return formacionEquipo;
    }

    public Integer equipoRows(Integer idTorn, Integer idJug) throws Exception {
        System.out.println("FormEquipo torneo" + idTorn);
        System.out.println("FormEquipo jugador" + idJug);
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(FormacionEquipo.class).
                add(Restrictions.or(
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador1", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador2", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador3", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador4", idJug.intValue())))).list();
        Integer rows = result.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }

    public List<FormacionEquipo> getFormEquipo() throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List<FormacionEquipo> result = session.createCriteria(FormacionEquipo.class).add(Restrictions.eq("idtorneo", 3)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public Integer getMaxControl(Integer idtorneo) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(PagoControl.class).
                add(Restrictions.eq("idTorneo", idtorneo)).setProjection(Projections.max("idControl"));
        Integer maxidcontrol = (Integer) criteria.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return maxidcontrol;
    }

    public void insertPagoControl(PagoControl pagoControl) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(pagoControl);
        session.getTransaction().commit();
        session.close();

    }

    public void insertTorneoControl(TorneoControl torneoControl) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(torneoControl);
        session.getTransaction().commit();
        session.close();

    }

    public Integer getPAgoControl() throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(TorneoControl.class).setProjection(Projections.max("idTorneoControl"));
        Integer maxidcontrol = (Integer) criteria.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return maxidcontrol;
    }

    public void insertPagoIndividual(PagoIndividual pagoIndividual) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(pagoIndividual);
        session.getTransaction().commit();
        session.close();
    }

    public void insertPagoParejaPaso(PagoParejaPaso pagoParejaPaso) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(pagoParejaPaso);
        session.getTransaction().commit();
        session.close();
    }

    public void insertPagoEquipoPaso(PagoEquipoPaso pagoEquipoPaso) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(pagoEquipoPaso);
        session.getTransaction().commit();
        session.close();
    }

    public Jugador getJugador(Integer idJugador) {
        Jugador jugador = null;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            jugador = (Jugador) session.createCriteria(Jugador.class).add(Restrictions.eq(
                    "idjugador", idJugador)).uniqueResult();
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            Logger.getLogger(MesaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jugador;
    }

    public FormacionPareja getformacionPareja(Integer idTorn, Integer idJug) throws Exception {
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            FormacionPareja formacionpareja = (FormacionPareja) session.createCriteria(FormacionPareja.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())))).uniqueResult();
            session.getTransaction().commit();
            session.close();
            return formacionpareja;
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

    public List<PagoParejaPaso> getListParejaPaso(Integer idTorneo, Integer Idformacionpareja) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List<PagoParejaPaso> result = session.createCriteria(PagoParejaPaso.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorneo.intValue()),
                Restrictions.eq("idFormacionPareja", Idformacionpareja.intValue()))).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public void insertPagoPareja(PagoPareja pagoPareja) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(pagoPareja);
        session.getTransaction().commit();
        session.close();
    }

    public List<PagoEquipoPaso> getListEquipoPaso(Integer idTorneo, Integer Idformacionequipo) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List<PagoEquipoPaso> result = session.createCriteria(PagoEquipoPaso.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorneo.intValue()),
                Restrictions.eq("idformacionequipo", Idformacionequipo.intValue()))).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public void insertPagoEquipo(PagoEquipo pagoEquipo) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(pagoEquipo);
        session.getTransaction().commit();
        session.close();
    }

    public int getrowsPagoControl(Integer idTorneo, Integer idJugador) throws Exception {
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List<PagoControl> pagoControl = session.createCriteria(PagoControl.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorneo),
                Restrictions.eq("idJugador", idJugador))).list();
        int rows = pagoControl.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }
}
