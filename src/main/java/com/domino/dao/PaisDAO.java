package com.domino.dao;

import com.domino.bens.Pais;
import com.domino.util.HibernateUtil;
import java.util.List;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author Sergio Cano
 */
public class PaisDAO {

    public List<Pais> getPaises() throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery("from Pais").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public void insertPaises(Pais pais) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(pais);
        session.getTransaction().commit();
        session.close();

    }

    public List<Pais> getPais() throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<Pais> result = session.createQuery("from Pais order by nombre").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public void updatePaises(Pais pais) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(pais);
        session.getTransaction().commit();
        session.close();

    }

    public void deletepaisSP(Integer idPais) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        SQLQuery q = session.createSQLQuery("CALL sp_deletepais(:id)");
        q.setParameter("id", idPais);
        q.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
