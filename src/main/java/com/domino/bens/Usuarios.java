/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.bens;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author A. Eliud Uriostegui Glez.
 */
@Entity
@Table (name="usuarios")
public class Usuarios implements Serializable {
    
    @Id
    @GeneratedValue (strategy= GenerationType.IDENTITY)
    @Column (name="idUsuario")
    private Integer idUsuario;
    @Column (name="idJugador")
    private Integer idJugador;
    @Column (name="correo")
    private String correo;
    @Column (name="psw")
    private String psw;
    @Column (name="roll")
    private Integer roll;
    @Column (name="fechaAlta")
    @Temporal (javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaAlta;

    public Usuarios() {
    }

    public Usuarios(Integer idUsuario, Integer idJugador, String correo, 
            String psw, Integer roll, Date fechaAlta) {
        this.idUsuario = idUsuario;
        this.idJugador = idJugador;
        this.correo = correo;
        this.psw = psw;
        this.roll = roll;
        this.fechaAlta = fechaAlta;
    }
    
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Integer getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(Integer idJugador) {
        this.idJugador = idJugador;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public Integer getRoll() {
        return roll;
    }

    public void setRoll(Integer roll) {
        this.roll = roll;
    }
}