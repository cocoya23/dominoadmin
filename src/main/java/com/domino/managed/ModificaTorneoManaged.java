/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.managed;

import com.domino.bens.Metropoli;
import com.domino.bens.Pais;
import com.domino.bens.Torneo;
import com.domino.service.MetropoliService;
import com.domino.service.PaisService;
import com.domino.service.TorneoService;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Sergio Cano
 */
@ManagedBean
@ViewScoped
public class ModificaTorneoManaged implements Serializable {

    @ManagedProperty("#{paisService}")
    private PaisService paisService;
    @ManagedProperty("#{metropoliService}")
    private MetropoliService metropoliService;
    @ManagedProperty("#{torneoService}")
    private TorneoService torneoService;
    private List<SelectItem> metropolis;
    private List<SelectItem> paises;
    private List<Torneo> torneos;
    //Variable de control if
    private Integer varpais;
    private Integer varformato;
    private Integer varformatopareja;
    private Integer varmultibolsa;
    private Integer varmodifica;
    private Integer varmultibolsapareja;
    //Datos del torneo
    private String nombre;
    private Integer ambito;
    private Integer paisID;
    private Integer metropoliID;
    private String sede;
    private Date fechainitor;
    private Date fechafintor;
    private Integer genero;
    private Integer formato;
    private Integer formatopareja;
    private Integer multibolsa;
    private Integer multibolsapareja;
    //Datos de la modalidad
    //Individual
    private boolean individual;
    private String parind;
    private String insind;
    private String metaind;
    private String timeind;
    private String bolsa1ind;
    private String bolsa2ind;
    private String bolsa3ind;
    //pareja
    private boolean pareja;
    private String parpar;
    private String inspar;
    private String metapar;
    private String timepar;
    private String bolsa1par;
    private String bolsa2par;
    private String bolsa3par;
    //equipo
    private boolean equipo;
    private String parequ;
    private String insequ;
    private String metaequ;
    private String timeequ;
    private String bolsa1equ;
    private String bolsa2equ;
    private String bolsa3equ;
    //fechas de inscripcion
    private Date fechainiinter;
    private Date fechafininter;
    private Date fechainisede;
    private Date fechafinsede;
    Torneo torneo;
    //ModificaTorneo
    private Integer idTorneo;
    private Integer formatoequipo;

    @PostConstruct
    public void init() {

        try {
            torneos = torneoService.getTorneos();

            varmodifica = 0;


        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void cargaPais() {
        paises = new ArrayList(0);
        metropolis = new ArrayList(0);
        try {
            if (ambito == 2) {
                for (Pais pais : paisService.getPaises()) {
                    paises.add(new SelectItem(pais.getId(), pais.getNombre()));
                }
                for (Metropoli metropoli : metropoliService.getMetropoli()) {
                    metropolis.add(new SelectItem(metropoli.getIdMetropoli(), metropoli.getMetropoli()));
                }
                varpais = 1;
                paisID = 1;
                metropoliID = 93;
            } else {
                for (Metropoli metropoli : metropoliService.getMetropoli()) {
                    metropolis.add(new SelectItem(metropoli.getIdMetropoli(), metropoli.getMetropoli()));
                }
                varpais = 0;
                metropoliID = 93;
            }
        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void formatoMaya() {
        if (formato == 4) {
            metaind = "4 ganados o más con dos de ventaja";
        } else {
            metaind = "0";
        }

    }

    public void formatoMayapar() {

        if (formatopareja == 4) {
            metapar = "4 ganados o más con dos de ventaja";
        } else {
            metapar = "0";
        }

    }

    public void formatoMayaequ() {

        if (formatoequipo == 4) {
            metaequ = "4 ganados o más con dos de ventaja";
        } else {
            metaequ = "0";
        }
    }

    public void cargaCiudades() {
        System.out.println(paisID);
        metropolis = new ArrayList(0);
        try {

            for (Metropoli metropoli : metropoliService.getMetropoliId(paisID)) {
                metropolis.add(new SelectItem(metropoli.getIdMetropoli(), metropoli.getMetropoli()));
            }


        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cargaBolsa() {

        try {

            if (formato == 1) {
                varformato = 0;
                varmultibolsa = 0;
            } else {
                varformato = 1;
                varmultibolsa = 1;
            }

        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cargaMultibolsa() {

        try {

            if (multibolsa == 1) {
                varmultibolsa = 1;
            } else {
                varmultibolsa = 2;
            }

        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cargaBolsapareja() {

        try {

            if (formatopareja == 1) {
                varformatopareja = 0;
                varmultibolsapareja = 0;
            } else {
                varformatopareja = 1;
                varmultibolsapareja = 1;
            }

        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cargaMultibolsapareja() {

        try {
            if (multibolsapareja == 1) {
                varmultibolsapareja = 1;
            } else {
                varmultibolsapareja = 2;
            }

        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cargaTorneo() {
        paises = new ArrayList(0);
        metropolis = new ArrayList(0);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Map params = facesContext.getExternalContext().getRequestParameterMap();
            idTorneo = new Integer((String) params.get("idTorneo"));
            for (Torneo torneo : torneoService.getTorneoxID(idTorneo)) {
                nombre = torneo.getTorneo();
                ambito = torneo.getAmbito();
                if (ambito == 1) {
                    varpais = 0;
                    for (Metropoli metropoli : metropoliService.getMetropoli()) {
                        metropolis.add(new SelectItem(metropoli.getIdMetropoli(), metropoli.getMetropoli()));
                    }
                } else {
                    varpais = 1;
                    paisID = torneo.getIdPais();
                    for (Pais pais : paisService.getPaises()) {
                        paises.add(new SelectItem(pais.getId(), pais.getNombre()));
                    }
                    for (Metropoli metropoli : metropoliService.getMetropoliId(paisID)) {
                        metropolis.add(new SelectItem(metropoli.getIdMetropoli(), metropoli.getMetropoli()));
                    }
                }
                metropoliID = torneo.getIdMetropoli();
                sede = torneo.getSede();
                fechainitor = formatter.parse(torneo.getFechaHoraio());
                fechafintor = formatter.parse(torneo.getFechahorariofin());
                genero = torneo.getGenero();



                individual = modalidad2(torneo.getModInd());
                parind = String.valueOf(torneo.getPartidaind());
                insind = String.valueOf(torneo.getInsind());
                metaind = String.valueOf(torneo.getMetaind());
                timeind = String.valueOf(torneo.getTiempoPartidaind());
                bolsa1ind = String.valueOf(torneo.getModIndB1());
                bolsa2ind = String.valueOf(torneo.getModIndB2());
                bolsa3ind = String.valueOf(torneo.getModIndB3());

                pareja = modalidad2(torneo.getModPareja());
                parpar = String.valueOf(torneo.getPartidapar());
                inspar = String.valueOf(torneo.getInspar());
                metapar = String.valueOf(torneo.getMetapar());
                timepar = String.valueOf(torneo.getTiempoPartidapar());
                bolsa1par = String.valueOf(torneo.getModParejaB1());
                bolsa2par = String.valueOf(torneo.getModParejaB2());
                bolsa3par = String.valueOf(torneo.getModParejaB3());

                equipo = modalidad2(torneo.getModEquipo());
                parequ = String.valueOf(torneo.getPartidaequ());
                insequ = String.valueOf(torneo.getInsequ());
                metaequ = String.valueOf(torneo.getMetaequ());
                timeequ = String.valueOf(torneo.getTiempoPartidaequ());
                bolsa1equ = String.valueOf(torneo.getModEquipoB1());
                bolsa2equ = String.valueOf(torneo.getModEquipoB2());
                bolsa3equ = String.valueOf(torneo.getModEquipoB3());

                fechainiinter = formatter.parse(torneo.getHorarioIntDesde());
                fechafininter = formatter.parse(torneo.getHorarioIntHasta());
                fechainisede = formatter.parse(torneo.getHorarioSede());
                fechafinsede = formatter.parse(torneo.getHorarioSedeHasta());

                formato = torneo.getFormato();
                multibolsa = torneo.getNumeroMult();
                if (multibolsa == 2) {
                    formato = 3;
                }
                if (multibolsa == 1) {
                    formato = 2;
                }

                if (formato == 4) {
                    metaind = "4 ganados o más con dos de ventaja";
                }
                formatopareja = torneo.getFormatopar();
                multibolsapareja = torneo.getNumeroMultpar();
                if (multibolsapareja == 2) {
                    formatopareja = 3;
                }
                if (multibolsapareja == 1) {
                    formatopareja = 2;
                }
                if (formatopareja == 4) {
                    metapar = "4 ganados o más con dos de ventaja";
                }
                formatoequipo = torneo.getFotmatoequ();
                if (formatoequipo == 4) {
                    metaequ = "4 ganados o más con dos de ventaja";
                }


            }


            varmodifica = 1;
        } catch (Exception ex) {
            Logger.getLogger(ModificaTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void eliminaTor() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Map params = facesContext.getExternalContext().getRequestParameterMap();
            idTorneo = new Integer((String) params.get("idTorneo"));
            torneoService.deleteTorneoSP(idTorneo);
            /**
             * torneoService.deleteFormacionequipo(idTorneo);
             * torneoService.deleteRegistroTorneoModEquipo(idTorneo);
             * torneoService.deleteFormacionpareja(idTorneo);
             * torneoService.deleteRegistroTorneoModPareja(idTorneo);
             * torneoService.deleteJugadorTorneo(idTorneo);
             * torneoService.deleteTorneo(idTorneo);*
             */
            context.addMessage(null, new FacesMessage("Exito", "Se elimino el torneo "));
            torneos = torneoService.getTorneos();
            varmodifica = 0;
            FacesContext contex = FacesContext.getCurrentInstance();
            contex.getExternalContext().redirect("modificatorneo.xhtml");
        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "No se pudo eliminar el torneo"));
        }
    }

    public void updateTor() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            Integer idp = paisVar(ambito);
            String fecini = fecha(fechainitor);
            Integer ind = modalidad(individual);
            Integer par = modalidad(pareja);
            Integer equ = modalidad(equipo);
            float b1i = modalidaBolsa(bolsa1ind);
            float b2i = modalidaBolsa(bolsa2ind);
            float b3i = modalidaBolsa(bolsa3ind);
            float b1p = modalidaBolsa(bolsa1par);
            float b2p = modalidaBolsa(bolsa2par);
            float b3p = modalidaBolsa(bolsa3par);
            float b1e = modalidaBolsa(bolsa1equ);
            float b2e = modalidaBolsa(bolsa2equ);
            float b3e = modalidaBolsa(bolsa3equ);
            Integer mei = meta(metaind);
            Integer mep = meta(metapar);
            Integer mee = meta(metaequ);
            int pari = meta(timeind);
            int parp = meta(timepar);
            int pare = meta(timeequ);
            String fecfintor = fecha(fechafintor);
            String feciniinter = fecha(fechainiinter);
            String fecfininter = fecha(fechafininter);
            String fecinisede = fecha(fechainisede);
            String fecfinsede = fecha(fechafinsede);
            int partidasi = meta(parind);
            int partidasp = meta(parpar);
            int partidase = meta(parequ);
            float insi = modalidaBolsa(insind);
            float insp = modalidaBolsa(inspar);
            float inse = modalidaBolsa(insequ);
            Integer multi = 0;
            Integer form = 0;
            Integer formp = 0;
            Integer multipareja = 0;
            if (formato == 1) {
                multi = 0;
                form = 1;
                b3i = 0;
                b2i = 0;
            }
            if (formato == 2) {
                multi = 1;
                form = 2;
                b3i = 0;

            }
            if (formato == 3) {
                multi = 2;
                form = 2;
            }

            if (formatopareja == 1) {
                multipareja = 0;
                formp = 1;
                b2p = 0;
                b3p = 0;
            }
            if (formatopareja == 2) {
                multipareja = 1;
                formp = 2;
                b3p = 0;
            }
            if (formatopareja == 3) {
                multipareja = 2;
                formp = 2;
            }
            if (formatoequipo == 4) {
                mee = null;
                formatoequipo=4;
                
            }
            if (formatopareja == 4) {
                mep = null;
                formp=4;
            }
            if (formato == 4) {
                mei = null;
                form=4;
            }
            torneo = new Torneo(idTorneo, nombre, ambito, idp, metropoliID, sede, genero, fecini, form, multi, ind, b1i, b2i, b3i, par, b1p, b2p, b3p, equ, b1e, b2e, b3e, mei, pari, feciniinter, fecfininter, fecinisede, 1, 1, partidasi, insi, partidasp, insp, partidase, inse, fecfinsede, mep, parp, mee, pare, fecfintor, formp, multipareja, formatoequipo);
            torneoService.updateJugador(torneo);
            if (pareja == false) {
                torneoService.parejaSP(idTorneo);

            }

            if (equipo == false) {
                torneoService.equipoSP(idTorneo);
            }
            torneos = torneoService.getTorneos();
            varmodifica = 0;
            context.addMessage(null, new FacesMessage("Exito", "Se actualizo torneo " + nombre));
            FacesContext contex = FacesContext.getCurrentInstance();
            contex.getExternalContext().redirect("modificatorneo.xhtml");



        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "No se pudo actualizar el torneo"));

        }

    }

    public int paisVar(Integer amb) {
        if (amb == 1) {
            return 1;
        } else {
            return paisID;
        }

    }

    public Integer multibo(Integer mul) {
        try {
            if (mul == 1) {
                return mul;
            }
            if (mul == 2) {
                return mul;
            } else {
                return 0;
            }

        } catch (Exception e) {
            return 0;

        }

    }

    public Integer modalidad(boolean var) {
        if (var == true) {
            return 1;
        } else {
            return 0;
        }

    }

    public boolean modalidad2(Integer var) {
        if (var == 1) {
            return true;
        } else {
            return false;
        }

    }

    public String fecha(Date var) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechaCadena = sdf.format(var);
        return fechaCadena;
    }

    public float modalidaBolsa(String var) {
        try {
            Float bolsa = Float.parseFloat(var);
            return bolsa;
        } catch (Exception e) {
            return 0;

        }
    }

    public int meta(String var) {
        try {
            int bolsa = Integer.parseInt(var);
            return bolsa;
        } catch (Exception e) {
            return 0;

        }
    }

    public Integer getAmbito() {
        return ambito;
    }

    public void setAmbito(Integer ambito) {
        this.ambito = ambito;
    }

    public String getBolsa1equ() {
        return bolsa1equ;
    }

    public void setBolsa1equ(String bolsa1equ) {
        this.bolsa1equ = bolsa1equ;
    }

    public String getBolsa1ind() {
        return bolsa1ind;
    }

    public void setBolsa1ind(String bolsa1ind) {
        this.bolsa1ind = bolsa1ind;
    }

    public String getBolsa1par() {
        return bolsa1par;
    }

    public void setBolsa1par(String bolsa1par) {
        this.bolsa1par = bolsa1par;
    }

    public String getBolsa2equ() {
        return bolsa2equ;
    }

    public void setBolsa2equ(String bolsa2equ) {
        this.bolsa2equ = bolsa2equ;
    }

    public String getBolsa2ind() {
        return bolsa2ind;
    }

    public void setBolsa2ind(String bolsa2ind) {
        this.bolsa2ind = bolsa2ind;
    }

    public String getBolsa2par() {
        return bolsa2par;
    }

    public void setBolsa2par(String bolsa2par) {
        this.bolsa2par = bolsa2par;
    }

    public String getBolsa3equ() {
        return bolsa3equ;
    }

    public void setBolsa3equ(String bolsa3equ) {
        this.bolsa3equ = bolsa3equ;
    }

    public String getBolsa3ind() {
        return bolsa3ind;
    }

    public void setBolsa3ind(String bolsa3ind) {
        this.bolsa3ind = bolsa3ind;
    }

    public String getBolsa3par() {
        return bolsa3par;
    }

    public void setBolsa3par(String bolsa3par) {
        this.bolsa3par = bolsa3par;
    }

    public boolean isEquipo() {
        return equipo;
    }

    public void setEquipo(boolean equipo) {
        this.equipo = equipo;
    }

    public Date getFechafininter() {
        return fechafininter;
    }

    public void setFechafininter(Date fechafininter) {
        this.fechafininter = fechafininter;
    }

    public Date getFechafinsede() {
        return fechafinsede;
    }

    public void setFechafinsede(Date fechafinsede) {
        this.fechafinsede = fechafinsede;
    }

    public Date getFechafintor() {
        return fechafintor;
    }

    public void setFechafintor(Date fechafintor) {
        this.fechafintor = fechafintor;
    }

    public Date getFechainiinter() {
        return fechainiinter;
    }

    public void setFechainiinter(Date fechainiinter) {
        this.fechainiinter = fechainiinter;
    }

    public Date getFechainisede() {
        return fechainisede;
    }

    public void setFechainisede(Date fechainisede) {
        this.fechainisede = fechainisede;
    }

    public Date getFechainitor() {
        return fechainitor;
    }

    public void setFechainitor(Date fechainitor) {
        this.fechainitor = fechainitor;
    }

    public Integer getFormato() {
        return formato;
    }

    public void setFormato(Integer formato) {
        this.formato = formato;
    }

    public Integer getGenero() {
        return genero;
    }

    public void setGenero(Integer genero) {
        this.genero = genero;
    }

    public boolean isIndividual() {
        return individual;
    }

    public void setIndividual(boolean individual) {
        this.individual = individual;
    }

    public String getInsequ() {
        return insequ;
    }

    public void setInsequ(String insequ) {
        this.insequ = insequ;
    }

    public String getInsind() {
        return insind;
    }

    public void setInsind(String insind) {
        this.insind = insind;
    }

    public String getInspar() {
        return inspar;
    }

    public void setInspar(String inspar) {
        this.inspar = inspar;
    }

    public String getMetaequ() {
        return metaequ;
    }

    public void setMetaequ(String metaequ) {
        this.metaequ = metaequ;
    }

    public String getMetaind() {
        return metaind;
    }

    public void setMetaind(String metaind) {
        this.metaind = metaind;
    }

    public String getMetapar() {
        return metapar;
    }

    public void setMetapar(String metapar) {
        this.metapar = metapar;
    }

    public Integer getMetropoliID() {
        return metropoliID;
    }

    public void setMetropoliID(Integer metropoliID) {
        this.metropoliID = metropoliID;
    }

    public MetropoliService getMetropoliService() {
        return metropoliService;
    }

    public void setMetropoliService(MetropoliService metropoliService) {
        this.metropoliService = metropoliService;
    }

    public List<SelectItem> getMetropolis() {
        return metropolis;
    }

    public void setMetropolis(List<SelectItem> metropolis) {
        this.metropolis = metropolis;
    }

    public Integer getMultibolsa() {
        return multibolsa;
    }

    public void setMultibolsa(Integer multibolsa) {
        this.multibolsa = multibolsa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPaisID() {
        return paisID;
    }

    public void setPaisID(Integer paisID) {
        this.paisID = paisID;
    }

    public PaisService getPaisService() {
        return paisService;
    }

    public void setPaisService(PaisService paisService) {
        this.paisService = paisService;
    }

    public List<SelectItem> getPaises() {
        return paises;
    }

    public void setPaises(List<SelectItem> paises) {
        this.paises = paises;
    }

    public boolean isPareja() {
        return pareja;
    }

    public void setPareja(boolean pareja) {
        this.pareja = pareja;
    }

    public String getParequ() {
        return parequ;
    }

    public void setParequ(String parequ) {
        this.parequ = parequ;
    }

    public String getParind() {
        return parind;
    }

    public void setParind(String parind) {
        this.parind = parind;
    }

    public String getParpar() {
        return parpar;
    }

    public void setParpar(String parpar) {
        this.parpar = parpar;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getTimeequ() {
        return timeequ;
    }

    public void setTimeequ(String timeequ) {
        this.timeequ = timeequ;
    }

    public String getTimeind() {
        return timeind;
    }

    public void setTimeind(String timeind) {
        this.timeind = timeind;
    }

    public String getTimepar() {
        return timepar;
    }

    public void setTimepar(String timepar) {
        this.timepar = timepar;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public Integer getVarformato() {
        return varformato;
    }

    public void setVarformato(Integer varformato) {
        this.varformato = varformato;
    }

    public Integer getVarmultibolsa() {
        return varmultibolsa;
    }

    public void setVarmultibolsa(Integer varmultibolsa) {
        this.varmultibolsa = varmultibolsa;
    }

    public Integer getVarpais() {
        return varpais;
    }

    public void setVarpais(Integer varpais) {
        this.varpais = varpais;
    }

    public TorneoService getTorneoService() {
        return torneoService;
    }

    public void setTorneoService(TorneoService torneoService) {
        this.torneoService = torneoService;
    }

    public List<Torneo> getTorneos() {
        return torneos;
    }

    public void setTorneos(List<Torneo> torneos) {
        this.torneos = torneos;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public Integer getVarmodifica() {
        return varmodifica;
    }

    public void setVarmodifica(Integer varmodifica) {
        this.varmodifica = varmodifica;
    }

    public Integer getFormatopareja() {
        return formatopareja;
    }

    public void setFormatopareja(Integer formatopareja) {
        this.formatopareja = formatopareja;
    }

    public Integer getMultibolsapareja() {
        return multibolsapareja;
    }

    public void setMultibolsapareja(Integer multibolsapareja) {
        this.multibolsapareja = multibolsapareja;
    }

    public Integer getVarformatopareja() {
        return varformatopareja;
    }

    public void setVarformatopareja(Integer varformatopareja) {
        this.varformatopareja = varformatopareja;
    }

    public Integer getVarmultibolsapareja() {
        return varmultibolsapareja;
    }

    public void setVarmultibolsapareja(Integer varmultibolsapareja) {
        this.varmultibolsapareja = varmultibolsapareja;
    }

    public Integer getFormatoequipo() {
        return formatoequipo;
    }

    public void setFormatoequipo(Integer formatoequipo) {
        this.formatoequipo = formatoequipo;
    }
}
