package com.domino.dao;

import com.domino.bens.PagoIndividual;
import com.domino.bens.PagoIndividualControl;
import com.domino.bens.Torneo;
import com.domino.util.HibernateUtil;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class RandomDAO {

    Session session;
    private Integer idtorneo;

    public PagoIndividual getRandom() {
        PagoIndividual pagoIndividual = null;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            pagoIndividual = (PagoIndividual) session.createCriteria(PagoIndividual.class).
                    add(Restrictions.eq("idtorneo", idtorneo)).add(Restrictions.sqlRestriction("1=1 order by rand()")).setMaxResults(1).uniqueResult();
            session.getTransaction().commit();
            session.close();

        } catch (Exception ex) {
            Logger.getLogger(RandomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pagoIndividual;
    }

    public Integer getPagoIndividualControl(Integer idins) {
        Integer rows = 0;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createCriteria(PagoIndividualControl.class).add(Restrictions.eq("idinscripcion", idins)).list();
            rows = result.size();
            session.getTransaction().commit();
            session.close();

        } catch (Exception ex) {
            Logger.getLogger(RandomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rows;
    }

    public void savePagoIndC(PagoIndividualControl pagoIndividualControl) {

        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            session.save(pagoIndividualControl);
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            Logger.getLogger(RandomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public Integer getDistinc() {
        Integer rows = 0;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createCriteria(PagoIndividual.class).setProjection(Projections.distinct(Projections.property("pais"))).list();
            session.getTransaction().commit();
            session.close();
            rows = result.size();

        } catch (Exception ex) {
            Logger.getLogger(RandomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rows;
    }

    public Integer getDistincMetropoli() {
        Integer rows = 0;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createCriteria(PagoIndividual.class).setProjection(Projections.distinct(Projections.property("metropoli"))).list();
            session.getTransaction().commit();
            session.close();
            rows = result.size();

        } catch (Exception ex) {
            Logger.getLogger(RandomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rows;
    }

    public Integer getDistincClub() {
        Integer rows = 0;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createCriteria(PagoIndividual.class).setProjection(Projections.distinct(Projections.property("club"))).list();
            session.getTransaction().commit();
            session.close();
            rows = result.size();

        } catch (Exception ex) {
            Logger.getLogger(RandomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rows;
    }

    public Integer getDistincTotal() {
        Integer rows = 0;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createCriteria(PagoIndividual.class).setProjection(Projections.distinct(Projections.property("metropoli"))).setProjection(Projections.distinct(Projections.property("pais"))).list();
            session.getTransaction().commit();
            session.close();
            rows = result.size();

        } catch (Exception ex) {
            Logger.getLogger(RandomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rows;
    }

    public int getRowsPagoIndividual() throws Exception {
        int rows;
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery(
                "from PagoIndividual where idtorneo =" + idtorneo).list();
        rows = result.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }

    public int getRowsPagoIndividualControl() throws Exception {
        int rows;
        session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery(
                "from PagoIndividualControl where idtorneo = " + idtorneo).list();
        rows = result.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }

    public List<Torneo> getTorneos() throws Exception {
        session.beginTransaction();
        List<Torneo> result = session.createCriteria(Torneo.class).add(Restrictions.eq("activoTorneo", 1)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public PagoIndividualControl getPagoControlInd(Integer id) throws Exception {
        PagoIndividualControl pagoIndividualControl;
        session = HibernateUtil.getSession();
        session.beginTransaction();
        pagoIndividualControl = (PagoIndividualControl) session.createCriteria(PagoIndividualControl.class).
                add(Restrictions.eq("idinscripcion", id)).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return pagoIndividualControl;
    }
    
    
    public void insertPagoIndCont(PagoIndividualControl pagoIndividualControl) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.update(pagoIndividualControl);
     
        session.getTransaction().commit();
        session.close();
    }
}
