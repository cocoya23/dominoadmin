package com.domino.bens;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
@Table(name = "jugadores")
public class Jugador implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idjugador")
    private Integer idjugador;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "email")
    private String email;
    @Column(name = "psw")
    private String psw;
    @Column(name = "movil")
    private String movil;
    @Column(name = "telefonoLocal")
    private String telefonoLocal;
    @Column(name = "pais")
    private String pais;
    @Column(name = "metropoli")
    private String metropoli;
    @Column(name = "Club")
    private String club;
    @Column(name = "genero")
    private char genero;
    @Column(name = "fechaNacimiento")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    @Column(name = "discapacitado")
    private String discapacitado;
    @Column(name = "identificacionFoto")
    private String identificacionFoto;
    @Column(name = "numeroIdentificacion")
    private String numeroIdentificacion;
    @Column(name = "rfc")
    private String rfc;
    @Column(name = "curp")
    private String curp;
    @OneToMany(mappedBy="jugador")
    private Set<JugadorTorneo> jugadorTorneo;

    public Jugador() {
    }

    public Jugador(Integer idjugador, String nombre, String email, String psw, String movil, String telefonoLocal, String pais, String metropoli, String club, char genero, Date fechaNacimiento, String discapacitado, String identificacion, String numeroIdentificacion, String rfc, String curp) {
        this.idjugador = idjugador;
        this.nombre = nombre;
        this.email = email;
        this.psw = psw;
        this.movil = movil;
        this.telefonoLocal = telefonoLocal;
        this.pais = pais;
        this.metropoli = metropoli;
        this.club = club;
        this.genero = genero;
        this.fechaNacimiento = fechaNacimiento;
        this.discapacitado = discapacitado;
        this.identificacionFoto = identificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.rfc = rfc;
        this.curp = curp;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getTelefonoLocal() {
        return telefonoLocal;
    }

    public void setTelefonoLocal(String telefonoLocal) {
        this.telefonoLocal = telefonoLocal;
    }

    public String getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(String metropoli) {
        this.metropoli = metropoli;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

 

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }



    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    

    public String getDiscapacitado() {
        return discapacitado;
    }

    public void setDiscapacitado(String discapacitado) {
        this.discapacitado = discapacitado;
    }

    public String getIdentificacionFoto() {
        return identificacionFoto;
    }

    public void setIdentificacionFoto(String identificacionFoto) {
        this.identificacionFoto = identificacionFoto;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public Set<JugadorTorneo> getJugadorTorneo() {
        return jugadorTorneo;
    }

    public void setJugadorTorneo(Set<JugadorTorneo> jugadorTorneo) {
        this.jugadorTorneo = jugadorTorneo;
    }
    
    

   
    
    
}
