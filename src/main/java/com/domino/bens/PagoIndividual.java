package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "pago_individual")
public class PagoIndividual implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idinscripcion")
    private Integer idinscripcion;
    @Column(name = "idjugador")
    private Integer idjugador;
    @Column(name = "idtorneo")
    private Integer idtorneo;
    @Column(name = "pais")
    private Integer pais;
    @Column(name = "metropoli")
    private Integer metropoli;
    @Column(name = "club")
    private String club;
    @Column(name = "activo")
    private Integer activo;
    @Column(name = "fecha")
    private String fecha;
    @Column(name = "tipo_pago")
    private Integer tipo_pago;
    @Column(name = "id_capturista")
    private Integer id_capturista;
    @Column(name = "identificacion")
    private String identificacion;
    @Column(name = "noidentificacion")
    private String noidentificacion;
    @Column(name = "comentarios")
    private String comentarios;

    public PagoIndividual() {
    }

    public PagoIndividual(Integer idinscripcion, Integer idjugador, Integer idtorneo, Integer pais, Integer metropoli, String club, Integer activo, String fecha,
            Integer tipo_pago, Integer id_capturista, String identificacion, String noidentificacion, String comentarios) {
        this.idinscripcion = idinscripcion;
        this.idjugador = idjugador;
        this.idtorneo = idtorneo;
        this.pais = pais;
        this.metropoli = metropoli;
        this.club = club;
        this.activo = activo;
        this.fecha = fecha;
        this.tipo_pago = tipo_pago;
        this.id_capturista = id_capturista;
        this.identificacion = identificacion;
        this.noidentificacion = noidentificacion;
        this.comentarios = comentarios;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getId_capturista() {
        return id_capturista;
    }

    public void setId_capturista(Integer id_capturista) {
        this.id_capturista = id_capturista;
    }

    public Integer getIdinscripcion() {
        return idinscripcion;
    }

    public void setIdinscripcion(Integer idinscripcion) {
        this.idinscripcion = idinscripcion;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public Integer getTipo_pago() {
        return tipo_pago;
    }

    public void setTipo_pago(Integer tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNoidentificacion() {
        return noidentificacion;
    }

    public void setNoidentificacion(String noidentificacion) {
        this.noidentificacion = noidentificacion;
    }
}
