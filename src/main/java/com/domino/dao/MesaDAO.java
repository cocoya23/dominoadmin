package com.domino.dao;

import com.domino.bens.Jugador;
import com.domino.bens.Metropoli;
import com.domino.bens.PagoIndividualControl;
import com.domino.bens.Pais;
import com.domino.util.HibernateUtil;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Esta clase accede a la información de pago individual Control y jugadores. y
 * se obtendra la siguiente información.
 *
 * <ol> <il>Cantidad de pagos registrados por torneo.</il> <il>Mesa y jugadores
 * que se encuentran el dicha mesa en un
 * <code>list</code></il> <il>Torneo</il> </ol>
 *
 * @author A. Eliud Uriostegui Glez.
 * @version 1.0.0
 */
public class MesaDAO {

    /*
     * Abrimos sesión
     */
    Session session;

    /**
     * Obtiene la cantidad de filas de la tabla pago individual control.
     *
     * @param idTorneo
     * @return
     */
    public Integer getRows(Integer idTorneo) {
        Integer rows = 0;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createCriteria(PagoIndividualControl.class).add(Restrictions.eq("idtorneo", idTorneo)).list();
            rows = result.size();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            Logger.getLogger(RandomDAO.class.getName()).log(
                    Level.SEVERE, null, e);
        }
        return rows;
    }

    public List<PagoIndividualControl> getList() {
        List<PagoIndividualControl> result = null;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            //result = session.createSQLQuery("from pago_individual_control")
            //      .list();
            result = session.createCriteria(PagoIndividualControl.class).list();
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            Logger.getLogger(MesaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public Jugador getJugador(int idJugador) {
        Jugador jugador = null;
        try {
            session = HibernateUtil.getSession();
            session.beginTransaction();
            jugador = (Jugador) session.createCriteria(Jugador.class).add(Restrictions.eq(
                    "idjugador", idJugador)).uniqueResult();
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            Logger.getLogger(MesaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jugador;
    }

    public String nameMetropoli(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Metropoli metropoli = (Metropoli) session.createCriteria(Metropoli.class).
                add(Restrictions.eq("idMetropoli", id)).uniqueResult();
        String name = metropoli.getMetropoli();
        session.getTransaction().commit();
        session.close();
        return name;
    }
    
    public String namePais(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Pais pais = (Pais) session.createCriteria(Pais.class).
                add(Restrictions.eq("id", id)).uniqueResult();
        String name = pais.getNombre();
        session.getTransaction().commit();
        session.close();
        return name;
    }
}
