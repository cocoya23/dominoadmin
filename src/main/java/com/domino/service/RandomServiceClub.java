package com.domino.service;

import com.domino.bens.PagoIndividual;
import com.domino.bens.PagoIndividualControl;
import com.domino.dao.RandomDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.hibernate.Session;

@ManagedBean(name = "randomServiceClub", eager = true)
@ApplicationScoped
public class RandomServiceClub implements Serializable {

    RandomDAO getRandom = new RandomDAO();
    PagoIndividual pagoIndividual;
    PagoIndividualControl pagoIndividualControl;
    private List<PagoIndividual> pagoindlist;
    private boolean condicion = false;
    private boolean condicion1 = false;
    private boolean condicion2 = false;
    private boolean condicion3 = false;
    private boolean condicion4 = false;
    private Integer idinscripcion;
    private Integer idjugador;
    private Integer idtorneo;
    private Integer pais;
    private Integer metropoli;
    private String club;
    private Integer idinscripcion2;
    private Integer idjugador2;
    private Integer idtorneo2;
    private Integer pais2;
    private Integer metropoli2;
    private String club2;
    private Integer idinscripcion3;
    private Integer idjugador3;
    private Integer idtorneo3;
    private Integer pais3;
    private Integer metropoli3;
    private String club3;
    private Integer idinscripcion4;
    private Integer idjugador4;
    private Integer idtorneo4;
    private Integer pais4;
    private Integer metropoli4;
    private String club4;

    public void getRandom(Integer torneo) throws Exception {
        int count = 0;
        int rows2 = 0;
        getRandom.setIdtorneo(torneo);
        int filas = getRandom.getRowsPagoIndividual();
        System.out.println("FILAS:" + filas);
        int n = 0;
        pagoindlist = new ArrayList(0);
        Session session = HibernateUtil.getSession();
        session.isConnected();
        while (condicion == false) {
            while (condicion1 == false) {
                if (n == 0) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion = pagoIndividual.getIdinscripcion();
                    idjugador = pagoIndividual.getIdjugador();
                    idtorneo = pagoIndividual.getIdtorneo();
                    pais = pagoIndividual.getPais();
                    metropoli = pagoIndividual.getMetropoli();
                    club = pagoIndividual.getClub();
                    if (club == null) {
                        club = "";
                    }
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion);
                    count += 1;
                    System.out.println("COUNT CLUB:" + count);
                    if (count == (filas * 5)) {
                        condicion1 = true;
                        condicion = true;
                        count = 0;
                    }
                    if (rows2 == 0) {
                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion, idjugador, idtorneo, pais, metropoli, club);
                        getRandom.savePagoIndC(pagoIndividualControl);
                        pagoindlist.add(pagoIndividual);
                        n = pagoindlist.size();
                        System.out.println("***********" + n + "**********");
                        count = 0;
                        condicion1 = true;
                    }


                }
            }
            if (n == 1) {
                while (condicion2 == false) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion2 = pagoIndividual.getIdinscripcion();
                    System.out.println("***********" + idinscripcion2);
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion2);
                    count += 1;
                    System.out.println("COUNT CLUB:" + count);
                    if (count == (filas * 5)) {
                        condicion2 = true;
                        condicion = true;
                        count = 0;
                    }
                    if (rows2 == 0) {
                        idinscripcion2 = pagoIndividual.getIdinscripcion();
                        idjugador2 = pagoIndividual.getIdjugador();
                        idtorneo2 = pagoIndividual.getIdtorneo();
                        pais2 = pagoIndividual.getPais();
                        metropoli2 = pagoIndividual.getMetropoli();
                        club2 = pagoIndividual.getClub();
                        if (club2 == null) {
                            club2 = "";
                        }
                        if (!club.equals(club2)) {
                            pagoIndividualControl = new PagoIndividualControl(null, idinscripcion2, idjugador2, idtorneo2, pais2, metropoli2, club2);
                            getRandom.savePagoIndC(pagoIndividualControl);
                            pagoindlist.add(pagoIndividual);
                            condicion2 = true;
                            count = 0;

                        }

                    }


                }
            }

            n = pagoindlist.size();
            System.out.println("***********" + n + "**********");
            if (n == 2) {
                while (condicion3 == false) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion3 = pagoIndividual.getIdinscripcion();
                    System.out.println("***********" + idinscripcion3);
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion3);
                    count += 1;
                    System.out.println("COUNT CLUB:" + count);
                    if (count == (filas * 5)) {
                        condicion3 = true;
                        condicion = true;
                        count = 0;
                    }
                    if (rows2 == 0) {
                        idinscripcion3 = pagoIndividual.getIdinscripcion();
                        idjugador3 = pagoIndividual.getIdjugador();
                        idtorneo3 = pagoIndividual.getIdtorneo();
                        pais3 = pagoIndividual.getPais();
                        metropoli3 = pagoIndividual.getMetropoli();
                        club3 = pagoIndividual.getClub();
                        if (club3 == null) {
                            club3 = "";
                        }
                        if ( (pais==pais3 &&  (!club.equals(club2) && !club2.equals(club3) && !club.equals(club3))) || (!club.equals(club2) && !club2.equals(club3) && !club.equals(club3)) ) {
                            pagoIndividualControl = new PagoIndividualControl(null, idinscripcion3, idjugador3, idtorneo3, pais3, metropoli3, club3);
                            getRandom.savePagoIndC(pagoIndividualControl);
                            pagoindlist.add(pagoIndividual);
                            condicion3 = true;
                            count = 0;

                        }


                    }


                }
            }

            n = pagoindlist.size();
            System.out.println("***********" + n + "**********");
            if (n == 3) {
                while (condicion4 == false) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion4 = pagoIndividual.getIdinscripcion();
                    System.out.println("***********" + idinscripcion4);
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion4);
                    count += 1;
                    System.out.println("COUNT CLUB:" + count);
                    if (count == (filas * 5)) {
                        condicion4 = true;
                        condicion = true;
                        count = 0;
                    }
                    if (rows2 == 0) {
                        idinscripcion4 = pagoIndividual.getIdinscripcion();
                        idjugador4 = pagoIndividual.getIdjugador();
                        idtorneo4 = pagoIndividual.getIdtorneo();
                        pais4 = pagoIndividual.getPais();
                        metropoli4 = pagoIndividual.getMetropoli();
                        club4 = pagoIndividual.getClub();
                        if (club4 == null) {
                            club4 = "";
                        }
                        if ((!club.equals(club2) && !club2.equals(club3) && !club.equals(club3) && !club.equals(club4) && !club2.equals(club4) && !club3.equals(club4))) {
                            pagoIndividualControl = new PagoIndividualControl(null, idinscripcion4, idjugador4, idtorneo4, pais4, metropoli4, club4);
                            getRandom.savePagoIndC(pagoIndividualControl);
                            pagoindlist.add(pagoIndividual);
                            condicion4 = true;
                            count = 0;

                        }

                    }


                }
            }
            n = pagoindlist.size();
            System.out.println("***********" + n + "**********");
            if (n == 4) {
                condicion1 = false;
                condicion2 = false;
                condicion3 = false;
                condicion4 = false;
                pagoindlist = new ArrayList(0);
                pagoIndividual = null;
                n = 0;
                count = 0;
            }

        }

        System.out.println("Salio de  while");

        boolean condicion5 = false;
        while (condicion5 == false) {
            pagoIndividual = getRandom.getRandom();
            idinscripcion = pagoIndividual.getIdinscripcion();
            System.out.println("***********" + idinscripcion);
            int rows = this.getRandom.getPagoIndividualControl(idinscripcion);
            int pagind = getRandom.getRowsPagoIndividual();
            int pagindc = getRandom.getRowsPagoIndividualControl();
            if (rows == 0) {
                idinscripcion = pagoIndividual.getIdinscripcion();
                idjugador = pagoIndividual.getIdjugador();
                idtorneo = pagoIndividual.getIdtorneo();
                pais = pagoIndividual.getPais();
                metropoli = pagoIndividual.getMetropoli();
                club = pagoIndividual.getClub();
                pagoIndividualControl = new PagoIndividualControl(null, idinscripcion, idjugador, idtorneo, pais, metropoli, club);
                getRandom.savePagoIndC(pagoIndividualControl);
                pagoindlist.add(pagoIndividual);

            }
            if (pagind == pagindc) {
                condicion5 = true;
            }


        }
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public boolean isCondicion() {
        return condicion;
    }

    public void setCondicion(boolean condicion) {
        this.condicion = condicion;
    }

    public RandomDAO getGetRandom() {
        return getRandom;
    }

    public void setGetRandom(RandomDAO getRandom) {
        this.getRandom = getRandom;
    }

    public Integer getIdinscripcion() {
        return idinscripcion;
    }

    public void setIdinscripcion(Integer idinscripcion) {
        this.idinscripcion = idinscripcion;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public PagoIndividual getPagoIndividual() {
        return pagoIndividual;
    }

    public void setPagoIndividual(PagoIndividual pagoIndividual) {
        this.pagoIndividual = pagoIndividual;
    }

    public List<PagoIndividual> getPagoindlist() {
        return pagoindlist;
    }

    public void setPagoindlist(List<PagoIndividual> pagoindlist) {
        this.pagoindlist = pagoindlist;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public String getClub2() {
        return club2;
    }

    public void setClub2(String club2) {
        this.club2 = club2;
    }

    public String getClub3() {
        return club3;
    }

    public void setClub3(String club3) {
        this.club3 = club3;
    }

    public String getClub4() {
        return club4;
    }

    public void setClub4(String club4) {
        this.club4 = club4;
    }

    public boolean isCondicion1() {
        return condicion1;
    }

    public void setCondicion1(boolean condicion1) {
        this.condicion1 = condicion1;
    }

    public Integer getIdinscripcion2() {
        return idinscripcion2;
    }

    public void setIdinscripcion2(Integer idinscripcion2) {
        this.idinscripcion2 = idinscripcion2;
    }

    public Integer getIdinscripcion3() {
        return idinscripcion3;
    }

    public void setIdinscripcion3(Integer idinscripcion3) {
        this.idinscripcion3 = idinscripcion3;
    }

    public Integer getIdinscripcion4() {
        return idinscripcion4;
    }

    public void setIdinscripcion4(Integer idinscripcion4) {
        this.idinscripcion4 = idinscripcion4;
    }

    public Integer getIdjugador2() {
        return idjugador2;
    }

    public void setIdjugador2(Integer idjugador2) {
        this.idjugador2 = idjugador2;
    }

    public Integer getIdjugador3() {
        return idjugador3;
    }

    public void setIdjugador3(Integer idjugador3) {
        this.idjugador3 = idjugador3;
    }

    public Integer getIdjugador4() {
        return idjugador4;
    }

    public void setIdjugador4(Integer idjugador4) {
        this.idjugador4 = idjugador4;
    }

    public Integer getIdtorneo2() {
        return idtorneo2;
    }

    public void setIdtorneo2(Integer idtorneo2) {
        this.idtorneo2 = idtorneo2;
    }

    public Integer getIdtorneo3() {
        return idtorneo3;
    }

    public void setIdtorneo3(Integer idtorneo3) {
        this.idtorneo3 = idtorneo3;
    }

    public Integer getIdtorneo4() {
        return idtorneo4;
    }

    public void setIdtorneo4(Integer idtorneo4) {
        this.idtorneo4 = idtorneo4;
    }

    public Integer getMetropoli2() {
        return metropoli2;
    }

    public void setMetropoli2(Integer metropoli2) {
        this.metropoli2 = metropoli2;
    }

    public Integer getMetropoli3() {
        return metropoli3;
    }

    public void setMetropoli3(Integer metropoli3) {
        this.metropoli3 = metropoli3;
    }

    public Integer getMetropoli4() {
        return metropoli4;
    }

    public void setMetropoli4(Integer metropoli4) {
        this.metropoli4 = metropoli4;
    }

    public PagoIndividualControl getPagoIndividualControl() {
        return pagoIndividualControl;
    }

    public void setPagoIndividualControl(PagoIndividualControl pagoIndividualControl) {
        this.pagoIndividualControl = pagoIndividualControl;
    }

    public Integer getPais2() {
        return pais2;
    }

    public void setPais2(Integer pais2) {
        this.pais2 = pais2;
    }

    public Integer getPais3() {
        return pais3;
    }

    public void setPais3(Integer pais3) {
        this.pais3 = pais3;
    }

    public Integer getPais4() {
        return pais4;
    }

    public void setPais4(Integer pais4) {
        this.pais4 = pais4;
    }
}
