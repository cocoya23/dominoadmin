package com.domino.service;

import com.domino.Frequency;
import com.domino.bens.PagoIndividual;
import com.domino.bens.PagoIndividualControl;
import com.domino.dao.PagoIndividualDAO;
import com.domino.dao.RandomDAO;
import com.domino.managed.RandomManaged;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.hibernate.Session;
/**
 *Esta clase es el manejo de datos de pago Unico. que nos permitira :
 * 
 * <ol>
 * <il></il>
 * </ol>
 * @author A. Eliud Uriostegui Gonzalez
 * 
 * @see RandomService
 * @see PagoIndividualDAO
 * @see PagoIndividualService
 */

@javax.faces.bean.ManagedBean(name = "RankingService", eager = true)
@ApplicationScoped
public class RankingService implements Serializable {

    @ManagedProperty("#{randomService}")
    private RandomService randomService;
    private PagoIndividualDAO pagoIndividualDAO =
            new PagoIndividualDAO();
    
    private PagoIndividual pagoIndividual = 
            new PagoIndividual();
    /*Coleccion de enteros <code>idPaises</code>*/
    private static ArrayList<Integer> frequency = 
            new ArrayList<Integer>();
    private static PagoIndividualControl control;
    
    /*Colección de objetos <code>Frequency</code>*/
    private static ArrayList<Frequency> listP =
            new ArrayList<Frequency>();
    
    /*Colección de usuarios en el top ranking*/
    private static List<PagoIndividualControl> pagoIndividualTopOne =
            new ArrayList<PagoIndividualControl>();
    
    /*Colección de usuarios en el top ranking 2*/
    private static List<PagoIndividualControl> pagoIndividualTopTwo =
            new ArrayList<PagoIndividualControl>();
    
    /*Colección de usuarios en el top ranking 3*/
    private static List<PagoIndividualControl> pagoIndividualTopThree =
            new ArrayList<PagoIndividualControl>();
    
    /*Colección en el top ranking others*/
    private static List<PagoIndividualControl> pagoIndividualTopOthers =
            new ArrayList<PagoIndividualControl>();
    
    private static List<PagoIndividual> pagoIndividuals;
    
    /*Coleción de objetos <code>Frequency</code>*/
    private static ArrayList<Frequency> listTopRanking = 
            new ArrayList<Frequency>();
    
    private RandomManaged randomManaged =
            new RandomManaged();
    private static RandomDAO randomDAO =
            new RandomDAO();
    
    private Random random = new Random();
    
    /*Variables*/
    int idInscripcion;
    int idJugador;
    int idTorneo;
    int idPais;
    int metropoli;
    String club;
    
    /**
     * Añade un pais a esta lista.
     * 
     * @param pais id del pais 
     */
    public void addPaisFrequency(int pais){
        this.frequency.add(pais);
    }
    
        /**
     * Obtiene el idPais por medio de idinscripcion
     * 
     * @param idinscripcion
     * @return idPais
     * @throws Exception 
     */
    public int getPais(int idinscripcion)throws Exception{
        Session session = HibernateUtil.getSession();
        session.isConnected();
        int idPais = this.pagoIndividualDAO.getPaisFilas(idinscripcion);
        return idPais;
    }
    
    /**
     * Añade un pais y su frecuencia a esta lista.
     * 
     * @param idPais
     * @param idFrequency 
     */
    public void addListaRanking(int idPais,int idFrequency){
        
        this.listP.add(new Frequency(idPais, idFrequency));
    }
    
    /**
     * Añade un pais y su frecuencia ha esta lista.
     * 
     * @param idPais
     * @param idFrequency 
     */
    public void addListaTopRanking(int idPais, int idFrequency){
        this.listTopRanking.add(new Frequency(idPais, idFrequency));
    }
    
    /**
     * Este metodo genera la lista con los usuarios del pais que se encuentra
     * en el TOP 1.
     * 
     * @param pais que es el top 1.
     */
    public void addTopOne(int pais, int torneo){
        try {
            int filas = pagoIndividualDAO.getRowsPagoIndividual();
            for(int i=1;i<=filas;i++){
                pagoIndividual = pagoIndividualDAO.getPais(i);
                this.idInscripcion = pagoIndividual.getIdinscripcion();
                this.idJugador = pagoIndividual.getIdjugador();
                this.idTorneo = pagoIndividual.getIdtorneo();
                this.idPais = pagoIndividual.getPais();
                this.metropoli = pagoIndividual.getMetropoli();
                this.club = pagoIndividual.getClub();
                if(club.equals("null")){
                    club="";
                }
                if(pais == idPais && torneo == idTorneo){
                    
                    control = new PagoIndividualControl(null, idInscripcion, 
                            idJugador, idTorneo, pais, metropoli, club);
                    this.pagoIndividualTopOne.add(control);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(com.domino.service.RankingService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Este metodo genera una lista con los usuarios
     * @param pais 
     */
    public void addTopTwo(int pais, int torneo){
        try {
            int filas = pagoIndividualDAO.getRowsPagoIndividual();
            for(int i=1;i<=filas;i++){
                pagoIndividual = pagoIndividualDAO.getPais(i);
                this.idInscripcion = pagoIndividual.getIdinscripcion();
                this.idJugador = pagoIndividual.getIdjugador();
                this.idTorneo = pagoIndividual.getIdtorneo();
                this.idPais = pagoIndividual.getPais();
                this.metropoli = pagoIndividual.getMetropoli();
                this.club = pagoIndividual.getClub();
                if(club.equals("null")){
                    club="";
                }
                if(pais == idPais && torneo == idTorneo){
                    control = new PagoIndividualControl(null, idInscripcion, 
                            idJugador, idTorneo, pais, metropoli, club);
                    this.pagoIndividualTopTwo.add(control);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(com.domino.service.RankingService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
        
    }
    
    /**
     * Genera lista en base al top 3.
     * 
     * @param pais 
     */
    public void addTopThree(int pais, int torneo){
        try {
            int filas = pagoIndividualDAO.getRowsPagoIndividual();
            for(int i=1;i<=filas;i++){
                pagoIndividual = pagoIndividualDAO.getPais(i);
                this.idInscripcion = pagoIndividual.getIdinscripcion();
                this.idJugador = pagoIndividual.getIdjugador();
                this.idTorneo = pagoIndividual.getIdtorneo();
                this.idPais = pagoIndividual.getPais();
                this.metropoli = pagoIndividual.getMetropoli();
                this.club = pagoIndividual.getClub();
                if(club.equals("null")){
                    club="";
                }
                if(pais == idPais && torneo == idTorneo){
                    control = new PagoIndividualControl(null, idInscripcion, 
                            idJugador, idTorneo, pais, metropoli, club);
                    this.pagoIndividualTopThree.add(control);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(com.domino.service.RankingService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Agrega los pago que pertenecen a others.
     * 
     * @param pais 
     */
    public void addTopOthers(int pais, int torneo){
        try {
            int filas = pagoIndividualDAO.getRowsPagoIndividual();
            for(int i=1;i<=filas;i++){
                pagoIndividual = pagoIndividualDAO.getPais(i);
                this.idInscripcion = pagoIndividual.getIdinscripcion();
                this.idJugador = pagoIndividual.getIdjugador();
                this.idTorneo = pagoIndividual.getIdtorneo();
                this.idPais = pagoIndividual.getPais();
                this.metropoli = pagoIndividual.getMetropoli();
                this.club = pagoIndividual.getClub();
                if(club.equals("null")){
                    club="";
                }
                if(pais == idPais && torneo == idTorneo){
                    control = new PagoIndividualControl(null, idInscripcion, 
                            idJugador, idTorneo, pais, metropoli, club);
                    this.pagoIndividualTopOthers.add(control);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(com.domino.service.RankingService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Genera un ranking de paises segun la cantidad de usuarios.
     * 
     * <ol>
     * <il>ordena la lista de paises segun el toop que haya.</il>
     * <il></il>
     * </ol>
     * @return
     * @throws Exception 
     */
    public ArrayList<Frequency> getRanking(int idTorneo) throws Exception{
        int counter = 0;
        //int rows = pagoIndividualDAO.getRowsPagoIndividual();
        pagoIndividuals = pagoIndividualDAO.getPagoIndividualList();
        for(PagoIndividual pi: pagoIndividuals){
            if (pi.getIdtorneo().equals(idTorneo)){
                addPaisFrequency(pi.getPais());
                counter++;
            }
        }
        //Si el numero de paises es menor a 4 mandara un mensaje de error.
        if(counter <4){
            //llamar metodo de error
            errorMessage(1);
        }else {
            Comparator<Integer> comparador = Collections.reverseOrder();
            /*ordena la lista de manera desc*/
            Collections.sort(frequency, comparador);
            /*Alamacenara el pais para evitar duplicidad y busquedas innecesarias*/
            int idPais = 0;
            for(int f: frequency){
            
            if(f == idPais){
            f = idPais;
            }else{
                int count = getCount(f);
                if(count == 0){
                    idPais = f;
                }else{
                    idPais = f;
                    addListaRanking(idPais, count);
                }
            }
        }
        Collections.sort(listP);
        //return listP;
        }
        //for (int i = 1;i<=rows;i++){
            /*Genera una lista solo con los paises*/
            //addPaisFrequency(getPais(i));
        //}
        //Comparator<Integer> comparador = Collections.reverseOrder();
        /*ordena la lista de manera desc*/
        //Collections.sort(frequency, comparador);
        /*Alamacenara el pais para evitar duplicidad y busquedas innecesarias*/
        //int idPais = 0; 
        
       // for(int f: frequency){
            
         //   if(f == idPais){
           // f = idPais;
          //  }else{
            //    int count = getCount(f);
              //  if(count == 0){
                //    idPais = f;
                //}else{
                  //  idPais = f;
                   // addListaRanking(idPais, count);
                //}
            //}
        //}
       // Collections.sort(listP);
        return listP;
    }
    
    /**
     * Obtiene una cuenta de los paises.
     * 
     * @param counter
     * @return 
     */
    private int getCount(int counter){
        int count = 0;
        Iterator<Integer> it = frequency.iterator();
        while (it.hasNext()) {
                    int pagoPais = it.next();
                    if (counter == pagoPais) {
                        count += 1;
                    }
                }
        return count;
    }
    /**
     * Regresa un List con el top 4 de los paises con mas registro.
     * 
     * @return Top Ranking de los 3 paises con mas jugadores y otros.
     * @throws Exception 
     */
    public List<Frequency> getTopRanking(int idTorneo) throws Exception{
        int idPaisFrequency;
        listP = getRanking(idTorneo);
        if(listP.isEmpty()){
            errorMessage(1);
        }else {
        Iterator itListP = listP.iterator();
        int posicion = 1;
        int others=0;
        while(itListP.hasNext()){
            Frequency topRankingPaises = (Frequency) itListP.next();
            if(posicion == 1){
                idPaisFrequency= topRankingPaises.getIdPais();
                addTopOne(idPaisFrequency,idTorneo);
            }else if(posicion ==2){
                idPaisFrequency = topRankingPaises.getIdPais();
                addTopTwo(idPaisFrequency,idTorneo);
            }else if(posicion == 3){
                idPaisFrequency = topRankingPaises.getIdPais();
                addTopThree(idPaisFrequency,idTorneo);
            }else if(posicion>3){
                idPaisFrequency = topRankingPaises.getIdPais();
                addTopOthers(idPaisFrequency,idTorneo);
            }
            if(posicion <4 ){
                idPaisFrequency = topRankingPaises.getIdPais();
                int ranking = topRankingPaises.getFrequency();
                addListaTopRanking(idPaisFrequency, ranking);
                posicion++;
            }else if(posicion >=4){
                others+= topRankingPaises.getFrequency();
                posicion++;
            }
        } 
        addListaTopRanking(4, others);
        }
        return listTopRanking;
    }
    
    /**
     * Genera la lista de la partida inicial
     * completando con el metodo random
     * 
     * @param idTorneo de os participantes del torneo
     */
    public void setListaNueva(int idTorneo){
        try {
            getTopRanking(idTorneo);
            pagoIndividualDAO.truncateTable();
            int index;
            int times=0;
            
            if (pagoIndividualTopOthers.isEmpty()) {
                errorMessage(2);
            }else{
            
            for(PagoIndividualControl pic: pagoIndividualTopThree){
                if(pic.getIdtorneo().equals(idTorneo)){
                    times++;
                }
            }
            for(int i=1;i<=times;i++){
                /*Lista 1*/
                index = random.nextInt(pagoIndividualTopOne.size());
                control = pagoIndividualTopOne.get(index);
                while(control.getIdtorneo()!=idTorneo){
                    pagoIndividualTopOne.remove(index);
                    index = random.nextInt(pagoIndividualTopOne.size());
                    control = pagoIndividualTopOne.get(index);
                }
                randomDAO.savePagoIndC(control);
                System.out.println("idinscripcion: "+control.getIdinscripcion()+
                        "_idjugador: "+control.getIdjugador()+"_idTorneo: "+
                        control.getIdtorneo()+"_pais: "+control.getPais()+
                        "_metropoli: "+control.getMetropoli()+"_club: "+
                        control.getClub());
                pagoIndividualTopOne.remove(index);
                
                /*Lista 2*/
                index = random.nextInt(pagoIndividualTopTwo.size());
                control = pagoIndividualTopTwo.get(index);
                randomDAO.savePagoIndC(control);
                System.out.println("idinscripcion: "+control.getIdinscripcion()+
                        "_idjugador: "+control.getIdjugador()+"_idTorneo: "+
                        control.getIdtorneo()+"_pais: "+control.getPais()+
                        "_metropoli: "+control.getMetropoli()+"_club: "+
                        control.getClub());
                pagoIndividualTopTwo.remove(index);
                
                /*Lista 3*/
                index = random.nextInt(pagoIndividualTopThree.size());
                control = pagoIndividualTopThree.get(index);
                randomDAO.savePagoIndC(control);
                System.out.println("idinscripcion: "+control.getIdinscripcion()+
                        "_idjugador: "+control.getIdjugador()+"_idTorneo: "+
                        control.getIdtorneo()+"_pais: "+control.getPais()+
                        "_metropoli: "+control.getMetropoli()+"_club: "+
                        control.getClub());
                pagoIndividualTopThree.remove(index);
                
                /*Lista otros*/
                index = random.nextInt(pagoIndividualTopOthers.size());
                control = pagoIndividualTopOthers.get(index);
                randomDAO.savePagoIndC(control);
                System.out.println("idinscripcion: "+control.getIdinscripcion()+
                        "_idjugador: "+control.getIdjugador()+"_idTorneo: "+
                        control.getIdtorneo()+"_pais: "+control.getPais()+
                        "_metropoli: "+control.getMetropoli()+"_club: "+
                        control.getClub());
                pagoIndividualTopOthers.remove(index);
            }
            this.randomService.getRandom(idTorneo);
            }
        } catch (Exception ex) {
            Logger.getLogger(com.domino.service.RankingService.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void errorMessage(int err){
        
        if(err == 1){
            System.out.println("No se cuenta con los jugadores suficientes.");
        }else if(err == 2){
            System.out.println("No cuenta con suficientes paises");
        }
    }
    
    
    
    
    public void setPosicion1(){
        
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public static PagoIndividualControl getControl() {
        return control;
    }

    public static void setControl(PagoIndividualControl control) {
        RankingService.control = control;
    }

    public static ArrayList<Integer> getFrequency() {
        return frequency;
    }

    public static void setFrequency(ArrayList<Integer> frequency) {
        RankingService.frequency = frequency;
    }

    public int getIdInscripcion() {
        return idInscripcion;
    }

    public void setIdInscripcion(int idInscripcion) {
        this.idInscripcion = idInscripcion;
    }

    public int getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(int idJugador) {
        this.idJugador = idJugador;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public int getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(int idTorneo) {
        this.idTorneo = idTorneo;
    }

    public static ArrayList<Frequency> getListP() {
        return listP;
    }

    public static void setListP(ArrayList<Frequency> listP) {
        RankingService.listP = listP;
    }

    public static ArrayList<Frequency> getListTopRanking() {
        return listTopRanking;
    }

    public static void setListTopRanking(ArrayList<Frequency> listTopRanking) {
        RankingService.listTopRanking = listTopRanking;
    }

    public int getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(int metropoli) {
        this.metropoli = metropoli;
    }

    public PagoIndividual getPagoIndividual() {
        return pagoIndividual;
    }

    public void setPagoIndividual(PagoIndividual pagoIndividual) {
        this.pagoIndividual = pagoIndividual;
    }

    public PagoIndividualDAO getPagoIndividualDAO() {
        return pagoIndividualDAO;
    }

    public void setPagoIndividualDAO(PagoIndividualDAO pagoIndividualDAO) {
        this.pagoIndividualDAO = pagoIndividualDAO;
    }

    public static List<PagoIndividualControl> getPagoIndividualTopOne() {
        return pagoIndividualTopOne;
    }

    public static void setPagoIndividualTopOne(List<PagoIndividualControl> pagoIndividualTopOne) {
        RankingService.pagoIndividualTopOne = pagoIndividualTopOne;
    }

    public static List<PagoIndividualControl> getPagoIndividualTopOthers() {
        return pagoIndividualTopOthers;
    }

    public static void setPagoIndividualTopOthers(List<PagoIndividualControl> pagoIndividualTopOthers) {
        RankingService.pagoIndividualTopOthers = pagoIndividualTopOthers;
    }

    public static List<PagoIndividualControl> getPagoIndividualTopThree() {
        return pagoIndividualTopThree;
    }

    public static void setPagoIndividualTopThree(List<PagoIndividualControl> pagoIndividualTopThree) {
        RankingService.pagoIndividualTopThree = pagoIndividualTopThree;
    }

    public static List<PagoIndividualControl> getPagoIndividualTopTwo() {
        return pagoIndividualTopTwo;
    }

    public static void setPagoIndividualTopTwo(List<PagoIndividualControl> pagoIndividualTopTwo) {
        RankingService.pagoIndividualTopTwo = pagoIndividualTopTwo;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    public static RandomDAO getRandomDAO() {
        return randomDAO;
    }

    public static void setRandomDAO(RandomDAO randomDAO) {
        RankingService.randomDAO = randomDAO;
    }

    public RandomManaged getRandomManaged() {
        return randomManaged;
    }

    public void setRandomManaged(RandomManaged randomManaged) {
        this.randomManaged = randomManaged;
    }

    public RandomService getRandomService() {
        return randomService;
    }

    public void setRandomService(RandomService randomService) {
        this.randomService = randomService;
    }
    
    
}