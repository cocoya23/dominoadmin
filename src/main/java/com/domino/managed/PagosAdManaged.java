package com.domino.managed;

import com.domino.bens.*;
import com.domino.other.EnviarCorreo;
import com.domino.service.CreateReciboPDF;
import com.domino.service.PagosAdService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Sergio Cano
 */
@ManagedBean
@ViewScoped
public class PagosAdManaged implements Serializable {

    @ManagedProperty("#{pagosAdService}")
    private PagosAdService pagosAdService;
    Torneo torneo;
    private List<SelectItem> torneos;
    private List<SelectItem> jugadores;
    private Integer idtorneo;
    private String nametorneo;
    private Integer varpagos;
    private Integer idjugador;
    Jugador jugador;
    private boolean booind;
    private boolean boopar;
    private boolean booequ;
    private String insind = "";
    private String inspar = "";
    private String insequ = "";
    private Float suma;
    private Float sumapareja;
    private Float sumaequipo1;
    private Float sumaequipo2;
    private Float sumaequipo3;
    private Float sumatotal;
    private String parejaname;
    private String insindpar = "";
    private String insparpar = "";
    private String insequpar = "";
    private String insindequ1 = "";
    private String insparequ1 = "";
    private String insequequ1 = "";
    private String insindequ2 = "";
    private String insparequ2 = "";
    private String insequequ2 = "";
    private String insindequ3 = "";
    private String insparequ3 = "";
    private String insequequ3 = "";
    private String equiponame1;
    private String equiponame2;
    private String equiponame3;
    private String checkindividual;
    private String checkpareja;
    private String checkequipo1;
    private String checkequipo2;
    private String checkequipo3;
    private int pago = 0;
    private boolean tarjeta;
    private String iden;
    PagoControl pagoControl;
    private Integer idequipo1;
    private Integer idequipo2;
    private Integer idequipo3;
    private Integer idpareja;
    TorneoControl torneoControl;
    private String modalidad = " ";
    private String noiden;
    private String comen;
    private boolean chkind;
    private boolean chkpar;
    private boolean chkeq1;
    private boolean chkeq2;
    private boolean chkeq3;
    private boolean booequ2;
    private boolean booequ3;

    @PostConstruct
    public void init() {
        torneos = new ArrayList(0);
        try {
            for (Torneo torneo : pagosAdService.getTorneos()) {
                torneos.add(new SelectItem(torneo.getIdTorneo(), torneo.getTorneo()));
            }
            varpagos = 0;
        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void muestraJugadores() {
        try {
            jugadores = new ArrayList(0);
            for (JugadorTorneo jugadortorneo : pagosAdService.getJugador(idtorneo)) {
                jugadores.add(new SelectItem(jugadortorneo.getJugador().getIdjugador(), jugadortorneo.getJugador().getNombre()));
            }
            varpagos = 1;
        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modalidades() {
        JugadorTorneo jugadorTorneo;
        try {
            jugadorTorneo = pagosAdService.getJugadorxTorneo(idtorneo, idjugador);
            pago = 0;
            Integer individual = jugadorTorneo.getIndividual();
            Integer pareja = jugadorTorneo.getPareja();
            Integer equipo = jugadorTorneo.getEquipo();
            Torneo torneo = pagosAdService.getTorneos(idtorneo);
            Float ind;
            Float par;
            Float equ;
            Float indpar;
            Float parpar;
            Float equpar;

            if (individual == 1) {
                //booind = pagosAdService.getrowsPago(idtorneo, idjugador);
                insind = String.valueOf(torneo.getInsind());
                ind = torneo.getInsind();
            } else {
                ind = Float.parseFloat("0.0");
                insind = "0";
            }


            if (pareja == 1) {
                //boopar = pagosAdService.getrowsPago(idtorneo, idjugador);
                inspar = String.valueOf(torneo.getInspar());
                par = torneo.getInspar();
                parejaname = pagosAdService.namePareja(idtorneo, idjugador);
                idpareja = pagosAdService.idPareja(idtorneo, idjugador);
                if (idpareja == 0) {
                    insindpar = "0";
                    insparpar = "0";
                    insequpar = "0";

                } else {
                    JugadorTorneo jugadorTorneo2 = pagosAdService.getJugadorxTorneo(idtorneo, idpareja);
                    Integer individualpar = jugadorTorneo2.getIndividual();
                    Integer parejapar = jugadorTorneo2.getPareja();
                    Integer parejaequ = jugadorTorneo2.getEquipo();
                    if (individualpar == 1) {
                        insindpar = String.valueOf(torneo.getInsind());
                        indpar = torneo.getInsind();
                    } else {
                        indpar = Float.parseFloat("0.0");
                        insindpar = "0";
                    }
                    if (parejapar == 1) {
                        insparpar = String.valueOf(torneo.getInspar());
                        parpar = torneo.getInspar();
                    } else {
                        parpar = Float.parseFloat("0.0");
                        insparpar = "0";
                    }
                    if (parejaequ == 1) {
                        insequpar = String.valueOf(torneo.getInsequ());
                        equpar = torneo.getInsequ();
                    } else {
                        equpar = Float.parseFloat("0.0");
                        insequpar = "0";
                    }
                    sumapareja = indpar + parpar + equpar;
                }
            } else {
                par = Float.parseFloat("0.0");
                inspar = "0";
                parejaname = "";
            }




            if (equipo == 1) {
                booequ = pagosAdService.getrowsPago(idtorneo, idjugador);
                insequ = String.valueOf(torneo.getInsequ());
                equ = torneo.getInsequ();
                Integer rowsequipos = pagosAdService.equipoRows(idtorneo, idjugador);
                if (rowsequipos == 0) {
                    insindequ1 = " ";
                    insparequ1 = " ";
                    insequequ1 = " ";
                    insindequ2 = " ";
                    insparequ2 = " ";
                    insequequ2 = " ";
                    insindequ3 = " ";
                    insparequ3 = " ";
                    insequequ3 = " ";
                    equiponame1 = " ";
                    equiponame2 = " ";
                    equiponame3 = " ";
                    sumaequipo1 = Float.parseFloat("0");
                    sumaequipo2 = Float.parseFloat("0");
                    sumaequipo3 = Float.parseFloat("0");

                } else {
                    int varequipo = equipo(idtorneo, idjugador);
                    if (varequipo == 1) {
                        System.out.println("SI hay equipo");
                    } else {
                        System.out.println("NO hay equipo");
                    }
                }

            } else {
                equ = Float.parseFloat("0.0");
                insequ = "0";
                insindequ1 = " ";
                insparequ1 = " ";
                insequequ1 = " ";
                insindequ2 = " ";
                insparequ2 = " ";
                insequequ2 = " ";
                insindequ3 = " ";
                insparequ3 = " ";
                insequequ3 = " ";
                equiponame1 = " ";
                equiponame2 = " ";
                equiponame3 = " ";
                sumaequipo1 = Float.parseFloat("0");
                sumaequipo2 = Float.parseFloat("0");
                sumaequipo3 = Float.parseFloat("0");
            }
            suma = ind + par + equ;

            bloqueaPago(idtorneo, idjugador);


        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int equipo(Integer idtor, Integer idju) {
        int var = 0;
        try {
            FormacionEquipo formacionEquipo = pagosAdService.formEquipo(idtor, idju);
            Integer id1 = formacionEquipo.getIdjugador1();
            Integer id2 = formacionEquipo.getIdjugador2();
            Integer id3 = formacionEquipo.getIdjugador3();
            Integer id4 = formacionEquipo.getIdjugador4();
            id1 = idnull(id1);
            id2 = idnull(id2);
            id3 = idnull(id3);
            id4 = idnull(id4);
            if (id1 == 0 || id2 == 0 || id3 == 0 || id4 == 0) {
                insindequ1 = "";
                insparequ1 = "";
                insequequ1 = "";
                insindequ2 = "";
                insparequ2 = "";
                insequequ2 = "";
                insindequ3 = "";
                insparequ3 = "";
                insequequ3 = "";
                equiponame1 = "";
                equiponame2 = "";
                equiponame3 = "";
                sumaequipo1 = Float.parseFloat("0");
                sumaequipo2 = Float.parseFloat("0");
                sumaequipo3 = Float.parseFloat("0");
                var = 0;
            } else {
                if (idjugador.intValue() == id1.intValue()) {
                    insindequ1 = pagoIndividual(id2.intValue());
                    insparequ1 = pagoPareja(id2.intValue());
                    insequequ1 = pagoEquipo(id2.intValue());
                    insindequ2 = pagoIndividual(id3.intValue());
                    insparequ2 = pagoPareja(id3.intValue());
                    insequequ2 = pagoEquipo(id3.intValue());
                    insindequ3 = pagoIndividual(id4.intValue());
                    insparequ3 = pagoPareja(id4.intValue());
                    insequequ3 = pagoEquipo(id4.intValue());
                    equiponame1 = pagosAdService.getNombreEquipo(id2);
                    equiponame2 = pagosAdService.getNombreEquipo(id3);
                    equiponame3 = pagosAdService.getNombreEquipo(id4);
                    idequipo1 = id2;
                    idequipo2 = id3;
                    idequipo3 = id4;
                    sumaequipo1 = Float.parseFloat(insindequ1) + Float.parseFloat(insparequ1) + Float.parseFloat(insequequ1);
                    sumaequipo2 = Float.parseFloat(insindequ2) + Float.parseFloat(insparequ2) + Float.parseFloat(insequequ2);
                    sumaequipo3 = Float.parseFloat(insindequ3) + Float.parseFloat(insparequ3) + Float.parseFloat(insequequ3);
                    var = 1;

                }
                if (idjugador.intValue() == id2.intValue()) {
                    insindequ1 = pagoIndividual(id1.intValue());
                    insparequ1 = pagoPareja(id1.intValue());
                    insequequ1 = pagoEquipo(id1.intValue());
                    insindequ2 = pagoIndividual(id3.intValue());
                    insparequ2 = pagoPareja(id3.intValue());
                    insequequ2 = pagoEquipo(id3.intValue());
                    insindequ3 = pagoIndividual(id4.intValue());
                    insparequ3 = pagoPareja(id4.intValue());
                    insequequ3 = pagoEquipo(id4.intValue());
                    equiponame1 = pagosAdService.getNombreEquipo(id1);
                    equiponame2 = pagosAdService.getNombreEquipo(id3);
                    equiponame3 = pagosAdService.getNombreEquipo(id4);
                    idequipo1 = id1;
                    idequipo2 = id3;
                    idequipo3 = id4;
                    sumaequipo1 = Float.parseFloat(insindequ1) + Float.parseFloat(insparequ1) + Float.parseFloat(insequequ1);
                    sumaequipo2 = Float.parseFloat(insindequ2) + Float.parseFloat(insparequ2) + Float.parseFloat(insequequ2);
                    sumaequipo3 = Float.parseFloat(insindequ3) + Float.parseFloat(insparequ3) + Float.parseFloat(insequequ3);
                    var = 1;


                }
                if (idjugador.intValue() == id3.intValue()) {
                    insindequ1 = pagoIndividual(id1.intValue());
                    insparequ1 = pagoPareja(id1.intValue());
                    insequequ1 = pagoEquipo(id1.intValue());
                    insindequ2 = pagoIndividual(id2.intValue());
                    insparequ2 = pagoPareja(id2.intValue());
                    insequequ2 = pagoEquipo(id2.intValue());
                    insindequ3 = pagoIndividual(id4.intValue());
                    insparequ3 = pagoPareja(id4.intValue());
                    insequequ3 = pagoEquipo(id4.intValue());
                    equiponame1 = pagosAdService.getNombreEquipo(id1);
                    equiponame2 = pagosAdService.getNombreEquipo(id2);
                    equiponame3 = pagosAdService.getNombreEquipo(id4);
                    idequipo1 = id1;
                    idequipo2 = id2;
                    idequipo3 = id4;
                    sumaequipo1 = Float.parseFloat(insindequ1) + Float.parseFloat(insparequ1) + Float.parseFloat(insequequ1);
                    sumaequipo2 = Float.parseFloat(insindequ2) + Float.parseFloat(insparequ2) + Float.parseFloat(insequequ2);
                    sumaequipo3 = Float.parseFloat(insindequ3) + Float.parseFloat(insparequ3) + Float.parseFloat(insequequ3);
                    var = 1;

                }
                if (idjugador.intValue() == id4.intValue()) {
                    insindequ1 = pagoIndividual(id1.intValue());
                    insparequ1 = pagoPareja(id1.intValue());
                    insequequ1 = pagoEquipo(id1.intValue());
                    insindequ2 = pagoIndividual(id2.intValue());
                    insparequ2 = pagoPareja(id2.intValue());
                    insequequ2 = pagoEquipo(id2.intValue());
                    insindequ3 = pagoIndividual(id3.intValue());
                    insparequ3 = pagoPareja(id3.intValue());
                    insequequ3 = pagoEquipo(id3.intValue());
                    equiponame1 = pagosAdService.getNombreEquipo(id1);
                    equiponame2 = pagosAdService.getNombreEquipo(id2);
                    equiponame3 = pagosAdService.getNombreEquipo(id3);
                    idequipo1 = id1;
                    idequipo2 = id2;
                    idequipo3 = id3;
                    sumaequipo1 = Float.parseFloat(insindequ1) + Float.parseFloat(insparequ1) + Float.parseFloat(insequequ1);
                    sumaequipo2 = Float.parseFloat(insindequ2) + Float.parseFloat(insparequ2) + Float.parseFloat(insequequ2);
                    sumaequipo3 = Float.parseFloat(insindequ3) + Float.parseFloat(insparequ3) + Float.parseFloat(insequequ3);
                    var = 1;

                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
        return var;
    }

    public String pagoIndividual(Integer id) {
        JugadorTorneo jugadorTorneo3;
        String pago = "";

        try {
            jugadorTorneo3 = pagosAdService.getJugadorxTorneo(idtorneo, id);
            Torneo torneo2 = pagosAdService.getTorneos(idtorneo);
            if (jugadorTorneo3.getIndividual() == 0) {
                pago = "0";
            } else {
                String inscripcion = String.valueOf(torneo2.getInsind());
                pago = inscripcion;

            }

        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pago;
    }

    public String pagoPareja(Integer id) {
        String pago = "";
        JugadorTorneo jugadorTorneo4;
        try {
            jugadorTorneo4 = pagosAdService.getJugadorxTorneo(idtorneo, id);
            Torneo torneo3 = pagosAdService.getTorneos(idtorneo);
            if (jugadorTorneo4.getPareja() == 0) {
                pago = "0";
            } else {
                String inscripcion = String.valueOf(torneo3.getInspar());
                pago = inscripcion;

            }
        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pago;
    }

    public String pagoEquipo(Integer id) {
        String pago = "";
        JugadorTorneo jugadorTorneo5;
        try {
            jugadorTorneo5 = pagosAdService.getJugadorxTorneo(idtorneo, id);
            Torneo torneo4 = pagosAdService.getTorneos(idtorneo);
            if (jugadorTorneo5.getEquipo() == 0) {
                pago = "0";
            } else {
                String inscripcion = String.valueOf(torneo4.getInsequ());
                pago = inscripcion;




            }
        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pago;
    }

    public void calculaTotal() {
        Float suma1;
        Float sumapareja1;
        Float sumaequ1;
        Float sumaequ2;
        Float sumaequ3;
        if (checkindividual.equals("true")) {
            suma1 = suma;
        } else {
            suma1 = Float.parseFloat("0");
        }
        if (checkpareja.equals("true")) {
            sumapareja1 = sumapareja;
        } else {
            sumapareja1 = Float.parseFloat("0");
        }
        if (checkequipo1.equals("true")) {
            sumaequ1 = sumaequipo1;
        } else {
            sumaequ1 = Float.parseFloat("0");
        }
        if (checkequipo2.equals("true")) {
            sumaequ2 = sumaequipo2;
        } else {
            sumaequ2 = Float.parseFloat("0");
        }
        if (checkequipo3.equals("true")) {
            sumaequ3 = sumaequipo3;
        } else {
            sumaequ3 = Float.parseFloat("0");
        }
        sumatotal = suma1 + sumapareja1 + sumaequ1 + sumaequ2 + sumaequ3;
    }

    public void confirmaPago() {
        pago = 1;
    }

    public Integer idnull(Integer id) {
        if (id == null) {
            return 0;
        } else {
            return id;
        }
    }

    public void efectivoPago() {
        try {
            if (checkindividual.equals("true")) {
                generaPago(idtorneo, idjugador, "Efectivo");
                pagosAdService.insertaPagoJugador(idjugador, idtorneo, 0, "Efectivo", "Efectivo", "Efectivo", 1);
            }
            if (checkpareja.equals("true")) {
                idpareja = pagosAdService.idPareja(idtorneo, idjugador);
                generaPago(idtorneo, idpareja, "Efectivo");
                pagosAdService.insertaPagoJugador(idpareja, idtorneo, 0, "Efectivo", "Efectivo", "Efectivo", 2);
            }
            if (checkequipo1.equals("true")) {
                generaPago(idtorneo, idequipo1, "Efectivo");
                pagosAdService.insertaPagoJugador(idequipo1, idtorneo, 0, "Efectivo", "Efectivo", "Efectivo", 3);
            }
            if (checkequipo2.equals("true")) {
                generaPago(idtorneo, idequipo2, "Efectivo");
                pagosAdService.insertaPagoJugador(idequipo2, idtorneo, 0, "Efectivo", "Efectivo", "Efectivo", 3);
            }
            if (checkequipo3.equals("true")) {
                generaPago(idtorneo, idequipo3, "Efectivo");
                pagosAdService.insertaPagoJugador(idequipo3, idtorneo, 0, "Efectivo", "Efectivo", "Efectivo", 3);
            }



        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void tarjetaPago() {
        try {
            if (checkindividual.equals("true")) {
                generaPago(idtorneo, idjugador, "Tarjeta");
                pagosAdService.insertaPagoJugador(idjugador, idtorneo, 1, iden, noiden, comen, 1);
            }
            if (checkpareja.equals("true")) {
                idpareja = pagosAdService.idPareja(idtorneo, idjugador);
                generaPago(idtorneo, idpareja, "Tarjeta");
                pagosAdService.insertaPagoJugador(idpareja, idtorneo, 1, iden, noiden, comen, 2);
            }
            if (checkequipo1.equals("true")) {
                generaPago(idtorneo, idequipo1, "Tarjeta");
                pagosAdService.insertaPagoJugador(idequipo1, idtorneo, 1, iden, noiden, comen, 3);
            }
            if (checkequipo2.equals("true")) {
                generaPago(idtorneo, idequipo2, "Tarjeta");
                pagosAdService.insertaPagoJugador(idequipo2, idtorneo, 1, iden, noiden, comen, 3);
            }
            if (checkequipo3.equals("true")) {
                generaPago(idtorneo, idequipo3, "Tarjeta");
                pagosAdService.insertaPagoJugador(idequipo3, idtorneo, 1, iden, noiden, comen, 3);
            }

            FacesContext.getCurrentInstance().getExternalContext().redirect("modulopagos.xhtml");
        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generaPago(Integer idtor, Integer idjug, String tipopago) {

        try {
            JugadorTorneo jugadorTorneo;
            jugadorTorneo = pagosAdService.getJugadorxTorneo(idtor, idjug);
            String adj = "";
            String adj2 = "";
            String adjnom = "";
            CreateReciboPDF createReciboPDF = new CreateReciboPDF();
            String expedido = "Bernardo Couto 70; Col. Algarín; CP 06880; Delegación Cuauhtémoc;Ciudad de México.";
            Integer maxidcontrol = pagosAdService.getMaxControl(idtor);
            pagoControl = new PagoControl(null, idjug, idtor, maxidcontrol);
            torneoControl = new TorneoControl(null, idjug, idtor);
            pagosAdService.insertPagoControl(pagoControl);
            pagosAdService.insertTorneoControl(torneoControl);
            torneo = pagosAdService.getTorneos(idtor);
            jugadorTorneo = pagosAdService.getJugadorxTorneo(idtor, idjug);
            String nombre = pagosAdService.getNombreEquipo(idjug);
            Float individual = torneo.getInsind();
            Float pareja = torneo.getInspar();
            Float equipo = torneo.getInsequ();
            modalidad = "";


            if (jugadorTorneo.getIndividual() == 1) {
                if (individual <= 2000) {
                    Integer maxpagocontrol = pagosAdService.getPAgoControl();
                    modalidad = modalidad + " individual ";
                    String valor = String.valueOf(individual);
                    String unidadAmparo = "Participación en la modalidad de: Individual";
                    createReciboPDF.generarComprobantePDFIndividual(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), individual, valor, unidadAmparo, "Efectivo");
                    adj = adj + "Comprobante de pago.pdf;";


                } else {
                    Integer resultado;
                    Integer numero2 = 2000;
                    Integer numero3 = (int) individual.doubleValue();
                    resultado = numero3 / numero2;
                    int comprobantes = numero3 - (resultado * numero2);


                    if (comprobantes == 0) {
                        for (int i = 0; i <= (resultado - 1); i++) {
                            CreateReciboPDF createReciboPDF2 = new CreateReciboPDF();
                            Integer maxpagocontrol = pagosAdService.getPAgoControl();
                            modalidad = modalidad + " individual ";
                            String valor = String.valueOf(individual);
                            String unidadAmparo = "Participación en la modalidad de: Individual";
                            createReciboPDF2.generarComprobantePDFIndividual(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), individual, "2,000", unidadAmparo, tipopago);
                            adj2 = "Comprobante de pago.pdf;";
                            EnviarCorreo correo = new EnviarCorreo();
                            correo.sendEmail(adj2, adj2);
                            adj2 = "";
                        }

                        Integer maxpagocontrol = pagosAdService.getPAgoControl();
                        modalidad = modalidad + " individual ";
                        String valor = String.valueOf(individual);
                        String unidadAmparo = "Participación en la modalidad de: Individual";
                        createReciboPDF.generarComprobantePDFIndividual(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), individual, "2,000", unidadAmparo, tipopago);
                        adj = adj + "Comprobante de pago.pdf;";


                    } else {


                        for (int i = 0; i <= resultado; i++) {
                            CreateReciboPDF createReciboPDF2 = new CreateReciboPDF();
                            Integer maxpagocontrol = pagosAdService.getPAgoControl();
                            modalidad = modalidad + " individual ";
                            String valor = String.valueOf(individual);
                            String unidadAmparo = "Participación en la modalidad de: Individual";
                            createReciboPDF2.generarComprobantePDFIndividual(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), individual, "2,000", unidadAmparo, tipopago);
                            adj2 = "Comprobante de pago.pdf;";
                            EnviarCorreo correo = new EnviarCorreo();
                            correo.sendEmail(adj2, adj2);
                            adj2 = "";
                        }


                        String comp = String.valueOf(comprobantes);
                        Integer maxpagocontrol = pagosAdService.getPAgoControl();
                        modalidad = modalidad + " individual ";
                        String valor = String.valueOf(individual);
                        String unidadAmparo = "Participación en la modalidad de: Individual";
                        createReciboPDF.generarComprobantePDFIndividual(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), individual, comp, unidadAmparo, tipopago);
                        adj = adj + "Comprobante de pago.pdf;";
                    }


                }
                torneoControl = new TorneoControl(null, idjug, idtor);
                pagosAdService.insertTorneoControl(torneoControl);

            }




            if (jugadorTorneo.getPareja() == 1) {
                if (pareja <= 2000) {
                    Integer maxpagocontrol = pagosAdService.getPAgoControl();
                    modalidad = modalidad + " pareja ";
                    String valor = String.valueOf(pareja);
                    String unidadAmparo = "Participación en la modalidad de: Pareja";
                    createReciboPDF.generarComprobantePDFPareja(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), pareja, valor, unidadAmparo, tipopago);
                    adj = adj + "Comprobante de pago pareja.pdf;";
                } else {
                    Integer resultado;
                    Integer numero2 = 2000;
                    Integer numero3 = (int) pareja.doubleValue();
                    resultado = numero3 / numero2;
                    int comprobantes = numero3 - (resultado * numero2);
                    if (comprobantes == 0) {
                        for (int i = 0; i <= (resultado - 1); i++) {
                            CreateReciboPDF createReciboPDF2 = new CreateReciboPDF();
                            Integer maxpagocontrol = pagosAdService.getPAgoControl();
                            modalidad = modalidad + " pareja";
                            String valor = String.valueOf(pareja);
                            String unidadAmparo = "Participación en la modalidad de: Pareja";
                            createReciboPDF2.generarComprobantePDFPareja(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), pareja, "2,000", unidadAmparo, tipopago);
                            adj2 = "Comprobante de pago pareja.pdf;";
                            EnviarCorreo correo = new EnviarCorreo();
                            correo.sendEmail(adj2, adj2);
                            adj2 = "";
                        }

                        Integer maxpagocontrol = pagosAdService.getPAgoControl();
                        modalidad = modalidad + " pareja ";
                        String valor = String.valueOf(individual);
                        String unidadAmparo = "Participación en la modalidad de: Pareja";
                        createReciboPDF.generarComprobantePDFPareja(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), pareja, "2,000", unidadAmparo, tipopago);
                        adj = adj + "Comprobante de pago pareja.pdf;";
                    } else {
                        for (int i = 0; i <= resultado; i++) {
                            CreateReciboPDF createReciboPDF2 = new CreateReciboPDF();
                            Integer maxpagocontrol = pagosAdService.getPAgoControl();
                            modalidad = modalidad + " pareja ";
                            String valor = String.valueOf(pareja);
                            String unidadAmparo = "Participación en la modalidad de: Pareja";
                            createReciboPDF2.generarComprobantePDFPareja(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), pareja, "2,000", unidadAmparo, tipopago);
                            adj2 = "Comprobante de pago pareja.pdf;";
                            EnviarCorreo correo = new EnviarCorreo();
                            correo.sendEmail(adj2, adj2);
                            adj2 = "";
                        }
                        String comp = String.valueOf(comprobantes);
                        Integer maxpagocontrol = pagosAdService.getPAgoControl();
                        modalidad = modalidad + " pareja ";
                        String valor = String.valueOf(pareja);
                        String unidadAmparo = "Participación en la modalidad de: Pareja";
                        createReciboPDF.generarComprobantePDFPareja(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), pareja, comp, unidadAmparo, tipopago);
                        adj = adj + "Comprobante de pago pareja.pdf;";
                    }

                }
                torneoControl = new TorneoControl(null, idjug, idtor);
                pagosAdService.insertTorneoControl(torneoControl);
            }





            if (jugadorTorneo.getEquipo() == 1) {
                if (equipo <= 2000) {
                    Integer maxpagocontrol = pagosAdService.getPAgoControl();
                    modalidad = modalidad + " equipo ";
                    String valor = String.valueOf(pareja);
                    String unidadAmparo = "Participación en la modalidad de: Equipo";
                    createReciboPDF.generarComprobantePDFEquipo(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), equipo, valor, unidadAmparo, tipopago);
                    adj = adj + "Comprobante de pago equipo.pdf;";
                } else {
                    Integer resultado;
                    Integer numero2 = 2000;
                    Integer numero3 = (int) equipo.doubleValue();
                    resultado = numero3 / numero2;
                    int comprobantes = numero3 - (resultado * numero2);
                    if (comprobantes == 0) {
                        for (int i = 0; i < (resultado - 1); i++) {
                            CreateReciboPDF createReciboPDF2 = new CreateReciboPDF();
                            Integer maxpagocontrol = pagosAdService.getPAgoControl();
                            modalidad = modalidad + " equipo ";
                            String valor = String.valueOf(individual);
                            String unidadAmparo = "Participación en la modalidad de: Equipo";
                            createReciboPDF2.generarComprobantePDFEquipo(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), equipo, "2,000", unidadAmparo, tipopago);
                            adj2 = "Comprobante de pago equipo.pdf;";
                            EnviarCorreo correo = new EnviarCorreo();
                            correo.sendEmail(adj2, adj2);
                            adj2 = "";
                        }

                        Integer maxpagocontrol = pagosAdService.getPAgoControl();
                        modalidad = modalidad + " equipo ";
                        String valor = String.valueOf(individual);
                        String unidadAmparo = "Participación en la modalidad de: Equipo";
                        createReciboPDF.generarComprobantePDFEquipo(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), equipo, "2,000", unidadAmparo, tipopago);
                        adj = adj + "Comprobante de pago equipo.pdf;";
                    } else {
                        for (int i = 0; i < resultado; i++) {
                            CreateReciboPDF createReciboPDF2 = new CreateReciboPDF();
                            Integer maxpagocontrol = pagosAdService.getPAgoControl();
                            modalidad = modalidad + " equipo ";
                            String valor = String.valueOf(individual);
                            String unidadAmparo = "Participación en la modalidad de: Equipo";
                            createReciboPDF2.generarComprobantePDFEquipo(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), equipo, "2,000", unidadAmparo, tipopago);
                            adj2 = "Comprobante de pago equipo.pdf;";
                            EnviarCorreo correo = new EnviarCorreo();
                            correo.sendEmail(adj2, adj2);
                            adj2 = "";
                        }
                        String comp = String.valueOf(comprobantes);
                        Integer maxpagocontrol = pagosAdService.getPAgoControl();
                        modalidad = modalidad + " equipo ";
                        String valor = String.valueOf(individual);
                        String unidadAmparo = "Participación en la modalidad de: Equipo";
                        createReciboPDF.generarComprobantePDFEquipo(maxpagocontrol, torneo.getSede(), torneo.getFechaHoraio(), expedido, torneo.getTorneo(), equipo, comp, unidadAmparo, tipopago);
                        adj = adj + "Comprobante de pago equipo.pdf;";
                    }
                }
                torneoControl = new TorneoControl(null, idjug, idtor);
                pagosAdService.insertTorneoControl(torneoControl);
            }


            String numerogafete = String.valueOf(maxidcontrol);
            adj = adj + "Gafete.pdf";
            createReciboPDF.crearGafetePDF(torneo.getTorneo(), numerogafete, modalidad, nombre, torneo.getSede(), torneo.getFechaHoraio());
            EnviarCorreo correo = new EnviarCorreo();
            correo.sendEmail(adj, adj);

        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void bloqueaPago(Integer idtorneo, Integer idjugador) {
        try {
            FormacionEquipo formacionEquipo3 = this.pagosAdService.formEquipo(idtorneo, idjugador);
            JugadorTorneo jugadorTorneo = this.pagosAdService.getJugadorxTorneo(idtorneo, idjugador);
            Integer individual = jugadorTorneo.getIndividual();
            Integer pareja = jugadorTorneo.getPareja();
            Integer equipo = jugadorTorneo.getEquipo();
            if (individual == 1) {
                Integer rows = this.pagosAdService.getrowsPagoControl(idtorneo, idjugador);
                System.out.println("ROWS" + rows);
                if (rows == 1) {
                    booind = false;
                    chkind = true;

                } else {
                    booind = true;
                    chkind = false;

                }
            }

            if (pareja == 1) {
                Integer idpar = this.pagosAdService.idPareja(idtorneo, idjugador);
                Integer rows = this.pagosAdService.getrowsPagoControl(idtorneo, idpar);
                if (rows == 1) {
                    boopar = false;
                    chkpar = true;


                } else {
                    boopar = true;
                    chkpar = false;


                }
            }

            if (equipo == 1) {
                FormacionEquipo formacionEquipo = this.pagosAdService.formEquipo(idtorneo, idjugador);
                if (formacionEquipo.getIdjugador1() == idjugador) {
                    Integer rows1 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador2());
                    Integer rows2 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador3());
                    Integer rows3 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador4());
                    booequ = this.pagosAdService.bloqueaPago1(rows1);
                    booequ2 = this.pagosAdService.bloqueaPago1(rows2);
                    booequ3 = this.pagosAdService.bloqueaPago1(rows3);
                    chkeq1 = this.pagosAdService.bloqueaPago2(rows1);
                    chkeq2 = this.pagosAdService.bloqueaPago2(rows2);
                    chkeq3 = this.pagosAdService.bloqueaPago2(rows3);
                }
                if (formacionEquipo.getIdjugador2() == idjugador) {
                    Integer rows1 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador1());
                    Integer rows2 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador3());
                    Integer rows3 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador4());
                    booequ = this.pagosAdService.bloqueaPago1(rows1);
                    booequ2 = this.pagosAdService.bloqueaPago1(rows2);
                    booequ3 = this.pagosAdService.bloqueaPago1(rows3);
                    chkeq1 = this.pagosAdService.bloqueaPago2(rows1);
                    chkeq2 = this.pagosAdService.bloqueaPago2(rows2);
                    chkeq3 = this.pagosAdService.bloqueaPago2(rows3);
                }
                if (formacionEquipo.getIdjugador3() == idjugador) {
                    Integer rows1 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador1());
                    Integer rows2 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador2());
                    Integer rows3 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador4());
                    booequ = this.pagosAdService.bloqueaPago1(rows1);
                    booequ2 = this.pagosAdService.bloqueaPago1(rows2);
                    booequ3 = this.pagosAdService.bloqueaPago1(rows3);
                    chkeq1 = this.pagosAdService.bloqueaPago2(rows1);
                    chkeq2 = this.pagosAdService.bloqueaPago2(rows2);
                    chkeq3 = this.pagosAdService.bloqueaPago2(rows3);
                }
                if (formacionEquipo.getIdjugador4() == idjugador) {
                    Integer rows1 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador1());
                    Integer rows2 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador2());
                    Integer rows3 = this.pagosAdService.getrowsPagoControl(idtorneo, formacionEquipo.getIdjugador3());
                    booequ = this.pagosAdService.bloqueaPago1(rows1);
                    booequ2 = this.pagosAdService.bloqueaPago1(rows2);
                    booequ3 = this.pagosAdService.bloqueaPago1(rows3);
                    chkeq1 = this.pagosAdService.bloqueaPago2(rows1);
                    chkeq2 = this.pagosAdService.bloqueaPago2(rows2);
                    chkeq3 = this.pagosAdService.bloqueaPago2(rows3);
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(PagosAdManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public PagosAdService getPagosAdService() {
        return pagosAdService;
    }

    public void setPagosAdService(PagosAdService pagosAdService) {
        this.pagosAdService = pagosAdService;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public List<SelectItem> getTorneos() {
        return torneos;
    }

    public void setTorneos(List<SelectItem> torneos) {
        this.torneos = torneos;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public String getNametorneo() {
        return nametorneo;
    }

    public void setNametorneo(String nametorneo) {
        this.nametorneo = nametorneo;
    }

    public Integer getVarpagos() {
        return varpagos;
    }

    public void setVarpagos(Integer varpagos) {
        this.varpagos = varpagos;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public List<SelectItem> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<SelectItem> jugadores) {
        this.jugadores = jugadores;
    }

    public boolean isBooequ() {
        return booequ;
    }

    public void setBooequ(boolean booequ) {
        this.booequ = booequ;
    }

    public boolean isBooind() {
        return booind;
    }

    public void setBooind(boolean booind) {
        this.booind = booind;
    }

    public boolean isBoopar() {
        return boopar;
    }

    public void setBoopar(boolean boopar) {
        this.boopar = boopar;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public String getInsequ() {
        return insequ;
    }

    public void setInsequ(String insequ) {
        this.insequ = insequ;
    }

    public String getInsind() {
        return insind;
    }

    public void setInsind(String insind) {
        this.insind = insind;
    }

    public String getInspar() {
        return inspar;
    }

    public void setInspar(String inspar) {
        this.inspar = inspar;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Float getSuma() {
        return suma;
    }

    public void setSuma(Float suma) {
        this.suma = suma;
    }

    public String getParejaname() {
        return parejaname;
    }

    public void setParejaname(String parejaname) {
        this.parejaname = parejaname;
    }

    public String getInsequpar() {
        return insequpar;
    }

    public void setInsequpar(String insequpar) {
        this.insequpar = insequpar;
    }

    public String getInsindpar() {
        return insindpar;
    }

    public void setInsindpar(String insindpar) {
        this.insindpar = insindpar;
    }

    public String getInsparpar() {
        return insparpar;
    }

    public void setInsparpar(String insparpar) {
        this.insparpar = insparpar;
    }

    public Float getSumapareja() {
        return sumapareja;
    }

    public void setSumapareja(Float sumapareja) {
        this.sumapareja = sumapareja;
    }

    public String getInsequequ1() {
        return insequequ1;
    }

    public void setInsequequ1(String insequequ1) {
        this.insequequ1 = insequequ1;
    }

    public String getInsequequ2() {
        return insequequ2;
    }

    public void setInsequequ2(String insequequ2) {
        this.insequequ2 = insequequ2;
    }

    public String getInsequequ3() {
        return insequequ3;
    }

    public void setInsequequ3(String insequequ3) {
        this.insequequ3 = insequequ3;
    }

    public String getInsindequ1() {
        return insindequ1;
    }

    public void setInsindequ1(String insindequ1) {
        this.insindequ1 = insindequ1;
    }

    public String getInsindequ2() {
        return insindequ2;
    }

    public void setInsindequ2(String insindequ2) {
        this.insindequ2 = insindequ2;
    }

    public String getInsindequ3() {
        return insindequ3;
    }

    public void setInsindequ3(String insindequ3) {
        this.insindequ3 = insindequ3;
    }

    public String getInsparequ1() {
        return insparequ1;
    }

    public void setInsparequ1(String insparequ1) {
        this.insparequ1 = insparequ1;
    }

    public String getInsparequ2() {
        return insparequ2;
    }

    public void setInsparequ2(String insparequ2) {
        this.insparequ2 = insparequ2;
    }

    public String getInsparequ3() {
        return insparequ3;
    }

    public void setInsparequ3(String insparequ3) {
        this.insparequ3 = insparequ3;
    }

    public Float getSumaequipo1() {
        return sumaequipo1;
    }

    public void setSumaequipo1(Float sumaequipo1) {
        this.sumaequipo1 = sumaequipo1;
    }

    public Float getSumaequipo2() {
        return sumaequipo2;
    }

    public void setSumaequipo2(Float sumaequipo2) {
        this.sumaequipo2 = sumaequipo2;
    }

    public Float getSumaequipo3() {
        return sumaequipo3;
    }

    public void setSumaequipo3(Float sumaequipo3) {
        this.sumaequipo3 = sumaequipo3;
    }

    public String getEquiponame1() {
        return equiponame1;
    }

    public void setEquiponame1(String equiponame1) {
        this.equiponame1 = equiponame1;
    }

    public String getEquiponame2() {
        return equiponame2;
    }

    public void setEquiponame2(String equiponame2) {
        this.equiponame2 = equiponame2;
    }

    public String getEquiponame3() {
        return equiponame3;
    }

    public void setEquiponame3(String equiponame3) {
        this.equiponame3 = equiponame3;
    }

    public String getCheckequipo1() {
        return checkequipo1;
    }

    public void setCheckequipo1(String checkequipo1) {
        this.checkequipo1 = checkequipo1;
    }

    public String getCheckequipo2() {
        return checkequipo2;
    }

    public void setCheckequipo2(String checkequipo2) {
        this.checkequipo2 = checkequipo2;
    }

    public String getCheckequipo3() {
        return checkequipo3;
    }

    public void setCheckequipo3(String checkequipo3) {
        this.checkequipo3 = checkequipo3;
    }

    public String getCheckindividual() {
        return checkindividual;
    }

    public void setCheckindividual(String checkindividual) {
        this.checkindividual = checkindividual;
    }

    public String getCheckpareja() {
        return checkpareja;
    }

    public void setCheckpareja(String checkpareja) {
        this.checkpareja = checkpareja;
    }

    public Float getSumatotal() {
        return sumatotal;
    }

    public void setSumatotal(Float sumatotal) {
        this.sumatotal = sumatotal;
    }

    public int getPago() {
        return pago;
    }

    public void setPago(int pago) {
        this.pago = pago;
    }

    public boolean isTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(boolean tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getIden() {
        return iden;
    }

    public void setIden(String iden) {
        this.iden = iden;
    }

    public Integer getIdequipo1() {
        return idequipo1;
    }

    public void setIdequipo1(Integer idequipo1) {
        this.idequipo1 = idequipo1;
    }

    public Integer getIdequipo2() {
        return idequipo2;
    }

    public void setIdequipo2(Integer idequipo2) {
        this.idequipo2 = idequipo2;
    }

    public Integer getIdequipo3() {
        return idequipo3;
    }

    public void setIdequipo3(Integer idequipo3) {
        this.idequipo3 = idequipo3;
    }

    public Integer getIdpareja() {
        return idpareja;
    }

    public void setIdpareja(Integer idpareja) {
        this.idpareja = idpareja;
    }

    public PagoControl getPagoControl() {
        return pagoControl;
    }

    public void setPagoControl(PagoControl pagoControl) {
        this.pagoControl = pagoControl;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public TorneoControl getTorneoControl() {
        return torneoControl;
    }

    public void setTorneoControl(TorneoControl torneoControl) {
        this.torneoControl = torneoControl;
    }

    public String getComen() {
        return comen;
    }

    public void setComen(String comen) {
        this.comen = comen;
    }

    public String getNoiden() {
        return noiden;
    }

    public void setNoiden(String noiden) {
        this.noiden = noiden;
    }

    public boolean isBooequ2() {
        return booequ2;
    }

    public void setBooequ2(boolean booequ2) {
        this.booequ2 = booequ2;
    }

    public boolean isBooequ3() {
        return booequ3;
    }

    public void setBooequ3(boolean booequ3) {
        this.booequ3 = booequ3;
    }

    public boolean isChkeq1() {
        return chkeq1;
    }

    public void setChkeq1(boolean chkeq1) {
        this.chkeq1 = chkeq1;
    }

    public boolean isChkeq2() {
        return chkeq2;
    }

    public void setChkeq2(boolean chkeq2) {
        this.chkeq2 = chkeq2;
    }

    public boolean isChkeq3() {
        return chkeq3;
    }

    public void setChkeq3(boolean chkeq3) {
        this.chkeq3 = chkeq3;
    }

    public boolean isChkind() {
        return chkind;
    }

    public void setChkind(boolean chkind) {
        this.chkind = chkind;
    }

    public boolean isChkpar() {
        return chkpar;
    }

    public void setChkpar(boolean chkpar) {
        this.chkpar = chkpar;
    }
}