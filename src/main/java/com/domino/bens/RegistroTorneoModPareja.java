
package com.domino.bens;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="registrotorneomodpareja")
public class RegistroTorneoModPareja implements Serializable{
    
@Id
@GeneratedValue (strategy = GenerationType.IDENTITY)
@Column (name="idRTMP")   
    private Integer idRTMP;

@Column (name="idjugador_x_torneo")   
    private Integer idjugador_x_torneo;

@Column (name="idTorneo")   
    private Integer idTorneo;

@Column (name="idjugador")   
    private Integer idjugador;

@Column (name="activo")   
    private String activo;

@Column (name="fechaRTMP")   
    private String fechaRTMP;

    public RegistroTorneoModPareja(){
    }

    public RegistroTorneoModPareja(Integer idRTMP, Integer idjugador_x_torneo, Integer idTorneo, Integer idjugador, String activo, String fechaRTMP) {
        this.idRTMP = idRTMP;
        this.idjugador_x_torneo = idjugador_x_torneo;
        this.idTorneo = idTorneo;
        this.idjugador = idjugador;
        this.activo = activo;
        this.fechaRTMP = fechaRTMP;
    }

    public Integer getIdRTMP() {
        return idRTMP;
    }

    public void setIdRTMP(Integer idRTMP) {
        this.idRTMP = idRTMP;
    }

    public Integer getIdjugador_x_torneo() {
        return idjugador_x_torneo;
    }

    public void setIdjugador_x_torneo(Integer idjugador_x_torneo) {
        this.idjugador_x_torneo = idjugador_x_torneo;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getFechaRTMP() {
        return fechaRTMP;
    }

    public void setFechaRTMP(String fechaRTMP) {
        this.fechaRTMP = fechaRTMP;
    }

    


}