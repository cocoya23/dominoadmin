package com.domino.dao;

import com.domino.bens.*;
import com.domino.util.HibernateUtil;
import com.mysql.jdbc.CallableStatement;
import com.mysql.jdbc.PreparedStatement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.management.Query;
import javax.transaction.Transaction;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Sergio Cano
 */
public class TorneoDAO {

    public void insertTorneo(Torneo torneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(torneo);
        session.getTransaction().commit();
        session.close();
    }

    public List<Torneo> getTorneos() throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<Torneo> result = session.createQuery("from Torneo").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public List<Torneo> getTorneoxID(Integer idTorn) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Torneo.class).
                add(Restrictions.eq("idTorneo", idTorn)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public void updateTorneo(Torneo torneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.update(torneo);
        session.getTransaction().commit();
        session.close();
    }

   /** public void deleteFormacionpareja(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<FormacionPareja> formacionpareja = session.createCriteria(FormacionPareja.class).
                add(Restrictions.eq("idtorneo", idTorneo)).list();
        int n = formacionpareja.size();
        if (n == 0) {
            session.getTransaction().commit();
            session.close();
        } else {
            session.delete(formacionpareja);
            session.getTransaction().commit();
            session.close();
        }

    }

    public void deleteFormacionequipo(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<FormacionEquipo> formacionequipo = session.createCriteria(FormacionEquipo.class).
                add(Restrictions.eq("idtorneo", idTorneo)).list();
        int n = formacionequipo.size();
        if (n == 0) {
            session.getTransaction().commit();
            session.close();
        } else {
            session.delete(formacionequipo);
            session.getTransaction().commit();
            session.close();
        }

    }

    public void deleteRegistroTorneoModPareja(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        RegistroTorneoModPareja formacionpareja = (RegistroTorneoModPareja) session.createCriteria(RegistroTorneoModPareja.class).
                add(Restrictions.eq("idTorneo", idTorneo)).list();

        if (formacionpareja == null) {
            session.getTransaction().commit();
            session.close();
        } else {
            session.delete(formacionpareja);
            session.getTransaction().commit();
            session.close();
        }

    }

    public void deleteRegistroTorneoModEquipo(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<RegistroTorneoModEquipo> formacionequipo = session.createCriteria(RegistroTorneoModEquipo.class).
                add(Restrictions.eq("idTorneo", idTorneo)).list();
        int n = formacionequipo.size();
        if (n == 0) {
            session.getTransaction().commit();
            session.close();
        } else {
            session.delete(formacionequipo);
            session.getTransaction().commit();
            session.close();
        }

    }

    public void updateJugadorTorneoPareja(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.eq("idTorneo", idTorneo)).list();
        if (jugadortorneo == null) {
            session.getTransaction().commit();
            session.close();
        } else {
            jugadortorneo.setPareja(0);
            session.update(jugadortorneo);
            session.getTransaction().commit();
            session.close();
        }

    }

    public void updateJugadorTorneoEquipo(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.eq("idTorneo", idTorneo)).list();
        if (jugadortorneo == null) {
            session.getTransaction().commit();
            session.close();
        } else {
            jugadortorneo.setEquipo(0);
            session.update(jugadortorneo);
            session.getTransaction().commit();
            session.close();
        }
    }

    public void deleteJugadorTorneo(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<JugadorTorneo> jugadortorneo = session.createCriteria(JugadorTorneo.class).
                add(Restrictions.eq("idTorneo", idTorneo)).list();
        int n = jugadortorneo.size();
        if (n == 0) {
            session.getTransaction().commit();
            session.close();
        } else {
            session.delete(jugadortorneo);
            session.getTransaction().commit();
            session.close();
        }

    }

    public void deleteTorneo(Integer idTorneo) throws Exception {
        System.out.println("-----***" + idTorneo + "****---");
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Torneo torneo = (Torneo) session.createCriteria(Torneo.class).
                add(Restrictions.eq("idTorneo", idTorneo)).list();
        if (torneo == null) {
            session.getTransaction().commit();
            session.close();
        } else {
            session.delete(torneo);
            session.getTransaction().commit();
            session.close();
        }

    }**/

    public void deleteTorneoSP(Integer idTorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        SQLQuery q = session.createSQLQuery("CALL sp_deletetorneo(:torneo)");
        q.setParameter("torneo", idTorneo);
        q.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
    
     public void parejaSP(Integer idTorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        SQLQuery q = session.createSQLQuery("CALL sp_deletepareja(:torneo)");
        q.setParameter("torneo", idTorneo);
        q.executeUpdate(); 
        session.getTransaction().commit();
        session.close();
    
     }
     public void equipoSP(Integer idTorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        SQLQuery q = session.createSQLQuery("CALL sp_deleteequipo(:torneo)");
        q.setParameter("torneo", idTorneo);
        q.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }
}
