package com.domino.service;

import com.domino.bens.Torneo;
import com.domino.dao.TorneoDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

@ManagedBean(name = "torneoService", eager = true)
@ApplicationScoped
public class TorneoService implements Serializable {

    TorneoDAO torneoDao = new TorneoDAO();

    public void insertTorneo(Torneo torneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        new TorneoDAO().insertTorneo(torneo);
    }

    public List<Torneo> getTorneos() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Torneo> torneos = this.torneoDao.getTorneos();
        return torneos;
    }

    public List<Torneo> getTorneoxID(Integer idTorn) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Torneo> torneos = this.torneoDao.getTorneoxID(idTorn);
        return torneos;
    }

    public void updateJugador(Torneo torneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDao.updateTorneo(torneo);
    }

    
    public void deleteTorneoSP(Integer idTorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDao.deleteTorneoSP(idTorneo);
    }

    public void parejaSP(Integer idTorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDao.parejaSP(idTorneo);
    }

    public void equipoSP(Integer idTorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDao.equipoSP(idTorneo);
    }
}
