/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.service;


import com.domino.bens.Jugador;
import com.domino.dao.JugadoresDAO;
import com.domino.dao.MetropoliDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

/**
 *
 * @author Sergio Cano
 */
@ManagedBean(name = "jugadorService", eager = true)
@ApplicationScoped
public class JugadorService implements Serializable{
    JugadoresDAO jugadorDAO = new JugadoresDAO();
    
     public List<Jugador> getJugadores() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Jugador> torneos = this.jugadorDAO.getJugadores();
        return torneos;
    }
    
}
