package com.domino.dao;

import com.domino.bens.Jugador;
import com.domino.bens.Metropoli;
import com.domino.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Sergio Cano
 */
public class MetropoliDAO {
    
    public List<Metropoli> getMetropoli() throws Exception {

        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Metropoli.class).         
            add(Restrictions.eq("idPais", 1)).list();
        session.getTransaction().commit();
        session.close();

        return result;
    }
   public List<Metropoli> getMetropoliId(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Metropoli.class).         
            add(Restrictions.eq("idPais", id)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
   
public void insertMetropoli(Metropoli metropoli) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(metropoli);
        session.getTransaction().commit();
        session.close();

    }

public List<Metropoli> getMetropoliMet(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<Metropoli> result = session.createCriteria(Metropoli.class).         
            add(Restrictions.eq("idPais", id)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

 public void updateMetropoli(Metropoli metropoli) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(metropoli);
        session.getTransaction().commit();
        session.close();

    }
 
  public void deleteMetropoli(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Metropoli metropoli = (Metropoli) session.createCriteria(Metropoli.class).
                add(Restrictions.eq("idMetropoli", id)).uniqueResult();
        session.delete(metropoli);
        session.getTransaction().commit();
        session.close();

    }
  
     public Integer getRowsJugador(Integer idmet) throws Exception {
        Integer rows;
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Jugador.class).         
            add(Restrictions.eq("metropoli", idmet)).list();
        rows=result.size();
        session.getTransaction().commit();
        session.close();

        return rows;
    }
 
    
}
