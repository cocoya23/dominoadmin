/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.bens;

import javax.persistence.*;

/**
 *
 * @author Sergio Cano
 */
@Entity
@Table(name = "pago_pareja_paso")
public class PagoParejaPaso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idinscripcion")
    private Integer idInscripcion;
    @Column(name = "idformacionpareja")
    private Integer idFormacionPareja;
    @Column(name = "idTorneo")
    private Integer idTorneo;
    @Column(name = "pais")
    private Integer pais;
    @Column(name = "metropoli")
    private Integer metropoli;
    @Column(name = "club")
    private String club;
    @Column(name = "activo")
    private Integer activo;
    @Column(name = "tipo_pago")
    private Integer tipoPago;
    @Column(name = "identificacion")
    private String identificacion;
    @Column(name = "noidentificacion")
    private String noIdentificacion;
    @Column(name = "comentarios")
    private String comentatios;
    @Column(name = "idjugador")
    private Integer idjugador;


    public PagoParejaPaso() {
    }

    public PagoParejaPaso(Integer idInscripcion, Integer idFormacionPareja,
            Integer idTorneo, Integer pais, Integer metropoli, String club,
            Integer activo, Integer tipoPago, String identificacion,
            String noIdentificacion, String comentatios, Integer idjugador) {
        this.idInscripcion = idInscripcion;
        this.idFormacionPareja = idFormacionPareja;
        this.idTorneo = idTorneo;
        this.pais = pais;
        this.metropoli = metropoli;
        this.club = club;
        this.activo = activo;
        this.tipoPago = tipoPago;
        this.identificacion = identificacion;
        this.noIdentificacion = noIdentificacion;
        this.comentatios = comentatios;
        this.idjugador = idjugador;
     
    }

    public Integer getIdInscripcion() {
        return idInscripcion;
    }

    public void setIdInscripcion(Integer idInscripcion) {
        this.idInscripcion = idInscripcion;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getComentatios() {
        return comentatios;
    }

    public void setComentatios(String comentatios) {
        this.comentatios = comentatios;
    }

    public Integer getIdFormacionPareja() {
        return idFormacionPareja;
    }

    public void setIdFormacionPareja(Integer idFormacionPareja) {
        this.idFormacionPareja = idFormacionPareja;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public String getNoIdentificacion() {
        return noIdentificacion;
    }

    public void setNoIdentificacion(String noIdentificacion) {
        this.noIdentificacion = noIdentificacion;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public Integer getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(Integer tipoPago) {
        this.tipoPago = tipoPago;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    
}