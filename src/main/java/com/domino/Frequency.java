package com.domino;

/**
 * Esta clase modela la frecuencia de los paises.
 * 
 * @author A. Eliud Uriostegui Gonzalez
 * @version 1.0.0
 */
public class Frequency implements Comparable<Frequency>{
    
    private int idPais;
    private int frequency;
    
    public Frequency(){
    }
    
    public Frequency (int idPais){
        this.idPais = idPais;
    }
    public Frequency(int idPais,int frequency){
    
        this.idPais = idPais;
        this.frequency = frequency;
    }
    
    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int Frequency) {
        this.frequency = Frequency;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }
    
    @Override
    public int compareTo(Frequency o){
        if (frequency > o.frequency){
            return -1;
        }if (frequency < o.frequency){
            return 1;
        }
        return 0;
    }
}