package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author A. Eliud Uriostegui Glez.
 */

@Entity
@Table (name="pago_equipo")
public class PagoEquipo implements Serializable {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name="idinscripcion")
    private Integer idIncripcion;
    @Column (name="idformacionequipo")
    private Integer idFormacionEquipo;
    @Column (name="idTorneo")
    private Integer idTorneo;
    @Column (name="pais")
    private Integer pais;
    @Column (name="metropoli")
    private Integer metropoli;
    @Column (name="club")
    private String club;
    @Column (name="activo")
    private Integer activo;
    @Column (name="tipo_pago")
    private Integer tipoPago;
    @Column (name="identificacion")
    private String identificacion;
    @Column (name = "noidentificacion")
    private String noIdentifiacion;
    @Column (name="comentarios")
    private String comentarios;

    public PagoEquipo() {
    }

    public PagoEquipo(Integer idIncripcion, Integer idFormacionEquipo, 
            Integer idTorneo, Integer pais, Integer metropoli, String club, 
            Integer activo, Integer tipoPago, String identificacion, 
            String noIdentifiacion, String comentarios) {
        this.idIncripcion = idIncripcion;
        this.idFormacionEquipo = idFormacionEquipo;
        this.idTorneo = idTorneo;
        this.pais = pais;
        this.metropoli = metropoli;
        this.club = club;
        this.activo = activo;
        this.tipoPago = tipoPago;
        this.identificacion = identificacion;
        this.noIdentifiacion = noIdentifiacion;
        this.comentarios = comentarios;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Integer getIdFormacionEquipo() {
        return idFormacionEquipo;
    }

    public void setIdFormacionEquipo(Integer idFormacionEquipo) {
        this.idFormacionEquipo = idFormacionEquipo;
    }

    public Integer getIdIncripcion() {
        return idIncripcion;
    }

    public void setIdIncripcion(Integer idIncripcion) {
        this.idIncripcion = idIncripcion;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public String getNoIdentifiacion() {
        return noIdentifiacion;
    }

    public void setNoIdentifiacion(String noIdentifiacion) {
        this.noIdentifiacion = noIdentifiacion;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public Integer getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(Integer tipoPago) {
        this.tipoPago = tipoPago;
    }

 
}