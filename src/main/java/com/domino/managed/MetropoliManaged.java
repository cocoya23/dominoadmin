package com.domino.managed;

import com.domino.bens.Metropoli;
import com.domino.bens.Pais;
import com.domino.service.MetropoliService;
import com.domino.service.PaisService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

@ManagedBean
@ViewScoped
public class MetropoliManaged implements Serializable {

    @ManagedProperty("#{paisService}")
    private PaisService paisService;
    @ManagedProperty("#{metropoliService}")
    private MetropoliService metropoliService;
    private List<SelectItem> metropolis;
    private List<SelectItem> paises;
    private List<Metropoli> met;
    private Integer paisID;
    private String metropoliname;
    private Integer varpais;
    private String nombmet;
    private Integer idMetropoli;
    private String nommetropoli;
    Metropoli metropoli;

    @PostConstruct
    public void init() {
        paises = new ArrayList(0);
        try {
            for (Pais pais : paisService.getPaises()) {
                paises.add(new SelectItem(pais.getId(), pais.getNombre()));
            }

            met = metropoliService.getMetropoliMet(1);
        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void guardaMetropoli() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            System.out.println(metropoliname);
            System.out.println(paisID);
            metropoli = new Metropoli(null, metropoliname, paisID);
            metropoliService.insertMetropoli(metropoli);
            metropoliname = "";
            met = metropoliService.getMetropoliMet(paisID);
            context.addMessage(null, new FacesMessage("Exito", "Se registro nueva metropoli"));
        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "No se pudo guardar el pais"));
        }

    }

    public void modificaMetropoli() {
        try {
            System.out.println("Entro a modifica pais");
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Map params = facesContext.getExternalContext().getRequestParameterMap();
            idMetropoli = new Integer((String) params.get("idMetropoli"));
            nommetropoli = (String) params.get("nommetropoli");
            System.out.println(idMetropoli);
            System.out.println(nommetropoli);
        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void updatePais() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            metropoli = new Metropoli(idMetropoli, nombmet, paisID);
            metropoliService.updateMetropoli(metropoli);
            met = metropoliService.getMetropoliMet(paisID);

            context.addMessage(null, new FacesMessage("Exito", "Se actualizo metrópoli"));
        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void deleteMetropoli() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Map params = facesContext.getExternalContext().getRequestParameterMap();
            idMetropoli = new Integer((String) params.get("idMetropoli"));
            Integer rows = metropoliService.getRowsJugador(idMetropoli);
            System.out.println(rows);
            if (rows == 0) {
                metropoliService.deleteMetropoli(idMetropoli);
                met = metropoliService.getMetropoliMet(paisID);
                context.addMessage(null, new FacesMessage("Exito", "Se elimino metrópoli"));
            } else {
                met = metropoliService.getMetropoliMet(paisID);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "No se pude eliminar ya que existen jugadores con esta metrópoli"));
            }

        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void cargaMet() {
        try {
            met = metropoliService.getMetropoliMet(paisID);
        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public MetropoliService getMetropoliService() {
        return metropoliService;
    }

    public void setMetropoliService(MetropoliService metropoliService) {
        this.metropoliService = metropoliService;
    }

    public List<SelectItem> getMetropolis() {
        return metropolis;
    }

    public void setMetropolis(List<SelectItem> metropolis) {
        this.metropolis = metropolis;
    }

    public Integer getPaisID() {
        return paisID;
    }

    public void setPaisID(Integer paisID) {
        this.paisID = paisID;
    }

    public PaisService getPaisService() {
        return paisService;
    }

    public void setPaisService(PaisService paisService) {
        this.paisService = paisService;
    }

    public List<SelectItem> getPaises() {
        return paises;
    }

    public void setPaises(List<SelectItem> paises) {
        this.paises = paises;
    }

    public Metropoli getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Metropoli metropoli) {
        this.metropoli = metropoli;
    }

    public String getMetropoliname() {
        return metropoliname;
    }

    public void setMetropoliname(String metropoliname) {
        this.metropoliname = metropoliname;
    }

    public List<Metropoli> getMet() {
        return met;
    }

    public void setMet(List<Metropoli> met) {
        this.met = met;
    }

    public Integer getVarpais() {
        return varpais;
    }

    public void setVarpais(Integer varpais) {
        this.varpais = varpais;
    }

    public Integer getIdMetropoli() {
        return idMetropoli;
    }

    public void setIdMetropoli(Integer idMetropoli) {
        this.idMetropoli = idMetropoli;
    }

    public String getNombmet() {
        return nombmet;
    }

    public void setNombmet(String nombmet) {
        this.nombmet = nombmet;
    }

    public String getNommetropoli() {
        return nommetropoli;
    }

    public void setNommetropoli(String nommetropoli) {
        this.nommetropoli = nommetropoli;
    }
}
