package com.domino;


public class MesaPlayer {

    private Integer mesa;
    private String Jugador1;
    private String Jugador2;
    private String Jugador3;
    private String Jugador4;

    public MesaPlayer(Integer mesa, String Jugador1, String Jugador2, 
            String Jugador3, String Jugador4) {
        this.mesa = mesa;
        this.Jugador1 = Jugador1;
        this.Jugador2 = Jugador2;
        this.Jugador3 = Jugador3;
        this.Jugador4 = Jugador4;
    }

    public MesaPlayer() {
    }

    public String getJugador1() {
        return Jugador1;
    }

    public void setJugador1(String Jugador1) {
        this.Jugador1 = Jugador1;
    }

    public String getJugador2() {
        return Jugador2;
    }

    public void setJugador2(String Jugador2) {
        this.Jugador2 = Jugador2;
    }

    public String getJugador3() {
        return Jugador3;
    }

    public void setJugador3(String Jugador3) {
        this.Jugador3 = Jugador3;
    }

    public String getJugador4() {
        return Jugador4;
    }

    public void setJugador4(String Jugador4) {
        this.Jugador4 = Jugador4;
    }

    public Integer getMesa() {
        return mesa;
    }

    public void setMesa(Integer mesa) {
        this.mesa = mesa;
    }

}
