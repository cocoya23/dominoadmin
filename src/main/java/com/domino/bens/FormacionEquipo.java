package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "formacionequipo")
public class FormacionEquipo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idformacionequipo")
    private Integer idformacionequipo;
    
    @Column(name = "idtorneo")
    private Integer idtorneo;
    
    @Column(name = "idjugador1")
    private Integer idjugador1;
    
    @Column(name = "idjugador2")
    private Integer idjugador2;
    
    @Column(name = "idjugador3")
    private Integer idjugador3;
    
    @Column(name = "idjugador4")
    private Integer idjugador4;


    public FormacionEquipo() {
    }

    public FormacionEquipo(Integer idformacionequipo, Integer idtorneo, Integer idjugador1, Integer idjugador2, Integer idjugador3, Integer idjugador4) {
        this.idformacionequipo = idformacionequipo;
        this.idtorneo = idtorneo;
        this.idjugador1 = idjugador1;
        this.idjugador2 = idjugador2;
        this.idjugador3 = idjugador3;
        this.idjugador4 = idjugador4;
    }

    public Integer getIdformacionequipo() {
        return idformacionequipo;
    }

    public void setIdformacionequipo(Integer idformacionequipo) {
        this.idformacionequipo = idformacionequipo;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public Integer getIdjugador1() {
        return idjugador1;
    }

    public void setIdjugador1(Integer idjugador1) {
        this.idjugador1 = idjugador1;
    }

    public Integer getIdjugador2() {
        return idjugador2;
    }

    public void setIdjugador2(Integer idjugador2) {
        this.idjugador2 = idjugador2;
    }

    public Integer getIdjugador3() {
        return idjugador3;
    }

    public void setIdjugador3(Integer idjugador3) {
        this.idjugador3 = idjugador3;
    }

    public Integer getIdjugador4() {
        return idjugador4;
    }

    public void setIdjugador4(Integer idjugador4) {
        this.idjugador4 = idjugador4;
    }
}
