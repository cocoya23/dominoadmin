package com.domino.service;

import com.domino.MesaPlayer;
import com.domino.MesaPlayer2;
import com.domino.bens.Jugador;
import com.domino.bens.PagoIndividualControl;
import com.domino.dao.MesaDAO;
import com.domino.test.CodigoBarras;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "mesaService", eager = true)
@ApplicationScoped
public class MesaService {

    private static List<MesaPlayer> mesaPlayers =
            new ArrayList<MesaPlayer>();
    private static List<MesaPlayer2> mesaPlayers2 =
            new ArrayList<MesaPlayer2>();
    private static MesaPlayer ms = new MesaPlayer();
    private static MesaPlayer2 ms2 = new MesaPlayer2();
    private static List<PagoIndividualControl> individualControls;
    private static PagoIndividualControl control;
    private static MesaDAO mesaDAO = new MesaDAO();
    private static Jugador jugador = new Jugador();
    CodigoBarras barras=new CodigoBarras();
    Integer mesa;
    String jugador1, jugador2, jugador3, jugador4;
    String jugador5, jugador6, jugador7, jugador8;

    public void addListaMesaServices(int mesa, String jugador1, String jugador2,
            String jugador3, String jugador4) {

        this.mesa = mesa;
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.jugador3 = jugador3;
        this.jugador4 = jugador4;

        ms = new MesaPlayer(mesa, jugador1, jugador2, jugador3, jugador4);
        mesaPlayers.add(ms);
    }

    public void addListaMesaServices2(int mesa, String jugador1, String jugador2,
            String jugador3, String jugador4) {

        this.mesa = mesa;
        this.jugador5 = jugador1;
        this.jugador6 = jugador2;
        this.jugador7 = jugador3;
        this.jugador8 = jugador4;

        ms2 = new MesaPlayer2(mesa, jugador1, jugador2, jugador3, jugador4);
        mesaPlayers2.add(ms2);
    }

    /**
     *
     */
    public List<MesaPlayer> getMesas() throws Exception {
        int count = 0;
        Integer table = 1;
        String player1 = null;
        String player2 = null;
        String player3 = null;
        String player4 = null;
        String player5 = null;
        String player6 = null;
        String player7 = null;
        String player8 = null;
        individualControls = mesaDAO.getList();
        Integer rows = individualControls.size();
        Integer mod = rows % 4;
        String playerNull = "Empty";
        for (PagoIndividualControl pic : individualControls) {
            if (count == 0) {
                player1 = getJugador(pic.getIdjugador(),
                        pic.getIdinscripcion());
                player5 = getJugador2(pic.getIdjugador(),
                        pic.getIdinscripcion());
                count++;
                rows--;
            } else if (count == 1) {
                player2 = getJugador(pic.getIdjugador(),
                        pic.getIdinscripcion());
                player6 = getJugador2(pic.getIdjugador(),
                        pic.getIdinscripcion());
                count++;
                rows--;
            } else if (count == 2) {
                player3 = getJugador(pic.getIdjugador(),
                        pic.getIdinscripcion());
                player7 = getJugador2(pic.getIdjugador(),
                        pic.getIdinscripcion());
                count++;
                rows--;
            } else {
                player4 = getJugador(pic.getIdjugador(),
                        pic.getIdinscripcion());
                player8 = getJugador2(pic.getIdjugador(),
                        pic.getIdinscripcion());
                addListaMesaServices(table, player1, player2,
                        player3, player4);
                addListaMesaServices2(table, player5, player6,
                        player7, player8);
                count = 0;
                table++;
                rows--;
            }
            if (rows == 0) {
                if (mod == 1) {
                    addListaMesaServices(table, player1, playerNull,
                            playerNull, playerNull);
                    addListaMesaServices2(table, player5, playerNull,
                            playerNull, playerNull);
                } else if (mod == 2) {
                    addListaMesaServices(table, player1, player2,
                            playerNull, playerNull);
                    addListaMesaServices2(table, player5, player6,
                            playerNull, playerNull);
                } else if (mod == 3) {
                    addListaMesaServices(table, player1, player2, player3,
                            playerNull);
                    addListaMesaServices2(table, player5, player6, player7,
                            playerNull);
                }
            }
        }
        System.out.println("Tamaño:" + individualControls.size());
        for (MesaPlayer player : mesaPlayers) {
            System.out.println("Mesa" + player.getMesa() + "_Jugador1: "
                    + player.getJugador1() + "_Jugador2: " + player.getJugador2()
                    + "_Jugador3: " + player.getJugador3() + "_Jugador4: "
                    + player.getJugador4());
            
        }
        
        for (MesaPlayer2 player : mesaPlayers2) {
            barras.generaCodigo(player.getMesa(),player.getJugador1(), player.getJugador2(), player.getJugador3(), player.getJugador4());
            
        }
        return getMesaPlayers();
    }

    public String getJugador(Integer idJugador, Integer idInscripcion) throws Exception {
        String name = null;
        jugador = mesaDAO.getJugador(idJugador);
        if (jugador == null) {
            name = String.valueOf(idInscripcion) + "-null";
        } else {
            Integer idpais = Integer.parseInt(jugador.getPais());
            Integer idmet = Integer.parseInt(jugador.getMetropoli());
            String pais = mesaDAO.namePais(idpais);
            String metropoli = mesaDAO.nameMetropoli(idmet);
            name = String.valueOf(idInscripcion) + "-" + jugador.getNombre() + "-" + pais + "-" + metropoli + "-" + jugador.getClub();
        }
        return name;
    }

    public String getJugador2(Integer idJugador, Integer idInscripcion) throws Exception {
        String name = null;
        jugador = mesaDAO.getJugador(idJugador);
        if (jugador == null) {
            name = String.valueOf(idInscripcion) + "-null";
        } else {
            Integer idpais = Integer.parseInt(jugador.getPais());
            Integer idmet = Integer.parseInt(jugador.getMetropoli());
            String pais = mesaDAO.namePais(idpais);
            String metropoli = mesaDAO.nameMetropoli(idmet);
            name = String.valueOf(idInscripcion) + "-" + jugador.getNombre();
        }
        return name;
    }

    public static PagoIndividualControl getControl() {
        return control;
    }

    public static void setControl(PagoIndividualControl control) {
        MesaService.control = control;
    }

    public static List<PagoIndividualControl> getIndividualControls() {
        return individualControls;
    }

    public static void setIndividualControls(List<PagoIndividualControl> individualControls) {
        MesaService.individualControls = individualControls;
    }

    public static Jugador getJugador() {
        return jugador;
    }

    public static void setJugador(Jugador jugador) {
        MesaService.jugador = jugador;
    }

    public String getJugador1() {
        return jugador1;
    }

    public void setJugador1(String jugador1) {
        this.jugador1 = jugador1;
    }

    public String getJugador2() {
        return jugador2;
    }

    public void setJugador2(String jugador2) {
        this.jugador2 = jugador2;
    }

    public String getJugador3() {
        return jugador3;
    }

    public void setJugador3(String jugador3) {
        this.jugador3 = jugador3;
    }

    public String getJugador4() {
        return jugador4;
    }

    public void setJugador4(String jugador4) {
        this.jugador4 = jugador4;
    }

    public Integer getMesa() {
        return mesa;
    }

    public void setMesa(Integer mesa) {
        this.mesa = mesa;
    }

    public static MesaDAO getMesaDAO() {
        return mesaDAO;
    }

    public static void setMesaDAO(MesaDAO mesaDAO) {
        MesaService.mesaDAO = mesaDAO;
    }

    public static List<MesaPlayer> getMesaPlayers() {
        return mesaPlayers;
    }

    public static void setMesaPlayers(List<MesaPlayer> mesaPlayers) {
        MesaService.mesaPlayers = mesaPlayers;
    }

    public static MesaPlayer getMs() {
        return ms;
    }

    public static void setMs(MesaPlayer ms) {
        MesaService.ms = ms;
    }
}