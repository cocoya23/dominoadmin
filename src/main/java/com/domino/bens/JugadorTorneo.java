package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "jugador_x_torneo")
public class JugadorTorneo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idjugador_x_torneo")
    private Integer idjugador_x_torneo;
    @Column(name = "idTorneo")
    private Integer idTorneo;
    @Column(name = "idjugador")
    private Integer idjugador;
    @Column(name = "individual")
    private Integer individual;
    @Column(name = "pareja")
    private Integer pareja;
    @Column(name = "equipo")
    private Integer equipo;
    @ManyToOne(optional = false)
    @JoinColumn(name = "idjugador", referencedColumnName = "idjugador", insertable = false, updatable = false)
    private Jugador jugador;

    public JugadorTorneo() {
    }

    public JugadorTorneo(Integer idjugador_x_torneo, Integer idTorneo, Integer idjugador, Integer individual, Integer pareja, Integer equipo) {
        this.idjugador_x_torneo = idjugador_x_torneo;
        this.idTorneo = idTorneo;
        this.idjugador = idjugador;
        this.individual = individual;
        this.pareja = pareja;
        this.equipo = equipo;
    }

    public Integer getIdjugador_x_torneo() {
        return idjugador_x_torneo;
    }

    public void setIdjugador_x_torneo(Integer idjugador_x_torneo) {
        this.idjugador_x_torneo = idjugador_x_torneo;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public Integer getEquipo() {
        return equipo;
    }

    public void setEquipo(Integer equipo) {
        this.equipo = equipo;
    }

    public Integer getIndividual() {
        return individual;
    }

    public void setIndividual(Integer individual) {
        this.individual = individual;
    }

    public Integer getPareja() {
        return pareja;
    }

    public void setPareja(Integer pareja) {
        this.pareja = pareja;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }
}
