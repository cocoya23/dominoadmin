package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author A. Eliud Uriostegui Gonzalez
 * @version 1.0.0
 */

@Entity
@Table(name="pago_control")
public class PagoControl implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name="idpago")
    private Integer idPago;
    @Column (name ="idjugador")
    private Integer idJugador;
    @Column (name="idtorneo")
    private Integer idTorneo;
    @Column (name="idcontrol")
    private Integer idControl;
    
    public PagoControl(){
        
    }
    
    public PagoControl(Integer idPago, Integer idJugador, Integer idTorneo, 
            Integer idControl){
        
        this.idPago = idPago;
        this.idJugador = idJugador;
        this.idTorneo =idTorneo;
        this.idControl = idControl;
    }

    public Integer getIdControl() {
        return idControl;
    }

    public void setIdControl(Integer idControl) {
        this.idControl = idControl;
    }

    public Integer getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(Integer idJugador) {
        this.idJugador = idJugador;
    }

    public Integer getIdPago() {
        return idPago;
    }

    public void setIdPago(Integer idPago) {
        this.idPago = idPago;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }
    
    
}
