package com.domino.util;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil implements Serializable{

    private static SessionFactory sessionFactory;

    public static Session getSession() throws Exception {
        if (sessionFactory == null) {
            setUp();
            return sessionFactory.openSession();
        } else {
            return sessionFactory.openSession();
        }
    }

    private static void setUp() throws Exception {
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("/config/hibernate.cfg.xml") // configures settings from hibernate.cfg.xml
                .build();
        try {
             sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
		// The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            Logger.getLogger(HibernateUtil.class.getName()).log(Level.SEVERE, null, e);
            
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
