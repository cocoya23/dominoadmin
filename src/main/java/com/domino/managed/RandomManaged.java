package com.domino.managed;

import com.domino.MesaPlayer;
import com.domino.bens.PagoIndividualControl;
import com.domino.service.MesaService;
import com.domino.service.RandomService;
import com.domino.service.RankingService;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class RandomManaged implements Serializable {

    @ManagedProperty("#{randomService}")
    private RandomService randomService;
    @ManagedProperty("#{RankingService}")
    private RankingService rankingService;
    @ManagedProperty("#{mesaService}")
    private MesaService ms;
    private Integer idTorneo = 3;
    private List<MesaPlayer> players;
    private Integer variable = 1;
    private Integer variable2 = 0;
    private Integer id1;
    private Integer id2;

    @PostConstruct
    public void init() {

        try {
          
            players = ms.getMesas();
            int num = players.size();
            if (num == 0) {
                variable2 = 0;
            } else {
                variable2 = 1;
            }


        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    /**
     * public void generaRandom() { try {
     * this.randomService.getRandom(idTorneo);
     *
     * } catch (Exception ex) {
     * Logger.getLogger(RandomManaged.class.getName()).log(Level.SEVERE, null,
     * ex); } }
     *
     * /**
     * Llama al metodo setLista nueva.
     *
     * @param idTorneo de los participantes del torneo
     */
    public void listaRandom() {
        try {
            this.rankingService.setListaNueva(idTorneo);
            players = ms.getMesas();
            int num = players.size();
            if (num == 0) {
                variable2 = 0;
            } else {
                variable2 = 1;
            }
        } catch (Exception ex) {
            Logger.getLogger(RandomManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modificaRandom() throws Exception {
        System.out.println(id1);
        System.out.println(id2);
        PagoIndividualControl pag1;
        PagoIndividualControl pag2;
        pag1 = this.randomService.getPagoControlInd(id1);
        pag2 = this.randomService.getPagoControlInd(id2);
        PagoIndividualControl pag3 = new PagoIndividualControl(pag1.getId(), pag2.getIdinscripcion(), pag2.getIdjugador(), pag2.getIdtorneo(), pag2.getPais(), pag2.getMetropoli(), pag2.getClub());
        PagoIndividualControl pag4 = new PagoIndividualControl(pag2.getId(), pag1.getIdinscripcion(), pag1.getIdjugador(), pag1.getIdtorneo(), pag1.getPais(), pag1.getMetropoli(), pag1.getClub());
        this.randomService.insertPagoIndCont(pag3);
        this.randomService.insertPagoIndCont(pag4);
        players.clear();
        players = ms.getMesas();
        int num = players.size();
        if (num == 0) {
            variable2 = 0;
        } else {
            variable2 = 1;
        }
        id1=null;
        id2=null;
    }

    
     public void generaPapeletas() {
        try {
           
            players = ms.getMesas();
            int num = players.size();
            if (num == 0) {
                variable2 = 0;
            } else {
                variable2 = 1;
            }
        } catch (Exception ex) {
            Logger.getLogger(RandomManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
            
            
    public RandomService getRandomService() {
        return randomService;
    }

    public void setRandomService(RandomService randomService) {
        this.randomService = randomService;
    }

    public RankingService getRankingService() {
        return rankingService;
    }

    public void setRankingService(RankingService rankingService) {
        this.rankingService = rankingService;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public MesaService getMs() {
        return ms;
    }

    public void setMs(MesaService ms) {
        this.ms = ms;
    }

    public List<MesaPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<MesaPlayer> players) {
        this.players = players;
    }

    public Integer getVariable() {
        return variable;
    }

    public void setVariable(Integer variable) {
        this.variable = variable;
    }

    public Integer getVariable2() {
        return variable2;
    }

    public void setVariable2(Integer variable2) {
        this.variable2 = variable2;
    }

    public Integer getId1() {
        return id1;
    }

    public void setId1(Integer id1) {
        this.id1 = id1;
    }

    public Integer getId2() {
        return id2;
    }

    public void setId2(Integer id2) {
        this.id2 = id2;
    }
}