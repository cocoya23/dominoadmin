package com.domino.test;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.FileOutputStream;

public class CodigoBarras {

    public static final Font Nombre = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
    public static final Font titulo = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
    public static final Font tarjetas = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD, BaseColor.BLACK);

    public  void generaCodigo(Integer mesa,String Nombre1, String Nombre2, String Nombre3, String Nombre4) {
        Document documento = new Document();
        Paragraph parrafo = new Paragraph();

        try {
            // Obtenemos una instancia de un objeto PDFWriter
            PdfWriter pdfw = PdfWriter.getInstance(documento, new FileOutputStream("C:/Torneos/Mesa"+mesa+".pdf"));
            //Abro el documento
            documento.open();

            PdfPTable table = new PdfPTable(25);
            PdfPCell celdaInicial = new PdfPCell(new Paragraph(Nombre1, Nombre));
            PdfPCell celdaInicial2 = new PdfPCell(new Paragraph(Nombre2, Nombre));
            PdfPCell celdaInicial3 = new PdfPCell(new Paragraph(Nombre3, Nombre));
            PdfPCell celdaInicial4 = new PdfPCell(new Paragraph(Nombre4, Nombre));
            PdfPCell celdavs = new PdfPCell(new Paragraph("VS", Nombre));
            celdaInicial.setColspan(6);
            celdaInicial2.setColspan(6);
            celdaInicial3.setColspan(6);
            celdaInicial4.setColspan(6);
            celdavs.setColspan(1);
            table.addCell(celdaInicial);
            table.addCell(celdaInicial2);
            table.addCell(celdavs);
            table.addCell(celdaInicial3);
            table.addCell(celdaInicial4);
            table.addCell(new Paragraph("L", titulo));
            table.addCell(new Paragraph("P", titulo));
            table.addCell(new Paragraph("F", titulo));
            table.addCell(new Paragraph("C", titulo));
            table.addCell(new Paragraph("S", titulo));
            table.addCell(new Paragraph("Et", titulo));
            table.addCell(new Paragraph("L", titulo));
            table.addCell(new Paragraph("P", titulo));
            table.addCell(new Paragraph("F", titulo));
            table.addCell(new Paragraph("C", titulo));
            table.addCell(new Paragraph("D", titulo));
            table.addCell(new Paragraph("Et", titulo));
            table.addCell(new Paragraph("|||", titulo));
            table.addCell(new Paragraph("L", titulo));
            table.addCell(new Paragraph("P", titulo));
            table.addCell(new Paragraph("F", titulo));
            table.addCell(new Paragraph("C", titulo));
            table.addCell(new Paragraph("S", titulo));
            table.addCell(new Paragraph("Et", titulo));
            table.addCell(new Paragraph("L", titulo));
            table.addCell(new Paragraph("P", titulo));
            table.addCell(new Paragraph("F", titulo));
            table.addCell(new Paragraph("C", titulo));
            table.addCell(new Paragraph("D", titulo));
            table.addCell(new Paragraph("Et", titulo));
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(new Paragraph("|||", titulo));
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            PdfPCell celdafinal = new PdfPCell(new Paragraph("T Amarillas", tarjetas));
            PdfPCell celdafinal2 = new PdfPCell(new Paragraph("T Azules", tarjetas));
            PdfPCell celdafinal3 = new PdfPCell(new Paragraph("T Rojas", tarjetas));
            celdafinal.setColspan(2);
            celdafinal2.setColspan(2);
            celdafinal3.setColspan(2);
            table.addCell(celdafinal);
            table.addCell(celdafinal2);
            table.addCell(celdafinal3);
            PdfPCell celdafinal4 = new PdfPCell(new Paragraph("T Amarillas", tarjetas));
            PdfPCell celdafinal5 = new PdfPCell(new Paragraph("T Azules", tarjetas));
            PdfPCell celdafinal6 = new PdfPCell(new Paragraph("T Rojas", tarjetas));
            celdafinal4.setColspan(2);
            celdafinal5.setColspan(2);
            celdafinal6.setColspan(2);
            table.addCell(celdafinal4);
            table.addCell(celdafinal5);
            table.addCell(celdafinal6);
            table.addCell(new Paragraph("|||", titulo));
            PdfPCell celdafinal7 = new PdfPCell(new Paragraph("T Amarillas", tarjetas));
            PdfPCell celdafinal8 = new PdfPCell(new Paragraph("T Azules", tarjetas));
            PdfPCell celdafinal9 = new PdfPCell(new Paragraph("T Rojas", tarjetas));
            celdafinal7.setColspan(2);
            celdafinal8.setColspan(2);
            celdafinal9.setColspan(2);
            table.addCell(celdafinal7);
            table.addCell(celdafinal8);
            table.addCell(celdafinal9);
            PdfPCell celdafinal10 = new PdfPCell(new Paragraph("T Amarillas", tarjetas));
            PdfPCell celdafinal11 = new PdfPCell(new Paragraph("T Azules", tarjetas));
            PdfPCell celdafinal12 = new PdfPCell(new Paragraph("T Rojas", tarjetas));
            celdafinal10.setColspan(2);
            celdafinal11.setColspan(2);
            celdafinal12.setColspan(2);
            table.addCell(celdafinal10);
            table.addCell(celdafinal11);
            table.addCell(celdafinal12);
            PdfPCell celdafinal13 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal14 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal15 = new PdfPCell(new Paragraph(" "));
            celdafinal13.setColspan(2);
            celdafinal14.setColspan(2);
            celdafinal15.setColspan(2);
            table.addCell(celdafinal13);
            table.addCell(celdafinal14);
            table.addCell(celdafinal15);
            PdfPCell celdafinal16 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal17 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal18 = new PdfPCell(new Paragraph(" "));
            celdafinal16.setColspan(2);
            celdafinal17.setColspan(2);
            celdafinal18.setColspan(2);
            table.addCell(celdafinal16);
            table.addCell(celdafinal17);
            table.addCell(celdafinal18);
            PdfPCell celdafinal19 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal20 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal21 = new PdfPCell(new Paragraph(" "));
            celdafinal19.setColspan(2);
            celdafinal20.setColspan(2);
            celdafinal21.setColspan(2);
            table.addCell(celdafinal19);
            table.addCell(celdafinal20);
            table.addCell(celdafinal21);
            PdfPCell celdafinal22 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal23 = new PdfPCell(new Paragraph(" "));
            PdfPCell celdafinal24 = new PdfPCell(new Paragraph(" "));
            celdafinal22.setColspan(2);
            celdafinal23.setColspan(2);
            celdafinal24.setColspan(2);
            table.addCell(celdafinal22);
            table.addCell(celdafinal23);
            table.addCell(celdafinal24);
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(new Paragraph("|||", titulo));
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");
            table.addCell(" ");


            Image img;
            BarcodeEAN codeEAN = new BarcodeEAN();
            codeEAN.setCodeType(Barcode.EAN13);
            codeEAN.setCode("978193518261"+mesa);
            img = codeEAN.createImageWithBarcode(pdfw.getDirectContent(), BaseColor.BLACK, BaseColor.BLACK);
            img.setAlignment(Element.ALIGN_CENTER);

            Paragraph parrafo2 = new Paragraph();
            parrafo.add(new Phrase("Plantilla de Anotación \n Tlaquepaque \n Individual"));
            parrafo.setAlignment(Element.ALIGN_CENTER);
            parrafo2.add(new Phrase("Mesa: "+mesa+"                                                                                                                               Partida: 1"));

            Paragraph parrafo3 = new Paragraph();
            parrafo3.add(new Phrase(" "));
            Paragraph parrafo4 = new Paragraph();
            parrafo4.setAlignment(Element.ALIGN_CENTER);
            parrafo4.add(new Phrase("Sanciones/Tarjetas/Artículos\n", tarjetas));

            Paragraph parrafo5 = new Paragraph();
            parrafo5.setAlignment(Element.ALIGN_CENTER);
            parrafo5.add(new Phrase(" \n___________________________            _____________________________         firmas           _____________________________            ___________________________ ", tarjetas));

            Paragraph parrafo6 = new Paragraph();
            parrafo6.setAlignment(Element.ALIGN_CENTER);
            parrafo6.add(new Phrase("L Lugar a ocupar en la mesa C Tantos en contra acumulados P Puntos acumulados hasta la partida anterior S Sanciones acumuladas F Tantos a favor acumulados Et Eficiencia total ", tarjetas));

            PdfPTable table2 = new PdfPTable(2);
            table2.addCell("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            table2.addCell("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            table2.addCell("Total:");
            table2.addCell("Total:");
            float[] medidaCeldas = {1.5f, 1.5f};
            table2.setWidths(medidaCeldas);


            PdfPTable table3 = new PdfPTable(13);
            PdfPCell celdaIn = new PdfPCell(new Paragraph(Nombre1, Nombre));
            PdfPCell celdaIn2 = new PdfPCell(new Paragraph(Nombre2, Nombre));
            PdfPCell celdaIn3 = new PdfPCell(new Paragraph(Nombre3, Nombre));
            PdfPCell celdaIn4 = new PdfPCell(new Paragraph(Nombre4, Nombre));
            PdfPCell celdaIn5 = new PdfPCell(new Paragraph("VS", Nombre));
            celdaIn.setColspan(3);
            celdaIn2.setColspan(3);
            celdaIn5.setColspan(1);
            celdaIn3.setColspan(3);
            celdaIn4.setColspan(3);
            table3.addCell(celdaIn);
            table3.addCell(celdaIn2);
            table3.addCell(celdaIn5);
            table3.addCell(celdaIn3);
            table3.addCell(celdaIn4);
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(new Paragraph("| | |", titulo));
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(new Paragraph("| | |", titulo));
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");
            table3.addCell(" ");







            documento.add(parrafo);
            documento.add(parrafo2);
            documento.add(img);
            documento.add(table);
            documento.add(parrafo3);
            documento.add(table2);
            documento.add(parrafo4);
            documento.add(table3);
            documento.add(parrafo5);
            documento.add(parrafo6);

            documento.close();
        } catch (DocumentException ex) {
            // Atrapamos excepciones concernientes al documentoo.
        } catch (java.io.IOException ex) {
            // Atrapamos excepciones concernientes al I/O.
        }
    }
}
