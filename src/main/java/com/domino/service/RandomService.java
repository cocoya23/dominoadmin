package com.domino.service;

import com.domino.bens.PagoIndividual;
import com.domino.bens.PagoIndividualControl;
import com.domino.bens.Torneo;
import com.domino.dao.RandomDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.model.SelectItem;
import org.hibernate.Session;

@ManagedBean(name = "randomService", eager = true)
@ApplicationScoped
public class RandomService implements Serializable {

    @ManagedProperty("#{randomServiceClub}")
    private RandomServiceClub randomServiceclub;
    @ManagedProperty("#{randomServiceMetropoli}")
    private RandomServiceMetropoli randomServicemetropoli;
    RandomDAO getRandom = new RandomDAO();
    PagoIndividual pagoIndividual;
    PagoIndividualControl pagoIndividualControl;
    private List<PagoIndividual> pagoindlist;
    private boolean condicion = false;
    private boolean condicion1 = false;
    private boolean condicion2 = false;
    private boolean condicion3 = false;
    private boolean condicion4 = false;
    private Integer idinscripcion;
    private Integer idjugador;
    private Integer idtorneo;
    private Integer pais;
    private Integer metropoli;
    private String club;
    private Integer idinscripcion2;
    private Integer idjugador2;
    private Integer idtorneo2;
    private Integer pais2;
    private Integer metropoli2;
    private String club2;
    private Integer idinscripcion3;
    private Integer idjugador3;
    private Integer idtorneo3;
    private Integer pais3;
    private Integer metropoli3;
    private String club3;
    private Integer idinscripcion4;
    private Integer idjugador4;
    private Integer idtorneo4;
    private Integer pais4;
    private Integer metropoli4;
    private String club4;

    public List<Torneo> getTorneos() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Torneo> torneos = getRandom.getTorneos();
        return torneos;
    }

    public void getRandom(Integer torneo) throws Exception {
        int count = 0;
        int rows2 = 0;
        getRandom.setIdtorneo(torneo);
        int filas = getRandom.getRowsPagoIndividual();
        System.out.println("FILAS:" + filas);
        int n = 0;
        pagoindlist = new ArrayList(0);
        Session session = HibernateUtil.getSession();
        session.isConnected();
        while (condicion == false) {
            while (condicion1 == false) {
                if (n == 0) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion = pagoIndividual.getIdinscripcion();
                    idjugador = pagoIndividual.getIdjugador();
                    idtorneo = pagoIndividual.getIdtorneo();
                    pais = pagoIndividual.getPais();
                    metropoli = pagoIndividual.getMetropoli();
                    club = pagoIndividual.getClub();
                    if (club == null) {
                        club = "";
                    }
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion);
                    count += 1;
                    System.out.println("COUNT:" + count);
                    if (count == (filas * 5)) {
                        condicion1 = true;
                        condicion = true;
                    }
                    if (rows2 == 0) {
                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion, idjugador, idtorneo, pais, metropoli, club);
                        getRandom.savePagoIndC(pagoIndividualControl);
                        pagoindlist.add(pagoIndividual);
                        n = pagoindlist.size();
                        System.out.println("***********" + n + "**********");
                        count = 0;
                        condicion1 = true;
                    }


                }
            }
            if (n == 1) {
                while (condicion2 == false) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion2 = pagoIndividual.getIdinscripcion();
                    System.out.println("***********" + idinscripcion2);
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion2);
                    count += 1;
                    System.out.println("COUNT:" + count);
                    if (count == (filas * 5)) {

                        if (n == 1) {
                            while (condicion2 == false) {
                                pagoIndividual = getRandom.getRandom();
                                idinscripcion2 = pagoIndividual.getIdinscripcion();
                                System.out.println("***********" + idinscripcion2);
                                rows2 = this.getRandom.getPagoIndividualControl(idinscripcion2);
                                count += 1;
                                System.out.println("COUNT:" + count);
                                if (count == (filas * 5)) {
                                    condicion2 = true;
                                    condicion = true;
                                }
                                if (rows2 == 0) {
                                    idinscripcion2 = pagoIndividual.getIdinscripcion();
                                    idjugador2 = pagoIndividual.getIdjugador();
                                    idtorneo2 = pagoIndividual.getIdtorneo();
                                    pais2 = pagoIndividual.getPais();
                                    metropoli2 = pagoIndividual.getMetropoli();
                                    club2 = pagoIndividual.getClub();
                                    if (club2 == null) {
                                        club2 = "";
                                    }
                                    if (metropoli != metropoli2) {
                                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion2, idjugador2, idtorneo2, pais2, metropoli2, club2);
                                        getRandom.savePagoIndC(pagoIndividualControl);
                                        pagoindlist.add(pagoIndividual);
                                        condicion2 = true;
                                        count = 0;

                                    }

                                }


                            }
                        }

                        if (n == 2) {
                            while (condicion3 == false) {
                                pagoIndividual = getRandom.getRandom();
                                idinscripcion3 = pagoIndividual.getIdinscripcion();
                                System.out.println("***********" + idinscripcion3);
                                rows2 = this.getRandom.getPagoIndividualControl(idinscripcion3);
                                count += 1;
                                System.out.println("COUNT:" + count);
                                if (count == (filas * 5)) {
                                    condicion3 = true;
                                    condicion = true;
                                }
                                if (rows2 == 0) {
                                    idinscripcion3 = pagoIndividual.getIdinscripcion();
                                    idjugador3 = pagoIndividual.getIdjugador();
                                    idtorneo3 = pagoIndividual.getIdtorneo();
                                    pais3 = pagoIndividual.getPais();
                                    metropoli3 = pagoIndividual.getMetropoli();
                                    club3 = pagoIndividual.getClub();
                                    if (club3 == null) {
                                        club3 = "";
                                    }
                                    if ((metropoli != metropoli2 && metropoli2 != metropoli3 && metropoli != metropoli3)) {
                                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion3, idjugador3, idtorneo3, pais3, metropoli3, club3);
                                        getRandom.savePagoIndC(pagoIndividualControl);
                                        pagoindlist.add(pagoIndividual);
                                        condicion3 = true;
                                        count = 0;

                                    }


                                }


                            }
                        }

                        if (n == 3) {
                            while (condicion4 == false) {
                                count = 0;
                                pagoIndividual = getRandom.getRandom();
                                idinscripcion4 = pagoIndividual.getIdinscripcion();
                                System.out.println("***********" + idinscripcion4);
                                rows2 = this.getRandom.getPagoIndividualControl(idinscripcion4);
                                count += 1;
                                System.out.println("COUNT:" + count);
                                if (count == (filas * 5)) {
                                    condicion4 = true;
                                    condicion = true;
                                }
                                if (rows2 == 0) {
                                    idinscripcion4 = pagoIndividual.getIdinscripcion();
                                    idjugador4 = pagoIndividual.getIdjugador();
                                    idtorneo4 = pagoIndividual.getIdtorneo();
                                    pais4 = pagoIndividual.getPais();
                                    metropoli4 = pagoIndividual.getMetropoli();
                                    club4 = pagoIndividual.getClub();
                                    if (club4 == null) {
                                        club4 = "";
                                    }
                                    if ((metropoli != metropoli2 && metropoli2 != metropoli3 && metropoli2 != metropoli4 && metropoli3 != metropoli4 && metropoli != metropoli4 && metropoli != metropoli3)) {
                                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion4, idjugador4, idtorneo4, pais4, metropoli4, club4);
                                        getRandom.savePagoIndC(pagoIndividualControl);
                                        pagoindlist.add(pagoIndividual);
                                        condicion4 = true;
                                        count = 0;
                                        condicion4 = true;
                                        condicion = true;

                                    }

                                }
                            }
                        }

                    }
                    if (rows2 == 0) {
                        idinscripcion2 = pagoIndividual.getIdinscripcion();
                        idjugador2 = pagoIndividual.getIdjugador();
                        idtorneo2 = pagoIndividual.getIdtorneo();
                        pais2 = pagoIndividual.getPais();
                        metropoli2 = pagoIndividual.getMetropoli();
                        club2 = pagoIndividual.getClub();
                        if (club2 == null) {
                            club2 = "";
                        }
                        if (pais != pais2) {
                            pagoIndividualControl = new PagoIndividualControl(null, idinscripcion2, idjugador2, idtorneo2, pais2, metropoli2, club2);
                            getRandom.savePagoIndC(pagoIndividualControl);
                            pagoindlist.add(pagoIndividual);
                            condicion2 = true;
                            count = 0;

                        }

                    }


                }
            }

            n = pagoindlist.size();
            System.out.println("***********" + n + "**********");
            if (n == 2) {
                while (condicion3 == false) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion3 = pagoIndividual.getIdinscripcion();
                    System.out.println("***********" + idinscripcion3);
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion3);
                    count += 1;
                    System.out.println("COUNT:" + count);
                    if (count == (filas * 5)) {
                        if (n == 2) {
                            while (condicion3 == false) {
                                pagoIndividual = getRandom.getRandom();
                                idinscripcion3 = pagoIndividual.getIdinscripcion();
                                System.out.println("***********" + idinscripcion3);
                                rows2 = this.getRandom.getPagoIndividualControl(idinscripcion3);
                                count += 1;
                                System.out.println("COUNT:" + count);
                                if (count == (filas * 5)) {
                                    condicion3 = true;
                                    condicion = true;
                                }
                                if (rows2 == 0) {
                                    idinscripcion3 = pagoIndividual.getIdinscripcion();
                                    idjugador3 = pagoIndividual.getIdjugador();
                                    idtorneo3 = pagoIndividual.getIdtorneo();
                                    pais3 = pagoIndividual.getPais();
                                    metropoli3 = pagoIndividual.getMetropoli();
                                    club3 = pagoIndividual.getClub();
                                    if (club3 == null) {
                                        club3 = "";
                                    }
                                    if ((metropoli != metropoli2 && metropoli2 != metropoli3 && metropoli != metropoli3)) {
                                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion3, idjugador3, idtorneo3, pais3, metropoli3, club3);
                                        getRandom.savePagoIndC(pagoIndividualControl);
                                        pagoindlist.add(pagoIndividual);
                                        condicion3 = true;
                                        count = 0;

                                    }


                                }


                            }
                        }

                        if (n == 3) {
                            while (condicion4 == false) {
                                count = 0;
                                pagoIndividual = getRandom.getRandom();
                                idinscripcion4 = pagoIndividual.getIdinscripcion();
                                System.out.println("***********" + idinscripcion4);
                                rows2 = this.getRandom.getPagoIndividualControl(idinscripcion4);
                                count += 1;
                                System.out.println("COUNT:" + count);
                                if (count == (filas * 5)) {
                                    condicion4 = true;
                                    condicion = true;
                                }
                                if (rows2 == 0) {
                                    idinscripcion4 = pagoIndividual.getIdinscripcion();
                                    idjugador4 = pagoIndividual.getIdjugador();
                                    idtorneo4 = pagoIndividual.getIdtorneo();
                                    pais4 = pagoIndividual.getPais();
                                    metropoli4 = pagoIndividual.getMetropoli();
                                    club4 = pagoIndividual.getClub();
                                    if (club4 == null) {
                                        club4 = "";
                                    }
                                    if ((metropoli != metropoli2 && metropoli2 != metropoli3 && metropoli2 != metropoli4 && metropoli3 != metropoli4 && metropoli != metropoli4 && metropoli != metropoli3)) {
                                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion4, idjugador4, idtorneo4, pais4, metropoli4, club4);
                                        getRandom.savePagoIndC(pagoIndividualControl);
                                        pagoindlist.add(pagoIndividual);
                                        condicion4 = true;
                                        count = 0;
                                        condicion4 = true;
                                        condicion = true;

                                    }

                                }
                            }
                        }
                    }
                    if (rows2 == 0) {
                        idinscripcion3 = pagoIndividual.getIdinscripcion();
                        idjugador3 = pagoIndividual.getIdjugador();
                        idtorneo3 = pagoIndividual.getIdtorneo();
                        pais3 = pagoIndividual.getPais();
                        metropoli3 = pagoIndividual.getMetropoli();
                        club3 = pagoIndividual.getClub();
                        if (club3 == null) {
                            club3 = "";
                        }
                        if ((pais != pais2 && pais2 != pais3 && pais != pais3)) {
                            pagoIndividualControl = new PagoIndividualControl(null, idinscripcion3, idjugador3, idtorneo3, pais3, metropoli3, club3);
                            getRandom.savePagoIndC(pagoIndividualControl);
                            pagoindlist.add(pagoIndividual);
                            condicion3 = true;
                            count = 0;

                        }


                    }


                }
            }

            n = pagoindlist.size();
            System.out.println("***********" + n + "**********");
            if (n == 3) {
                while (condicion4 == false) {
                    pagoIndividual = getRandom.getRandom();
                    idinscripcion4 = pagoIndividual.getIdinscripcion();
                    System.out.println("***********" + idinscripcion4);
                    rows2 = this.getRandom.getPagoIndividualControl(idinscripcion4);
                    count += 1;
                    System.out.println("COUNT:" + count);
                    if (count == (filas * 5)) {
                        if (n == 3) {
                            while (condicion4 == false) {
                                count = 0;
                                pagoIndividual = getRandom.getRandom();
                                idinscripcion4 = pagoIndividual.getIdinscripcion();
                                System.out.println("***********" + idinscripcion4);
                                rows2 = this.getRandom.getPagoIndividualControl(idinscripcion4);
                                count += 1;
                                System.out.println("COUNT:" + count);
                                if (count == (filas * 5)) {
                                    condicion4 = true;
                                    condicion = true;
                                }
                                if (rows2 == 0) {
                                    idinscripcion4 = pagoIndividual.getIdinscripcion();
                                    idjugador4 = pagoIndividual.getIdjugador();
                                    idtorneo4 = pagoIndividual.getIdtorneo();
                                    pais4 = pagoIndividual.getPais();
                                    metropoli4 = pagoIndividual.getMetropoli();
                                    club4 = pagoIndividual.getClub();
                                    if (club4 == null) {
                                        club4 = "";
                                    }
                                    if ((metropoli != metropoli2 && metropoli2 != metropoli3 && metropoli2 != metropoli4 && metropoli3 != metropoli4 && metropoli != metropoli4 && metropoli != metropoli3)) {
                                        pagoIndividualControl = new PagoIndividualControl(null, idinscripcion4, idjugador4, idtorneo4, pais4, metropoli4, club4);
                                        getRandom.savePagoIndC(pagoIndividualControl);
                                        pagoindlist.add(pagoIndividual);
                                        condicion4 = true;
                                        count = 0;
                                        condicion4 = true;
                                        condicion = true;

                                    }

                                }
                            }
                        }

                    }
                    if (rows2 == 0) {
                        idinscripcion4 = pagoIndividual.getIdinscripcion();
                        idjugador4 = pagoIndividual.getIdjugador();
                        idtorneo4 = pagoIndividual.getIdtorneo();
                        pais4 = pagoIndividual.getPais();
                        metropoli4 = pagoIndividual.getMetropoli();
                        club4 = pagoIndividual.getClub();
                        if (club4 == null) {
                            club4 = "";
                        }
                        if ((pais != pais2 && pais2 != pais3 && pais2 != pais4 && pais3 != pais4 && pais != pais4 && pais != pais3)) {
                            pagoIndividualControl = new PagoIndividualControl(null, idinscripcion4, idjugador4, idtorneo4, pais4, metropoli4, club4);
                            getRandom.savePagoIndC(pagoIndividualControl);
                            pagoindlist.add(pagoIndividual);
                            condicion4 = true;
                            count = 0;

                        }

                    }


                }
            }
            n = pagoindlist.size();
            System.out.println("***********" + n + "**********");
            if (n == 4) {
                condicion1 = false;
                condicion2 = false;
                condicion3 = false;
                condicion4 = false;
                pagoindlist = new ArrayList(0);
                pagoIndividual = null;
                n = 0;
                count = 0;
            }

        }
        randomServicemetropoli.getRandom(torneo);
        System.out.println("Salio de  while");
    }

    public PagoIndividualControl getPagoControlInd(Integer id) throws Exception {
        PagoIndividualControl pagoIndividualControl;
        Session session = HibernateUtil.getSession();
        session.isConnected();
        pagoIndividualControl = this.getRandom.getPagoControlInd(id);
        return pagoIndividualControl;
    }
    
    public void insertPagoIndCont(PagoIndividualControl pagoIndividualControl) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.getRandom.insertPagoIndCont(pagoIndividualControl);
    
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public boolean isCondicion() {
        return condicion;
    }

    public void setCondicion(boolean condicion) {
        this.condicion = condicion;
    }

    public RandomDAO getGetRandom() {
        return getRandom;
    }

    public void setGetRandom(RandomDAO getRandom) {
        this.getRandom = getRandom;
    }

    public Integer getIdinscripcion() {
        return idinscripcion;
    }

    public void setIdinscripcion(Integer idinscripcion) {
        this.idinscripcion = idinscripcion;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public PagoIndividual getPagoIndividual() {
        return pagoIndividual;
    }

    public void setPagoIndividual(PagoIndividual pagoIndividual) {
        this.pagoIndividual = pagoIndividual;
    }

    public List<PagoIndividual> getPagoindlist() {
        return pagoindlist;
    }

    public void setPagoindlist(List<PagoIndividual> pagoindlist) {
        this.pagoindlist = pagoindlist;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public String getClub2() {
        return club2;
    }

    public void setClub2(String club2) {
        this.club2 = club2;
    }

    public String getClub3() {
        return club3;
    }

    public void setClub3(String club3) {
        this.club3 = club3;
    }

    public String getClub4() {
        return club4;
    }

    public void setClub4(String club4) {
        this.club4 = club4;
    }

    public boolean isCondicion1() {
        return condicion1;
    }

    public void setCondicion1(boolean condicion1) {
        this.condicion1 = condicion1;
    }

    public Integer getIdinscripcion2() {
        return idinscripcion2;
    }

    public void setIdinscripcion2(Integer idinscripcion2) {
        this.idinscripcion2 = idinscripcion2;
    }

    public Integer getIdinscripcion3() {
        return idinscripcion3;
    }

    public void setIdinscripcion3(Integer idinscripcion3) {
        this.idinscripcion3 = idinscripcion3;
    }

    public Integer getIdinscripcion4() {
        return idinscripcion4;
    }

    public void setIdinscripcion4(Integer idinscripcion4) {
        this.idinscripcion4 = idinscripcion4;
    }

    public Integer getIdjugador2() {
        return idjugador2;
    }

    public void setIdjugador2(Integer idjugador2) {
        this.idjugador2 = idjugador2;
    }

    public Integer getIdjugador3() {
        return idjugador3;
    }

    public void setIdjugador3(Integer idjugador3) {
        this.idjugador3 = idjugador3;
    }

    public Integer getIdjugador4() {
        return idjugador4;
    }

    public void setIdjugador4(Integer idjugador4) {
        this.idjugador4 = idjugador4;
    }

    public Integer getIdtorneo2() {
        return idtorneo2;
    }

    public void setIdtorneo2(Integer idtorneo2) {
        this.idtorneo2 = idtorneo2;
    }

    public Integer getIdtorneo3() {
        return idtorneo3;
    }

    public void setIdtorneo3(Integer idtorneo3) {
        this.idtorneo3 = idtorneo3;
    }

    public Integer getIdtorneo4() {
        return idtorneo4;
    }

    public void setIdtorneo4(Integer idtorneo4) {
        this.idtorneo4 = idtorneo4;
    }

    public Integer getMetropoli2() {
        return metropoli2;
    }

    public void setMetropoli2(Integer metropoli2) {
        this.metropoli2 = metropoli2;
    }

    public Integer getMetropoli3() {
        return metropoli3;
    }

    public void setMetropoli3(Integer metropoli3) {
        this.metropoli3 = metropoli3;
    }

    public Integer getMetropoli4() {
        return metropoli4;
    }

    public void setMetropoli4(Integer metropoli4) {
        this.metropoli4 = metropoli4;
    }

    public PagoIndividualControl getPagoIndividualControl() {
        return pagoIndividualControl;
    }

    public void setPagoIndividualControl(PagoIndividualControl pagoIndividualControl) {
        this.pagoIndividualControl = pagoIndividualControl;
    }

    public Integer getPais2() {
        return pais2;
    }

    public void setPais2(Integer pais2) {
        this.pais2 = pais2;
    }

    public Integer getPais3() {
        return pais3;
    }

    public void setPais3(Integer pais3) {
        this.pais3 = pais3;
    }

    public Integer getPais4() {
        return pais4;
    }

    public void setPais4(Integer pais4) {
        this.pais4 = pais4;
    }

    public boolean isCondicion2() {
        return condicion2;
    }

    public void setCondicion2(boolean condicion2) {
        this.condicion2 = condicion2;
    }

    public boolean isCondicion3() {
        return condicion3;
    }

    public void setCondicion3(boolean condicion3) {
        this.condicion3 = condicion3;
    }

    public boolean isCondicion4() {
        return condicion4;
    }

    public void setCondicion4(boolean condicion4) {
        this.condicion4 = condicion4;
    }

    public RandomServiceMetropoli getRandomServicemetropoli() {
        return randomServicemetropoli;
    }

    public void setRandomServicemetropoli(RandomServiceMetropoli randomServicemetropoli) {
        this.randomServicemetropoli = randomServicemetropoli;
    }

    public RandomServiceClub getRandomServiceclub() {
        return randomServiceclub;
    }

    public void setRandomServiceclub(RandomServiceClub randomServiceclub) {
        this.randomServiceclub = randomServiceclub;
    }
}
