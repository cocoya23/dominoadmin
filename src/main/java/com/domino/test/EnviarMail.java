package com.domino.test;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Envia un correo con un archivo pdf adjunto.
 * 
 * @author A. Eliud Uriostegui Glez.
 * @version 1.0.0
 */
public class EnviarMail {
    
    /*path de los adjuntos*/
    public static final String ADJUNTOS = "Comprobante de pago.pdf;Gafete.pdf";
    
    /*Nombre que tendran los archivos adjuntos*/
    public static final String ADJUNTOS_NOMBRE = 
            "Comprobante de pago.pdf;Gafete.pdf";
    
    public static final String DELIM = ";";
    
    public static void main (String []args){
    
        try {
            
            /*organizacion de los archivos*/
            String [] dividir_adj,dividir_adj_nombre;
            dividir_adj = ADJUNTOS.split(DELIM);
            dividir_adj_nombre = ADJUNTOS_NOMBRE.split(DELIM);
            
            /*Obtenemos el objeto Session. la configuracion sera 
             * temporalmente mi correo personal.
             */
            Properties properties = new Properties();
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            properties.setProperty("mail.smtp.port", "587");
            properties.setProperty("mail.smtp.user", "ing.uriostegui@gmail.com");
            properties.setProperty("mail.smtp.auth", "true");
            
            Session session = Session.getInstance(properties, null);
            //session.setDebug(true);
            //Se compone la parte del texto
            BodyPart bodyText = new MimeBodyPart();
            bodyText.setText("Comprobante de pago prueba pdf");
            
            //Adjunto del mensaje
            //BodyPart bodyAdjunto = new MimeBodyPart();
           // bodyAdjunto.setDataHandler(new DataHandler(new FileDataSource(
             //       "c://proyecto//DominoWeAd//Comprobante de pago.pdf")));// c:/prueba.pdf
            //bodyAdjunto.setFileName("Comprobante de pago.pdf");
            
            /*Adjuntando multiples documentos*/
            //lista de adjuntos
            List<BodyPart> adjuntos = new LinkedList<BodyPart>();
            for(int i=0; i<=dividir_adj.length-1;i++){
                BodyPart bodyAdjunto = new MimeBodyPart();
                bodyAdjunto.setDataHandler(new DataHandler(new FileDataSource(
                        dividir_adj[i])));
                bodyAdjunto.setFileName(dividir_adj_nombre[i]);
                adjuntos.add(bodyAdjunto);
            }
            
            //multiparte que agrupa texto y documento pdf
            MimeMultipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyText);
            //multipart.addBodyPart(bodyAdjunto);
            for(BodyPart bp : adjuntos){
                multipart.addBodyPart(bp);
            }
            
            /*Se compone el correo, to, from, subject, y el contenido */
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(
                    "ing.uriostegui@gmail.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                    //"sergioc@microsys.com.mx"));
                    "eliud.uriostegui@consultores-conocer.org.mx"));
            message.setSubject("Este correo ya esta saliendo desde netbeans, "
                    + "adjuntando el archivo de comprobante de pago en pdf si "
                    + "puedes me confirmas si se ve bien");
            message.setContent(multipart);
            
            /*Envia el correo*/
            Transport t = session.getTransport("smtp");
            t.connect("ing.uriostegui@gmail.com", "Hawkeye.34021972");
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }
}
