package com.domino.test;

import com.domino.other.EnviarCorreo;
import com.domino.service.CreateReciboPDF;
/**
 *
 * @author A. Eliud Uriostegui Glez.
 */
public class CreatePDFTest {
    
    public static void main (String []args){
        int folio = 99999;
            String lugar = "Tlalnepantla de Baz, Estado de México";
            String fecha = "6 de diciembre de 2014";
            String expedido = "Bernardo Couto 70; Col. Algarín; CP 06880; "
                    + "Delegación Cuauhtémoc; Ciudad de México. ";
            String torneo = "Torneo de Dominó Tlalnepantla Otoño 2014.";
            float cantidad = 1;
            String valor = "2000.00";
            String unidadAmparo = 
                    "Participación en las modalidades de individual y parejas";
            String tipoPago = "Efectivo";
            
                    /*Datos del Gafete*/
        String torneoGafete = "Tlalnepantla Invierno 2015";
        String numJugador = "1 2 3 4";
        String modalidad = "Individual y Parejas";
        String jugador = "Javier Ricardo Ramírez Villanueva";
        String lugarGafete = "México";
        String fechaGafete = "Fecha";
        
        CreateReciboPDF reciboPDF = new CreateReciboPDF();
        reciboPDF.generarComprobantePDF(folio, lugar, fecha, expedido, torneo, 
                cantidad, valor, unidadAmparo, tipoPago);
        reciboPDF.crearGafetePDF(torneoGafete, numJugador, modalidad, jugador, 
                lugarGafete, fechaGafete);
        EnviarCorreo correo = new EnviarCorreo();
       // correo.sendEmail();
    }
}
