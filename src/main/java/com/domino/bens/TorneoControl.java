/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author A. Eliud Uriostegui Glez.
 */

@Entity
@Table (name="torneo_control")
public class TorneoControl implements Serializable {
    
    @Id
    @GeneratedValue (strategy= GenerationType.IDENTITY)
    @Column (name="idtorneo_control")
    private Integer idTorneoControl;
    @Column (name="idjugador")
    private Integer idJugador;
    @Column (name="idtorneo")
    private Integer idTorneo;
    

    public TorneoControl() {
    }

    public TorneoControl(Integer idTorneoControl, Integer idJugador, 
            Integer idTorneo) {
        this.idTorneoControl = idTorneoControl;
        this.idJugador = idJugador;
        this.idTorneo = idTorneo;
       
    }
    
    public Integer getIdTorneoControl() {
        return idTorneoControl;
    }

    public void setIdTorneoControl(Integer idTorneoControl) {
        this.idTorneoControl = idTorneoControl;
    }

  

    public Integer getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(Integer idJugador) {
        this.idJugador = idJugador;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }
    
}
