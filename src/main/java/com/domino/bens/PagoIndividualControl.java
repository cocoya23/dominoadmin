package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "pago_individual_control")
public class PagoIndividualControl implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "idinscripcion")
    private Integer idinscripcion;
    @Column(name = "idjugador")
    private Integer idjugador;
    @Column(name = "idtorneo")
    private Integer idtorneo;
    @Column(name = "pais")
    private Integer pais;
    @Column(name = "metropoli")
    private Integer metropoli;
    @Column(name = "club")
    private String club;
   

    public PagoIndividualControl() {
    }

    public PagoIndividualControl(Integer id,Integer idinscripcion, Integer idjugador, Integer idtorneo, Integer pais, Integer metropoli, String club) {
        this.id=id;
        this.idinscripcion = idinscripcion;
        this.idjugador = idjugador;
        this.idtorneo = idtorneo;
        this.pais = pais;
        this.metropoli = metropoli;
        this.club = club;
     
    }



    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

  

    public Integer getIdinscripcion() {
        return idinscripcion;
    }

    public void setIdinscripcion(Integer idinscripcion) {
        this.idinscripcion = idinscripcion;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    
}