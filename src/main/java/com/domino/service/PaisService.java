package com.domino.service;

import com.domino.bens.Pais;
import com.domino.dao.PaisDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

@ManagedBean(name = "paisService", eager = true)
@ApplicationScoped
public class PaisService implements Serializable {

    PaisDAO paisDao = new PaisDAO();

    public List<Pais> getPaises() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Pais> paises = this.paisDao.getPaises();
        return paises;
    }

    public void insertPaises(Pais pais) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.paisDao.insertPaises(pais);
    }

    public List<Pais> getPais() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Pais> paises = this.paisDao.getPais();
        return paises;
    }

    public void updatePaises(Pais pais) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.paisDao.updatePaises(pais);
    }

    public void deletepaisSP(Integer idPais) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.paisDao.deletepaisSP(idPais);
    }
}
