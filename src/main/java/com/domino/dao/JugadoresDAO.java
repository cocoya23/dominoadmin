/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.dao;

import com.domino.bens.Jugador;
import com.domino.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;


/**
 *
 * @author Sergio Cano
 */
public class JugadoresDAO {
    
    public List<Jugador> getJugadores() throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<Jugador> result = session.createQuery("from Jugador").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    
    
    
}
