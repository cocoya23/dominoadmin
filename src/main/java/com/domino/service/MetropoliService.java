package com.domino.service;

import com.domino.bens.Metropoli;
import com.domino.dao.MetropoliDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

@ManagedBean(name = "metropoliService", eager = true)
@ApplicationScoped
public class MetropoliService implements Serializable {

    MetropoliDAO metropoliDAO = new MetropoliDAO();

    public List<Metropoli> getMetropoli() throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Metropoli> metropoli = this.metropoliDAO.getMetropoli();
        return metropoli;
    }

    public List<Metropoli> getMetropoliId(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Metropoli> metropoli = this.metropoliDAO.getMetropoliId(id);
        return metropoli;
    }

    public void insertMetropoli(Metropoli metropoli) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.metropoliDAO.insertMetropoli(metropoli);

    }

    public List<Metropoli> getMetropoliMet(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Metropoli> metropoli = this.metropoliDAO.getMetropoliMet(id);
        return metropoli;
    }

    public void updateMetropoli(Metropoli metropoli) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.metropoliDAO.updateMetropoli(metropoli);
    }

    public void deleteMetropoli(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
       this.metropoliDAO.deleteMetropoli(id);
    }

    public Integer getRowsJugador(Integer idmet) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.metropoliDAO.getRowsJugador(idmet);
        return rows;
    }
}
