package com.domino.managed;

import com.domino.bens.Pais;
import com.domino.service.PaisService;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class PaisManaged implements Serializable {

    @ManagedProperty("#{paisService}")
    private PaisService paisService;
    private String paisn;
    Pais pais;
    private List<Pais> paises;
    private String nompais;
    private Integer idPais;

    @PostConstruct
    public void init() {

        try {
            paises = paisService.getPais();
        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void guardaPais() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            pais = new Pais(null, paisn);
            paisService.insertPaises(pais);
            context.addMessage(null, new FacesMessage("Exito", "Se registro pais" + paisn));
            paisn = "";
            paises = paisService.getPais();
        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "No se pudo guardar el pais"));
        }

    }

    public void modificaPais() {
        try {
            System.out.println("Entro a modifica pais");
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Map params = facesContext.getExternalContext().getRequestParameterMap();
            idPais = new Integer((String) params.get("idPais"));
            nompais = (String) params.get("nombre");
            System.out.println(idPais);
            System.out.println(nompais);
        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void updatePais() {
        try {
            pais = new Pais(idPais, nompais);
            paisService.updatePaises(pais);
            FacesContext contex = FacesContext.getCurrentInstance();
            contex.getExternalContext().redirect("registropais.xhtml");
        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void deletePais() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            Map params = facesContext.getExternalContext().getRequestParameterMap();
            idPais = new Integer((String) params.get("idPais"));
            paisService.deletepaisSP(idPais);
            paisn = "";
            paises = paisService.getPais();
            context.addMessage(null, new FacesMessage("Exito", "Se elimino pais"));

        } catch (Exception ex) {
            Logger.getLogger(PaisManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public PaisService getPaisService() {
        return paisService;
    }

    public void setPaisService(PaisService paisService) {
        this.paisService = paisService;
    }

    public String getPaisn() {
        return paisn;
    }

    public void setPaisn(String paisn) {
        this.paisn = paisn;
    }

    public List<Pais> getPaises() {
        return paises;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }

    public String getNompais() {
        return nompais;
    }

    public void setNompais(String nompais) {
        this.nompais = nompais;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }
}
