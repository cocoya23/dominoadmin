package com.domino.bens;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "formacionpareja")
public class FormacionPareja implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idformacionpareja")
    private Integer idformacionpareja;
    
    @Column(name = "idtorneo")
    private Integer idtorneo;
    
    @Column(name = "idjugador1")
    private Integer idjugador1;
    
    @Column(name = "idjugador2")
    private Integer idjugador2;

    public FormacionPareja() {
    }

    public FormacionPareja(Integer idformacionpareja, Integer idtorneo, Integer idjugador1, Integer idjugador2) {
        this.idformacionpareja = idformacionpareja;
        this.idtorneo = idtorneo;
        this.idjugador1 = idjugador1;
        this.idjugador2 = idjugador2;
    }

    public Integer getIdformacionpareja() {
        return idformacionpareja;
    }

    public void setIdformacionpareja(Integer idformacionpareja) {
        this.idformacionpareja = idformacionpareja;
    }

    public Integer getIdtorneo() {
        return idtorneo;
    }

    public void setIdtorneo(Integer idtorneo) {
        this.idtorneo = idtorneo;
    }

    public Integer getIdjugador1() {
        return idjugador1;
    }

    public void setIdjugador1(Integer idjugador1) {
        this.idjugador1 = idjugador1;
    }

    public Integer getIdjugador2() {
        return idjugador2;
    }

    public void setIdjugador2(Integer idjugador2) {
        this.idjugador2 = idjugador2;
    }
}
