package com.domino.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.sun.faces.lifecycle.Phase;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import javax.servlet.jsp.tagext.TryCatchFinally;
import org.primefaces.mobile.component.footer.Footer;

/**
 *
 * @author A. Eliud Uriostegui Glez.
 * @version 1.0.0
 */
public class CreateReciboPDF {

    /*
     * Margen del recibo
     */
    //Document recibo = new Document(PageSize.A4, 55, 55, 75, 75);
    Document recibopareja = new Document(PageSize.A4, 55, 55, 75, 75);
    Document reciboequipo = new Document(PageSize.A4, 55, 55, 75, 75);
    /*
     * Margen del gafete
     */
    Document gafete = new Document(PageSize.A4, 55, 55, 75, 75);
    /*
     * Variables comprobante
     */
    Integer folio;
    String lugar;
    String fecha;
    String expedido;
    String torneo;
    Float cantidad;
    String valor;
    String unidadAmparo;
    String tipoPago;
    /*
     * Variables gafete
     */
    String torneoGafete;
    String numJugador;
    String modalidad;
    String jugador;
    String lugarGafete;
    String fechaGafete;
    /*
     * Definiendo tipos de estilo de letras a usar
     */
    public static final Font BLACK_NEGRITA_TITUTLO =
            new Font(Font.FontFamily.HELVETICA, 17, Font.BOLD, BaseColor.BLACK);
    public static final Font BlACK_NEGRITA_SUBTITULO =
            new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLACK);
    public static final Font RED_NEGRITA_DATOS =
            new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.RED);
    public static final Font BLACK_FONT_DATOS =
            new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLACK);
    public static final Font ENLACE =
            new Font(Font.FontFamily.HELVETICA, 14, Font.UNDERLINE,
            BaseColor.BLUE);

    /**
     * Genera un comprobante en pdf.
     *
     * @param folio folio del comprobante
     * @param lugar lugar del torneo.
     * @param fecha fecha de pago
     * @param expedido lugar en el que ha sido expedido el comprobante
     * @param torneo nombre del torneo
     * @param cantidad cantidad
     * @param valor valor
     * @param unidadAmparo lo que cubre este pago
     * @param tipoPago por medio de que fue el pago.
     */
    public void generarComprobantePDF(Integer folio, String lugar, String fecha,
            String expedido, String torneo, Float cantidad, String valor,
            String unidadAmparo, String tipoPago) {
        try {

            /*
             * Datos del comprobante
             */
            Document recibo = new Document(PageSize.A4, 55, 55, 75, 75);
            this.folio = folio;
            this.lugar = lugar;
            this.fecha = fecha;
            this.expedido = expedido;
            this.torneo = torneo;
            this.cantidad = cantidad;
            this.valor = valor;
            this.unidadAmparo = unidadAmparo;
            this.tipoPago = tipoPago;

            /*
             * El archivo que vamos a generar
             */
            FileOutputStream fileOutputStream = new FileOutputStream(
                    "Comprobante de pago.pdf");
            /*
             * Obtener la instalacia de PdfWriter
             */
            PdfWriter.getInstance(recibo, fileOutputStream);

            /*
             * Abrimos el documento
             */
            recibo.open();

            /*
             * Creamos fuentes para el contenido y los titulos
             */
            Font fontRED = FontFactory.getFont(FontFactory.HELVETICA_BOLD,
                    13, BaseColor.RED);
//            Font fontContenido = FontFactory.getFont(
//                    FontFactory.TIMES_ROMAN.toString(),11,Font.NORMAL, 
//                    BaseColor.BLACK);

            /*
             * Creación de un parrafo
             */
            Paragraph encabezado = new Paragraph();

            /*
             * Agregar un titulo
             */
            encabezado.add(new Phrase(
                    "Amigos por la Cultura, el Deporte y la Recreación S.C.",
                    BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(
                    "Registro federal de contribuyentes: ACD140324JPA",
                    BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Régimen General de Ley "
                    + "Personas Morales", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Comprobante de operación con el "
                    + "público en general", BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Folio:", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase("" + folio, fontRED));
            encabezado.add(new Phrase(Chunk.NEWLINE));

            /*
             * Agregamos el encabezado al documento alineado a la derecha
             */
            encabezado.setAlignment(Element.ALIGN_RIGHT);
            recibo.add(encabezado);
            recibo.add(Chunk.NEWLINE);
            recibo.add(new LineSeparator());
            recibo.add(Chunk.NEWLINE);
            /*
             * Información del comprobante
             */
            Paragraph info = new Paragraph();

            /*
             * Lugar del torneo
             */
            info.add(new Phrase("Lugar:", BLACK_FONT_DATOS));
            info.add(new Phrase(lugar + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Fecha:", BLACK_FONT_DATOS));
            info.add(new Phrase(fecha + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante expedido en:", BLACK_FONT_DATOS));
            info.add(new Phrase("\n" + expedido + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante para participar en el "
                    + "concurso de dominó:\n", BLACK_FONT_DATOS));
            info.add(new Phrase(torneo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Cantidad:", BLACK_FONT_DATOS));
            info.add(new Phrase("" + cantidad + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Unidad de medida y "
                    + "clase del uso que se ampara:", BLACK_FONT_DATOS));
            info.add(new Phrase(unidadAmparo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Valor por las modalidades a participar:",
                    BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Total pagado:", BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Pagado con:", BLACK_FONT_DATOS));
            info.add(new Phrase(tipoPago + ". \n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));

            /*
             * Lo agregamos al cuerpo del pdf
             */
            recibo.add(info);
            recibo.add(new LineSeparator());

            /*
             * Creamos un nuevo parrafo para el pie de pagina
             */
            Paragraph footer = new Paragraph();
            footer.add(new Phrase("Av. Compositores 211-A Col. Analco CP 62166, "
                    + "Cuernavaca, Morelos\n", BLACK_FONT_DATOS));
            footer.add(new Phrase("Tel. 55382706  Cel. 5513716609 ",
                    BLACK_FONT_DATOS));

            /*
             * Agregando hipervinculos
             */
            Anchor enlace = null;
            enlace = new Anchor("amigosporeldomino@gmail.com", ENLACE);
            enlace.setReference("mailto:amigosporeldomino@gmail.com");
            footer.add(enlace);
            /*
             * Agregamos el footer al documento
             */
            footer.setAlignment(Element.ALIGN_CENTER);
            footer.add(new Phrase("\nwww.amigosporeldomino.com",
                    BLACK_FONT_DATOS));
            recibo.add(footer);
            recibo.close();

            /*
             * Abrir el archivo
             */
            //File file = new File("Comprobante de pago.pdf");
            //Desktop.getDesktop().open(file);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void generarComprobantePDFIndividual(Integer folio, String lugar, String fecha,
            String expedido, String torneo, Float cantidad, String valor,
            String unidadAmparo, String tipoPago) {
        try {

            /*
             * Datos del comprobante
             */
            Document recibo = new Document(PageSize.A4, 55, 55, 75, 75);
            this.folio = folio;
            this.lugar = lugar;
            this.fecha = fecha;
            this.expedido = expedido;
            this.torneo = torneo;
            this.cantidad = cantidad;
            this.valor = valor;
            this.unidadAmparo = unidadAmparo;
            this.tipoPago = tipoPago;

            /*
             * El archivo que vamos a generar
             */
            FileOutputStream fileOutputStream = new FileOutputStream("Comprobante de pago.pdf");
            /*
             * Obtener la instalacia de PdfWriter
             */
            PdfWriter.getInstance(recibo, fileOutputStream);

            /*
             * Abrimos el documento
             */
            recibo.open();

            /*
             * Creamos fuentes para el contenido y los titulos
             */
            Font fontRED = FontFactory.getFont(FontFactory.HELVETICA_BOLD,
                    13, BaseColor.RED);
//            Font fontContenido = FontFactory.getFont(
//                    FontFactory.TIMES_ROMAN.toString(),11,Font.NORMAL, 
//                    BaseColor.BLACK);

            /*
             * Creación de un parrafo
             */
            Paragraph encabezado = new Paragraph();

            /*
             * Agregar un titulo
             */
            encabezado.add(new Phrase(
                    "Amigos por la Cultura, el Deporte y la Recreación S.C.",
                    BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(
                    "Registro federal de contribuyentes: ACD140324JPA",
                    BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Régimen General de Ley "
                    + "Personas Morales", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Comprobante de operación con el "
                    + "público en general", BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Folio:", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase("" + folio, fontRED));
            encabezado.add(new Phrase(Chunk.NEWLINE));

            /*
             * Agregamos el encabezado al documento alineado a la derecha
             */
            encabezado.setAlignment(Element.ALIGN_RIGHT);
            recibo.add(encabezado);
            recibo.add(Chunk.NEWLINE);
            recibo.add(new LineSeparator());
            recibo.add(Chunk.NEWLINE);
            /*
             * Información del comprobante
             */
            Paragraph info = new Paragraph();

            /*
             * Lugar del torneo
             */
            info.add(new Phrase("Lugar:", BLACK_FONT_DATOS));
            info.add(new Phrase(lugar + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Fecha:", BLACK_FONT_DATOS));
            info.add(new Phrase(fecha + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante expedido en:", BLACK_FONT_DATOS));
            info.add(new Phrase("\n" + expedido + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante para participar en el "
                    + "concurso de dominó:\n", BLACK_FONT_DATOS));
            info.add(new Phrase(torneo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Cantidad:", BLACK_FONT_DATOS));
            info.add(new Phrase("" + cantidad + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Unidad de medida y "
                    + "clase del uso que se ampara:", BLACK_FONT_DATOS));
            info.add(new Phrase(unidadAmparo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Valor por las modalidades a participar:",
                    BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Total pagado:", BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Pagado con:", BLACK_FONT_DATOS));
            info.add(new Phrase(tipoPago + ". \n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));

            /*
             * Lo agregamos al cuerpo del pdf
             */
            recibo.add(info);
            recibo.add(new LineSeparator());

            /*
             * Creamos un nuevo parrafo para el pie de pagina
             */
            Paragraph footer = new Paragraph();
            footer.add(new Phrase("Av. Compositores 211-A Col. Analco CP 62166, "
                    + "Cuernavaca, Morelos\n", BLACK_FONT_DATOS));
            footer.add(new Phrase("Tel. 55382706  Cel. 5513716609 ",
                    BLACK_FONT_DATOS));

            /*
             * Agregando hipervinculos
             */
            Anchor enlace = null;
            enlace = new Anchor("amigosporeldomino@gmail.com", ENLACE);
            enlace.setReference("mailto:amigosporeldomino@gmail.com");
            footer.add(enlace);
            /*
             * Agregamos el footer al documento
             */
            footer.setAlignment(Element.ALIGN_CENTER);
            footer.add(new Phrase("\nwww.amigosporeldomino.com",
                    BLACK_FONT_DATOS));
            recibo.add(footer);
            recibo.close();


            /*
             * Abrir el archivo
             */
            //File file = new File("Comprobante de pago.pdf");
            //Desktop.getDesktop().open(file);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void generarComprobantePDFPareja(Integer folio, String lugar, String fecha,
            String expedido, String torneo, Float cantidad, String valor,
            String unidadAmparo, String tipoPago) {
        try {

            /*
             * Datos del comprobante
             */

            this.folio = folio;
            this.lugar = lugar;
            this.fecha = fecha;
            this.expedido = expedido;
            this.torneo = torneo;
            this.cantidad = cantidad;
            this.valor = valor;
            this.unidadAmparo = unidadAmparo;
            this.tipoPago = tipoPago;

            /*
             * El archivo que vamos a generar
             */
            FileOutputStream fileOutputStream = new FileOutputStream(
                    "Comprobante de pago pareja.pdf");
            /*
             * Obtener la instalacia de PdfWriter
             */
            PdfWriter.getInstance(recibopareja, fileOutputStream);

            /*
             * Abrimos el documento
             */
            recibopareja.open();

            /*
             * Creamos fuentes para el contenido y los titulos
             */
            Font fontRED = FontFactory.getFont(FontFactory.HELVETICA_BOLD,
                    13, BaseColor.RED);
//            Font fontContenido = FontFactory.getFont(
//                    FontFactory.TIMES_ROMAN.toString(),11,Font.NORMAL, 
//                    BaseColor.BLACK);

            /*
             * Creación de un parrafo
             */
            Paragraph encabezado = new Paragraph();

            /*
             * Agregar un titulo
             */
            encabezado.add(new Phrase(
                    "Amigos por la Cultura, el Deporte y la Recreación S.C.",
                    BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(
                    "Registro federal de contribuyentes: ACD140324JPA",
                    BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Régimen General de Ley "
                    + "Personas Morales", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Comprobante de operación con el "
                    + "público en general", BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Folio:", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase("" + folio, fontRED));
            encabezado.add(new Phrase(Chunk.NEWLINE));

            /*
             * Agregamos el encabezado al documento alineado a la derecha
             */
            encabezado.setAlignment(Element.ALIGN_RIGHT);
            recibopareja.add(encabezado);
            recibopareja.add(Chunk.NEWLINE);
            recibopareja.add(new LineSeparator());
            recibopareja.add(Chunk.NEWLINE);
            /*
             * Información del comprobante
             */
            Paragraph info = new Paragraph();

            /*
             * Lugar del torneo
             */
            info.add(new Phrase("Lugar:", BLACK_FONT_DATOS));
            info.add(new Phrase(lugar + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Fecha:", BLACK_FONT_DATOS));
            info.add(new Phrase(fecha + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante expedido en:", BLACK_FONT_DATOS));
            info.add(new Phrase("\n" + expedido + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante para participar en el "
                    + "concurso de dominó:\n", BLACK_FONT_DATOS));
            info.add(new Phrase(torneo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Cantidad:", BLACK_FONT_DATOS));
            info.add(new Phrase("" + cantidad + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Unidad de medida y "
                    + "clase del uso que se ampara:", BLACK_FONT_DATOS));
            info.add(new Phrase(unidadAmparo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Valor por las modalidades a participar:",
                    BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Total pagado:", BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Pagado con:", BLACK_FONT_DATOS));
            info.add(new Phrase(tipoPago + ". \n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));

            /*
             * Lo agregamos al cuerpo del pdf
             */
            recibopareja.add(info);
            recibopareja.add(new LineSeparator());

            /*
             * Creamos un nuevo parrafo para el pie de pagina
             */
            Paragraph footer = new Paragraph();
            footer.add(new Phrase("Av. Compositores 211-A Col. Analco CP 62166, "
                    + "Cuernavaca, Morelos\n", BLACK_FONT_DATOS));
            footer.add(new Phrase("Tel. 55382706  Cel. 5513716609 ",
                    BLACK_FONT_DATOS));

            /*
             * Agregando hipervinculos
             */
            Anchor enlace = null;
            enlace = new Anchor("amigosporeldomino@gmail.com", ENLACE);
            enlace.setReference("mailto:amigosporeldomino@gmail.com");
            footer.add(enlace);
            /*
             * Agregamos el footer al documento
             */
            footer.setAlignment(Element.ALIGN_CENTER);
            footer.add(new Phrase("\nwww.amigosporeldomino.com",
                    BLACK_FONT_DATOS));
            recibopareja.add(footer);
            recibopareja.close();

            /*
             * Abrir el archivo
             */
            //File file = new File("Comprobante de pago.pdf");
            //Desktop.getDesktop().open(file);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void generarComprobantePDFEquipo(Integer folio, String lugar, String fecha,
            String expedido, String torneo, Float cantidad, String valor,
            String unidadAmparo, String tipoPago) {
        try {

            /*
             * Datos del comprobante
             */

            this.folio = folio;
            this.lugar = lugar;
            this.fecha = fecha;
            this.expedido = expedido;
            this.torneo = torneo;
            this.cantidad = cantidad;
            this.valor = valor;
            this.unidadAmparo = unidadAmparo;
            this.tipoPago = tipoPago;

            /*
             * El archivo que vamos a generar
             */
            FileOutputStream fileOutputStream = new FileOutputStream(
                    "Comprobante de pago equipo.pdf");
            /*
             * Obtener la instalacia de PdfWriter
             */
            PdfWriter.getInstance(reciboequipo, fileOutputStream);

            /*
             * Abrimos el documento
             */
            reciboequipo.open();

            /*
             * Creamos fuentes para el contenido y los titulos
             */
            Font fontRED = FontFactory.getFont(FontFactory.HELVETICA_BOLD,
                    13, BaseColor.RED);
//            Font fontContenido = FontFactory.getFont(
//                    FontFactory.TIMES_ROMAN.toString(),11,Font.NORMAL, 
//                    BaseColor.BLACK);

            /*
             * Creación de un parrafo
             */
            Paragraph encabezado = new Paragraph();

            /*
             * Agregar un titulo
             */
            encabezado.add(new Phrase(
                    "Amigos por la Cultura, el Deporte y la Recreación S.C.",
                    BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(
                    "Registro federal de contribuyentes: ACD140324JPA",
                    BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Régimen General de Ley "
                    + "Personas Morales", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Comprobante de operación con el "
                    + "público en general", BLACK_NEGRITA_TITUTLO));
            encabezado.add(new Phrase(Chunk.NEWLINE));
            encabezado.add(new Phrase("Folio:", BlACK_NEGRITA_SUBTITULO));
            encabezado.add(new Phrase("" + folio, fontRED));
            encabezado.add(new Phrase(Chunk.NEWLINE));

            /*
             * Agregamos el encabezado al documento alineado a la derecha
             */
            encabezado.setAlignment(Element.ALIGN_RIGHT);
            reciboequipo.add(encabezado);
            reciboequipo.add(Chunk.NEWLINE);
            reciboequipo.add(new LineSeparator());
            reciboequipo.add(Chunk.NEWLINE);
            /*
             * Información del comprobante
             */
            Paragraph info = new Paragraph();

            /*
             * Lugar del torneo
             */
            info.add(new Phrase("Lugar:", BLACK_FONT_DATOS));
            info.add(new Phrase(lugar + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Fecha:", BLACK_FONT_DATOS));
            info.add(new Phrase(fecha + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante expedido en:", BLACK_FONT_DATOS));
            info.add(new Phrase("\n" + expedido + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Comprobante para participar en el "
                    + "concurso de dominó:\n", BLACK_FONT_DATOS));
            info.add(new Phrase(torneo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Cantidad:", BLACK_FONT_DATOS));
            info.add(new Phrase("" + cantidad + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Unidad de medida y "
                    + "clase del uso que se ampara:", BLACK_FONT_DATOS));
            info.add(new Phrase(unidadAmparo + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Valor por las modalidades a participar:",
                    BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Total pagado:", BLACK_FONT_DATOS));
            info.add(new Phrase("$" + valor + "\n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase("Pagado con:", BLACK_FONT_DATOS));
            info.add(new Phrase(tipoPago + ". \n\n", RED_NEGRITA_DATOS));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));
            info.add(new Phrase(Chunk.NEWLINE));

            /*
             * Lo agregamos al cuerpo del pdf
             */
            reciboequipo.add(info);
            reciboequipo.add(new LineSeparator());

            /*
             * Creamos un nuevo parrafo para el pie de pagina
             */
            Paragraph footer = new Paragraph();
            footer.add(new Phrase("Av. Compositores 211-A Col. Analco CP 62166, "
                    + "Cuernavaca, Morelos\n", BLACK_FONT_DATOS));
            footer.add(new Phrase("Tel. 55382706  Cel. 5513716609 ",
                    BLACK_FONT_DATOS));

            /*
             * Agregando hipervinculos
             */
            Anchor enlace = null;
            enlace = new Anchor("amigosporeldomino@gmail.com", ENLACE);
            enlace.setReference("mailto:amigosporeldomino@gmail.com");
            footer.add(enlace);
            /*
             * Agregamos el footer al documento
             */
            footer.setAlignment(Element.ALIGN_CENTER);
            footer.add(new Phrase("\nwww.amigosporeldomino.com",
                    BLACK_FONT_DATOS));
            reciboequipo.add(footer);
            reciboequipo.close();

            /*
             * Abrir el archivo
             */
            //File file = new File("Comprobante de pago.pdf");
            //Desktop.getDesktop().open(file);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void crearGafetePDF(String torneo, String numJugador,
            String modalidad, String jugador, String lugar, String fecha) {

        /*
         * Datos del Gafete
         */
        this.torneoGafete = torneo;
        this.numJugador = numJugador;
        this.modalidad = modalidad;
        this.jugador = jugador;
        this.lugarGafete = lugar;
        this.fechaGafete = fecha;

        try {

            FileOutputStream fileOutputStream = new FileOutputStream(
                    "Gafete.pdf");
            /*
             * Obtener la instalacia de PdfWriter
             */
            PdfWriter.getInstance(gafete, fileOutputStream);
            /*
             * Abrimos el documento
             */
            gafete.open();
            gafete.add(new LineSeparator());

            //FontFactory.getFont(FontFactory.HELVETICA_BOLD,
            // 13,BaseColor.RED);
            /*
             * tipos de letra
             */
            Font torneoFont = FontFactory.getFont(FontFactory.HELVETICA, 18,
                    BaseColor.BLACK);
            Font parentesisFont = FontFactory.getFont(FontFactory.HELVETICA, 14,
                    BaseColor.BLACK);
            Font fontNumJugador = FontFactory.getFont(FontFactory.HELVETICA, 72,
                    BaseColor.BLACK);
            Font fontModalidad = FontFactory.getFont(FontFactory.HELVETICA, 22,
                    BaseColor.BLACK);
            Font fontJugador = FontFactory.getFont(FontFactory.HELVETICA_BOLD,
                    22, BaseColor.BLACK);
            Font fontLugar = FontFactory.getFont(FontFactory.HELVETICA, 22,
                    BaseColor.BLACK);
            Font fontFecha = FontFactory.getFont(FontFactory.HELVETICA, 22,
                    BaseColor.BLACK);

            Paragraph p = new Paragraph();
            p.add(new Phrase(torneoGafete, torneoFont));
            p.add(new Phrase("\n(Nombre del torneo)\n", parentesisFont));
            p.setAlignment(Element.ALIGN_CENTER);
            gafete.add(p);

            p = new Paragraph();
            p.add(new Phrase("\n\n\n\n\n" + numJugador + "\n", fontNumJugador));
            p.add(new Phrase("\n(Número de Jugador)     ", parentesisFont));
            p.add(new Phrase("\n\n\n\n" + modalidad + "\n", fontModalidad));
            p.add(new Phrase("(Modalidades a Jugar)     ", parentesisFont));
            p.setAlignment(Element.ALIGN_RIGHT);
            gafete.add(p);
            gafete.add(Chunk.NEWLINE);

            p = new Paragraph();
            p.add(new Phrase("\n\n\n" + jugador, fontJugador));
            p.add(new Phrase("\n(Nombre del jugador)", parentesisFont));
            p.add(new Phrase("\n\n\n\n" + lugarGafete, fontLugar));
            p.add(new Phrase("\n(Lugar)", parentesisFont));
            gafete.add(p);
            gafete.add(Chunk.NEWLINE);

            p = new Paragraph();
            p.add(new Phrase("\n\n\n\n\n\n\n\n" + fechaGafete + "      ", fontFecha));
            p.add(new Phrase("\n(Fecha del torneo)", parentesisFont));

            p.setAlignment(Element.ALIGN_RIGHT);
            gafete.add(p);
            gafete.close();

            /*
             * Abrir el archivo
             */
            //File file = new File("Gafete.pdf");
            //Desktop.getDesktop().open(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Float getCantidad() {
        return cantidad;
    }

    public void setCantidad(Float cantidad) {
        this.cantidad = cantidad;
    }

    public String getExpedido() {
        return expedido;
    }

    public void setExpedido(String expedido) {
        this.expedido = expedido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getFechaGafete() {
        return fechaGafete;
    }

    public void setFechaGafete(String fechaGafete) {
        this.fechaGafete = fechaGafete;
    }

    public Integer getFolio() {
        return folio;
    }

    public void setFolio(Integer folio) {
        this.folio = folio;
    }

    public Document getGafete() {
        return gafete;
    }

    public void setGafete(Document gafete) {
        this.gafete = gafete;
    }

    public String getJugador() {
        return jugador;
    }

    public void setJugador(String jugador) {
        this.jugador = jugador;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getLugarGafete() {
        return lugarGafete;
    }

    public void setLugarGafete(String lugarGafete) {
        this.lugarGafete = lugarGafete;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getNumJugador() {
        return numJugador;
    }

    public void setNumJugador(String numJugador) {
        this.numJugador = numJugador;
    }

   

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getTorneo() {
        return torneo;
    }

    public void setTorneo(String torneo) {
        this.torneo = torneo;
    }

    public String getTorneoGafete() {
        return torneoGafete;
    }

    public void setTorneoGafete(String torneoGafete) {
        this.torneoGafete = torneoGafete;
    }

    public String getUnidadAmparo() {
        return unidadAmparo;
    }

    public void setUnidadAmparo(String unidadAmparo) {
        this.unidadAmparo = unidadAmparo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}