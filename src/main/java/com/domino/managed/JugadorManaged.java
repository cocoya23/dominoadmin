/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.domino.managed;

import com.domino.bens.Jugador;
import com.domino.service.JugadorService;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Sergio Cano
 */
@ManagedBean
@ViewScoped
public class JugadorManaged implements Serializable {

    @ManagedProperty("#{jugadorService}")
    private JugadorService jugadorService;
    private List<Jugador> jugadores;
    Jugador jugador;


    @PostConstruct
    public void init() {

        try {
            jugadores = jugadorService.getJugadores();
            

        } catch (Exception ex) {
            Logger.getLogger(RegistroTorneoManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public JugadorService getJugadorService() {
        return jugadorService;
    }

    public void setJugadorService(JugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    public List<Jugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }
}
